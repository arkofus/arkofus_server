package fr.arkofus.client.gui.jobs.slot;

import fr.arkofus.player.inventory.RecipeInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 17/11/2018 à 16:57
 * par Jullian Dorian
 */
public class SlotRecipeResult extends Slot {

    private final RecipeInventory recipeInventory;

    private final EntityPlayer entityPlayer;

    private int amountCrafted;

    public SlotRecipeResult(EntityPlayer entityPlayer, IInventory recipeInventory, IInventory resultInventory, int index, int xPosition, int yPosition) {
        super(resultInventory, index, xPosition, yPosition);
        this.entityPlayer = entityPlayer;
        this.recipeInventory = (RecipeInventory) recipeInventory;
    }

    @Override
    public int getSlotStackLimit() {
        return 200;
    }

    @Override
    public ItemStack decrStackSize(int amount) {
        if(this.getHasStack())
            this.amountCrafted += Math.min(amount, this.getStack().getCount());
        return this.inventory.decrStackSize(this.getSlotIndex(), amount);
    }

    @Override
    protected void onCrafting(ItemStack stack, int amount) {
        this.amountCrafted += amount;
        this.onCrafting(stack);
    }

    @Override
    protected void onCrafting(ItemStack stack) {
        if (this.amountCrafted > 0) {
            stack.onCrafting(this.entityPlayer.world, this.entityPlayer, this.amountCrafted);
        }
        this.amountCrafted = 0;
    }

    @Override
    public ItemStack onTake(EntityPlayer thePlayer, ItemStack stack) {
        System.out.println("on take");
        return super.onTake(thePlayer, stack);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return true;
    }


}
