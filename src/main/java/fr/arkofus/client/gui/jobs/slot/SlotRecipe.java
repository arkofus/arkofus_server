package fr.arkofus.client.gui.jobs.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 17/11/2018 à 16:29
 * par Jullian Dorian
 */
public class SlotRecipe extends Slot {

    public SlotRecipe(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public int getSlotStackLimit() {
        return 200;
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return true;
    }
}
