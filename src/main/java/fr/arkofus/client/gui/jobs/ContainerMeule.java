package fr.arkofus.client.gui.jobs;

import fr.arkofus.client.gui.inventory.slot.SlotBase;
import fr.arkofus.client.gui.jobs.slot.SlotRecipe;
import fr.arkofus.client.gui.jobs.slot.SlotRecipeResult;
import fr.arkofus.player.inventory.ArkofusInventory;
import fr.arkofus.player.inventory.RecipeInventory;
import fr.arkofus.player.inventory.ResultCraftInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;

/**
 * Class créée le 17/11/2018 à 15:48
 * par Jullian Dorian
 */
public class ContainerMeule extends Container {

    private RecipeInventory recipeInventory;
    private ResultCraftInventory resultCraftInventory;
    private ArkofusInventory arkofusInventory;

    public ContainerMeule(ArkofusInventory arkofusInventory, EntityPlayer entityPlayer){
        this.arkofusInventory = arkofusInventory;
        this.arkofusInventory.openInventory(entityPlayer);
        this.recipeInventory = new RecipeInventory(this);
        this.resultCraftInventory = new ResultCraftInventory();

        this.bindInventory();
        this.bindRecipe(entityPlayer);
    }

    private void bindRecipe(EntityPlayer entityPlayer) {
        for(int i = 0; i < 8; i++){
            this.addSlotToContainer(new SlotRecipe(recipeInventory, i, 50 + (18 * i), 50));
        }

        this.addSlotToContainer(new SlotRecipeResult(entityPlayer, recipeInventory, resultCraftInventory, 0, 80, 80));
    }

    private void bindInventory() {

        for(int i = 0; i < this.arkofusInventory.getSizeInventory();){
            for(int j = 0; j < 6; j++, i++){
                this.addSlotToContainer(new SlotBase(arkofusInventory, i, 250 + (18 *  j), 50 + (18 * (i / 6))));
            }
        }

    }

    @Override
    public void onContainerClosed(EntityPlayer playerIn) {
        this.arkofusInventory.closeInventory(playerIn);
    }

    @Override
    public void onCraftMatrixChanged(IInventory inventoryIn) {
        System.out.println("Matrix changed afficher le resultat");
    }

    /**
     * Determines whether supplied player can use this container
     *
     * @param playerIn
     */
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return !playerIn.isSpectator();
    }
}
