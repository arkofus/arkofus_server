package fr.arkofus.client.gui.inventory;

import fr.arkofus.Constants;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

/**
 * Class créée le 18/08/2018 à 11:47
 * par Jullian Dorian
 */
@SideOnly(Side.CLIENT)
public class GuiArkofusInventory extends GuiContainer {

    private final ResourceLocation INVENTORY = new ResourceLocation(Constants.MODID, "textures/gui/container/inventory_player.png");

    private IInventory inventory;

    private int oldMouseY;
    private int oldMouseX;

    public static int kamas;

    public GuiArkofusInventory(IInventory arkofusInventory, EntityPlayer entityPlayer) {
        super(new ContainerArkofusInventory(arkofusInventory, entityPlayer));
        this.inventory = arkofusInventory;

        this.xSize = 512 / 2;
        this.ySize = 512 / 2;

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.oldMouseX = mouseX;
        this.oldMouseY = mouseY;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.fontRenderer.drawString(I18n.format(inventory.getName()), 216/2, 13/2, 0xfde43a);
        //Draw the kamas of entity
        this.fontRenderer.drawString("Kamas", 52/2, 278/2, 0xfde43a);
    }

    /**
     * Draws the background layer of this container (behind the items).
     *
     * @param partialTicks
     * @param mouseX
     * @param mouseY
     */
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(INVENTORY);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0,0, this.xSize, this.ySize);

    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
    }

    //Client
    private void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY, EntityLivingBase ent) {

    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}
