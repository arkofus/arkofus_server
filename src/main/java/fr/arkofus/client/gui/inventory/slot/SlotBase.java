package fr.arkofus.client.gui.inventory.slot;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 18/08/2018 à 11:57
 * par Jullian Dorian
 */
public class SlotBase extends Slot {

    public SlotBase(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return true;
    }

    @Override
    public int getSlotStackLimit() {
        return 200;
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return getSlotStackLimit();
    }
}
