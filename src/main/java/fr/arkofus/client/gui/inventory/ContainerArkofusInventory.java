package fr.arkofus.client.gui.inventory;

import fr.arkofus.client.gui.inventory.slot.SlotArmor;
import fr.arkofus.client.gui.inventory.slot.SlotBase;
import fr.arkofus.item.ArmorType;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 18/08/2018 à 11:48
 * par Jullian Dorian
 */
public class ContainerArkofusInventory extends Container {

    protected IInventory arkofusInventory;

    public ContainerArkofusInventory(IInventory arkofusInventory, EntityPlayer entityPlayer){
        this.arkofusInventory = arkofusInventory;
        arkofusInventory.openInventory(entityPlayer);

        bindHotbar();
        bindInventory();
        bindArmor();
    }

    /**
     * Place les 6 slots de la hotbar custom
     */
    private void bindHotbar(){

        for(int x = 0; x < 6; x++){
            this.addSlotToContainer(new SlotBase(arkofusInventory, x, (150 + (x*36))/2, 438/2));
        }

    }

    /**
     * Place tous les slots de l'inventaire (hors armures)
     */
    private void bindInventory(){
        //Inventaire bas custom
        //x43, y317
        //6->41
        for(int y = 0; y < 3; y++){
            for(int x = 0; x < 12; x++){
                this.addSlotToContainer(new SlotBase(arkofusInventory, 6 + x + y * 12, (43 + (x * 36)) / 2, (317 + (y * 36)) / 2));
            }
        }

        //x295,y70
        //42->71
        for(int y = 0; y < 6; y++){
            for(int x = 0; x < 5; x++){
                this.addSlotToContainer(new SlotBase(arkofusInventory, 42 + x + y * 5, (295 + (x * 36)) / 2, (70 + (y * 36)) / 2));
            }
        }
    }

    /**
     * Place tous les slots d'armures & d'armes
     */
    private void bindArmor(){
        //Amu
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.AMULETTE, 72, 22/2, 62/2));

        //Anneau
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.ANNEAU, 73, 22/2, 104/2));
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.ANNEAU, 74, 22/2, 146/2));

        //Bouclier
        //--Dernier index
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.BOUCLIER, 86, 22 / 2, 188/2));
        //Arme
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.ARME, 75, 64/2, 188/2));

        //Chapeau
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.CHAPEAU, 76, 232/2, 62/2));
        //Cape
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.CAPE, 77, 232/2, 104/2));
        //Ceinture
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.CEINTURE, 78, 232 /2, 146/2));
        //Botte
        this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.BOTTE, 79, 232 /2, 188/2));

        //Dofus
        for(int x = 0; x < 6; ++x){
            this.addSlotToContainer(new SlotArmor(arkofusInventory, ArmorType.DOFUS, 80 + x, (22 +(x * 42)) /2, 230/2));
        }

    }

    /**
     * Called when the container is closed.
     *
     * @param playerIn
     */
    @Override
    public void onContainerClosed(EntityPlayer playerIn) {
        this.arkofusInventory.closeInventory(playerIn);
    }

    @Override
    public boolean getCanCraft(EntityPlayer player) {
        return super.getCanCraft(player);
    }

    /**
     * Determines whether supplied player can use this container
     *
     * @param playerIn
     */
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }
}
