package fr.arkofus.client.gui.inventory.slot;

import fr.arkofus.item.ArmorType;
import fr.arkofus.item.base.IItemBase;
import fr.arkofus.item.base.armor.ItemArmor;
import fr.arkofus.item.base.ItemBase;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 18/08/2018 à 12:17
 * par Jullian Dorian
 */
public class SlotArmor extends SlotBase {

    private ArmorType type;

    public SlotArmor(IInventory inventoryIn, ArmorType type, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
        this.type = type;
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }

    /**
     * Check if the stack is allowed to be placed in this slot, used for armor slots as well as furnace fuel.
     *
     * @param stack
     */
    @Override
    public boolean isItemValid(ItemStack stack) {
        Item item = stack.getItem();

        if(item instanceof IItemBase){
            ArmorType itemType = ((IItemBase) item).getTypeArmor();
            return itemType != ArmorType.UNAUTHORIZED && this.type == itemType;
        }

        return false;
    }


}
