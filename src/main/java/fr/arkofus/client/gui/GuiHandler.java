package fr.arkofus.client.gui;

import fr.arkofus.client.gui.inventory.ContainerArkofusInventory;
import fr.arkofus.client.gui.inventory.GuiArkofusInventory;
import fr.arkofus.client.gui.jobs.ContainerMeule;
import fr.arkofus.player.inventory.ArkofusInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

/**
 * Class créée le 28/07/2018 à 09:36
 * par Jullian Dorian
 */
public class GuiHandler implements IGuiHandler {

    public static final int GUI_INVENTORY = 1;
    public static final int GUI_MEULE = 2;

    /**
     * Returns a Server side Container to be displayed to the user.
     *
     * @param ID     The Gui ID Number
     * @param player The player viewing the Gui
     * @param world  The current world
     * @param x      X Position
     * @param y      Y Position
     * @param z      Z Position
     * @return A GuiScreen/Container to be displayed to the user, null if none.
     */
    @Nullable
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID){
            case GUI_INVENTORY:
                return new ContainerArkofusInventory(new ArkofusInventory(player), player);
            case GUI_MEULE:
                return new ContainerMeule(new ArkofusInventory(player), player);
        }
        return null;
    }

    /**
     * Returns a Container to be displayed to the user. On the client side, this
     * needs to return a instance of GuiScreen On the server side, this needs to
     * return a instance of Container
     *
     * @param ID     The Gui ID Number
     * @param player The player viewing the Gui
     * @param world  The current world
     * @param x      X Position
     * @param y      Y Position
     * @param z      Z Position
     * @return A GuiScreen/Container to be displayed to the user, null if none.
     */
    @Nullable
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        //Check client side
        switch (ID){
            case GUI_INVENTORY:
                return new GuiArkofusInventory(new ArkofusInventory(player), player);
        }
        return null;
    }
}
