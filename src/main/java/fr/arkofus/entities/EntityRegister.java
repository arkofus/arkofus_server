package fr.arkofus.entities;

import fr.arkofus.ArkofusServer;
import fr.arkofus.Constants;
import fr.arkofus.entities.entity.EntityPNJ;
import fr.arkofus.entities.entity.bouftou.EntityBouftonOrageux;
import fr.arkofus.entities.spell.EntityCoinsThrowing;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;

/**
 * Class créée le 30/07/2018 à 22:43
 * par Jullian Dorian
 */
public class EntityRegister {

    public static void commonInitialization(){
        int id = 0;

        createEntity("boufton_orageux", id++, EntityBouftonOrageux.class, new Color(0, 0, 0).getRGB(), new Color(128, 212,255).getRGB());

        createEntity("coins_throwing", id++, EntityCoinsThrowing.class, 64, 10, true);

        createEntity("_pnj", id++, EntityPNJ.class, 32, 10, false);
    }

    @SideOnly(Side.CLIENT)
    public static void clientInitialization(){}

    /**
     * Create a new Entity
     * @param name Name of the entity
     * @param id The id of the entity
     * @param mobClass The class of the Entity
     * @param colorPrimary The first color
     * @param colorSecondary The second color
     * @param <T>
     */
    private static <T extends Entity> void createEntity(String name, int id, Class<T> mobClass, int colorPrimary, int colorSecondary){
        EntityRegistry.registerModEntity(new ResourceLocation(Constants.MODID, name), mobClass, name, id, ArkofusServer.INSTANCE, 40, 1, true,
                colorPrimary, colorSecondary);
    }

    /**
     * Create a new Entity
     * @param name Name of the entity
     * @param id The id of the entity
     * @param mobClass The class of the Entity
     * @param <T>
     */
    private static <T extends Entity> void createEntity(String name, int id, Class<T> mobClass, int trackingRange, int updateFrequency, boolean sendVelocity){
        EntityRegistry.registerModEntity(new ResourceLocation(Constants.MODID, name), mobClass, name, id, ArkofusServer.INSTANCE, trackingRange, updateFrequency, sendVelocity);
    }

    /**
     * Register model to the entity
     * @param entity Enter the class Entity to register this
     * @param render Enter the render from the Entity
     * @param <T>
     */
    @SideOnly(Side.CLIENT)
    private static <T extends Entity> void registerModel(Class<T> entity, Render<? extends Entity> render){
        RenderingRegistry.registerEntityRenderingHandler(entity, render);
    }


}
