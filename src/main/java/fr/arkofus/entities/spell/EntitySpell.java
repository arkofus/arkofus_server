package fr.arkofus.entities.spell;

import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.spell.Damage;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.utils.Math;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 03/08/2018 à 13:39
 * par Jullian Dorian
 */
public abstract class EntitySpell extends EntityThrowable {

    protected ISpell<? extends EntitySpell> spell;

    public EntitySpell(World world){
        super(world);
    }

    public EntitySpell(World world, EntityPlayer entityPlayer, ISpell<? extends EntitySpell> spell){
        super(world, entityPlayer);
        spell.prepare(entityPlayer, ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID()));
        this.spell = spell;
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if(!spell.isInvocation()) {
            if (result.entityHit != null) {
                if (result.entityHit instanceof ArkofusEntity) {
                    ArkofusEntity entityHit = (ArkofusEntity) result.entityHit;
                    ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(((EntityPlayer) getThrower()).getUniqueID());

                    System.out.println("--Dommage par défaut");
                    int index = ThreadLocalRandom.current().nextInt(0, spell.getDamages().size());
                    boolean isCritical = ThreadLocalRandom.current().nextFloat() <= spell.getCritical();
                    Damage damage = spell.getDamages().get(index);
                    float damageSpell = !isCritical ? damage.getDamage() : damage.getDamageCritical();
                    System.out.println("Damage : " + damageSpell);

                    System.out.println("--Ajout des effets");
                    double element = arkofusPlayer.getPrimary(damage.getType()).getTotal();
                    System.out.println("Element : " + element);

                    System.out.println("--Resistances");
                    double res = entityHit.getFinalResistance(damage.getType());
                    System.out.println(res);
                    double resFixe = entityHit.getFinalResistanceFixe(damage.getType());
                    System.out.println(resFixe);

                    System.out.println("--Final damage");
                    float finalDamage = Math.clamp(damageSpell * (1+(arkofusPlayer.getPuissance() + element)/100) +
                            (arkofusPlayer.getDamage() + arkofusPlayer.getDamageElement(damage.getType()) + (isCritical?arkofusPlayer.getDamageCritical():0) * (1+((-res + resFixe)/100))));
                    System.out.println("Final : " + finalDamage);

                    entityHit.attackEntityFrom(DamageSource.GENERIC, finalDamage);
                } else {
                    result.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, getThrower()), 100.0f);
                }

                this.setDead();
            }
        }
    }
}
