package fr.arkofus.entities.spell;

import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.packet.client.CPacketParticle;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;

/**
 * Class créée le 31/07/2018 à 11:50
 * par Jullian Dorian
 */
public class EntityCoinsThrowing extends EntitySpell{

    public EntityCoinsThrowing(World worldIn) {
        super(worldIn);
    }

    public EntityCoinsThrowing(World world, EntityPlayer entityPlayer, ISpell<? extends EntitySpell> spell){
        super(world, entityPlayer, spell);
    }

    @Override
    public void onUpdate() {
        super.onUpdate();

        Entity thrower = getThrower();

        if(thrower != null) {

            if (thrower != null && thrower instanceof EntityPlayer) {
                CPacketParticle particlePacket = new CPacketParticle(EnumParticleTypes.FIREWORKS_SPARK, this.posX, this.posY, this.posZ, 0, 0, 0);
                //RegistryPacket.getNetwork().sendToAll(particlePacket);
                //System.out.println("Display particle server");
            } else {
                System.out.println("Thrower null or not entityplayer setted");
            }

        }

        if(spell != null) {
            if (ticksExisted >= spell.getMaxReach()) {
                setDead();
            }
        } else {
            setDead();
            System.out.println("Instant dead");
        }

        System.out.println("tick : " + ticksExisted);

    }

    @Override
    protected float getGravityVelocity() {
        return 0.00f;
    }
}
