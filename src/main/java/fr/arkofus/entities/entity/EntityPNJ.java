package fr.arkofus.entities.entity;

import fr.arkofus.quest.Quest;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 24/11/2018 à 12:17
 * par Jullian Dorian
 */
public class EntityPNJ extends EntityCreature {

    private List<Quest> quests;

    public EntityPNJ(World worldIn) {
        super(worldIn);
    }

    public EntityPNJ(World worldIn, String name, List<Quest> quests){
        super(worldIn);
        this.setCustomNameTag(name);
        this.quests = quests;
    }

    @Override
    public boolean hasCustomName() {
        return true;
    }

    @Override
    public ITextComponent getDisplayName() {
        return super.getDisplayName();
    }

}
