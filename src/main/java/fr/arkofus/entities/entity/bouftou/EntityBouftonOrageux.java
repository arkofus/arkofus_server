package fr.arkofus.entities.entity.bouftou;


import fr.arkofus.characteristic.ElementType;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkas on 27/09/2017.
 * at 14:39
 */
public class EntityBouftonOrageux extends ArkofusEntity {

    public EntityBouftonOrageux(World worldIn) {
        super(worldIn, "Boufton Orageux");
        this.setSize(0.9f, 0.9f);

        this.setLevel(3, 7);
        this.setExperience(145, 42);
        this.setVitalite(21, 3.75f);
        this.setMovementSpeed(2);

        this.setCoefficientResistance(ElementType.NEUTRE, 1.0f);
        this.setCoefficientResistance(ElementType.TERRE, 1.5f);
        this.setCoefficientResistance(ElementType.FEU, 1.0f);
        this.setCoefficientResistance(ElementType.EAU, 1.0f);
        this.setCoefficientResistance(ElementType.AIR, 0.5f);
        this.setResistance(ElementType.NEUTRE, -3.0f);
        this.setResistance(ElementType.TERRE, -10.0f);
        this.setResistance(ElementType.FEU, -12.0f);
        this.setResistance(ElementType.EAU, -12.0f);
        this.setResistance(ElementType.AIR, -2.0f);
    }

    @Override
    public float getEyeHeight(){
        return 0.450F;
    }

    @Override
    public ArkofusEntity getNewInstance(World world) {
        return new EntityBouftonOrageux(world);
    }

    @Override
    public List<ItemStack> getLoots(ArkofusPlayer player) {
        List<ItemStack> itemStacks = new ArrayList<>();
        int prospection = player.getProspection();

        //   50 - 54%
        ItemStack laine_celeste = getDropItem(Item.getItemFromBlock(Blocks.WOOL), prospection, 50, true, 1, 4);

        //   50 - 54%
        ItemStack bave_bouftou = getDropItem(Items.LEATHER, prospection, 50, true, 1, 4);

        //   50 - 54%
        //ItemStack viande_intangible = canDropItem(prospection, 50, 1) ? new ItemStack(Items.COOKED_BEEF) : ItemStack.EMPTY;
        //if(!bave_bouftou.isEmpty()) bave_bouftou.setCount(rand.nextInt(1 + 4));

        itemStacks.add(laine_celeste);
        itemStacks.add(bave_bouftou);
        return itemStacks;
    }
}
