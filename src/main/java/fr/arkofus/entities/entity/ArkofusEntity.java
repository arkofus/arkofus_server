package fr.arkofus.entities.entity;

import fr.arkofus.ArkofusServer;
import fr.arkofus.characteristic.ElementType;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.server.zone.Zone;
import fr.arkofus.utils.Math;
import fr.arkofus.utils.SpecialString;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 21/07/2018 à 17:18
 * par Jullian Dorian
 */
public abstract class ArkofusEntity extends EntityMob {

    private static final DataParameter<Integer> LEVEL = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);

    private static final DataParameter<Integer> EXPERIENCE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_EXPERIENCE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Float> BASE_VITALITE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> VITALITE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_VITALITE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_FIXE_NEUTRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_FIXE_NEUTRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> RES_NEUTRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_RES_NEUTRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_FIXE_TERRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_FIXE_TERRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> RES_TERRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_RES_TERRE = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_FIXE_FEU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_FIXE_FEU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> RES_FEU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_RES_FEU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_FIXE_EAU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_FIXE_EAU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> RES_EAU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_RES_EAU = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_FIXE_AIR = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_FIXE_AIR = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> RES_AIR = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);
    private static final DataParameter<Float> COEF_RES_AIR = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    private static final DataParameter<Integer> RES_CRIT = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.VARINT);
    private static final DataParameter<Float> COEF_RES_CRIT = EntityDataManager.createKey(ArkofusEntity.class, DataSerializers.FLOAT);

    /* Le nom de l'entité */
    private String entityName;
    /* Le nom de tag de l'entité */
    private String nameTag;

    private int levelMin, levelMax;

    /* C'est la zone ou a spawn l'entité */
    @SideOnly(Side.SERVER)
    private Zone zone = null;

    protected ThreadLocalRandom random = ThreadLocalRandom.current();

    public ArkofusEntity(World worldIn) {
        super(worldIn);
    }

    protected ArkofusEntity(World world, String name){
        super(world);

        this.entityName = name;
        this.nameTag = "";
    }

    public abstract List<ItemStack> getLoots(ArkofusPlayer arkofusPlayer);

    @Override
    public void onUpdate() {
        super.onUpdate();
    }

    @Override
    protected void entityInit() {
        super.entityInit();

        this.dataManager.register(LEVEL, 1);

        this.dataManager.register(EXPERIENCE, 1);
        this.dataManager.register(COEF_EXPERIENCE, 1.0f);

        this.dataManager.register(BASE_VITALITE, 20.0f);
        this.dataManager.register(VITALITE, 20.0f);
        this.dataManager.register(COEF_VITALITE, 1.0f);

        this.dataManager.register(RES_FIXE_NEUTRE, 0);
        this.dataManager.register(COEF_RES_FIXE_NEUTRE, 0.0f);
        this.dataManager.register(RES_NEUTRE, 0.0f);
        this.dataManager.register(COEF_RES_NEUTRE, 0.0f);

        this.dataManager.register(RES_FIXE_TERRE, 0);
        this.dataManager.register(COEF_RES_FIXE_TERRE, 0.0f);
        this.dataManager.register(RES_TERRE, 0.0f);
        this.dataManager.register(COEF_RES_TERRE, 0.0f);

        this.dataManager.register(RES_FIXE_FEU, 0);
        this.dataManager.register(COEF_RES_FIXE_FEU, 0.0f);
        this.dataManager.register(RES_FEU, 0.0f);
        this.dataManager.register(COEF_RES_FEU, 0.0f);

        this.dataManager.register(RES_FIXE_EAU, 0);
        this.dataManager.register(COEF_RES_FIXE_EAU, 0.0f);
        this.dataManager.register(RES_EAU, 0.0f);
        this.dataManager.register(COEF_RES_EAU, 0.0f);

        this.dataManager.register(RES_FIXE_AIR, 0);
        this.dataManager.register(COEF_RES_FIXE_AIR, 0.0f);
        this.dataManager.register(RES_AIR, 0.0f);
        this.dataManager.register(COEF_RES_AIR, 0.0f);

        this.dataManager.register(RES_CRIT, 0);
        this.dataManager.register(COEF_RES_CRIT, 0.0f);
    }

    @Override
    protected void initEntityAI(){
        super.initEntityAI();
        int priority = 0;
        this.tasks.addTask(priority++, new EntityAIWander(this, 1.3000054d));
        this.tasks.addTask(priority++, new EntityAILookIdle(this));
        this.tasks.addTask(priority++, new EntityAIWatchClosest(this, EntityPlayer.class, 6));
    }

    @Override
    public boolean isAIDisabled(){
        return false;
    }

    @Override
    protected void applyEntityAttributes() {
        //Enregistrement des attributs
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25d);
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(8d);
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(15d);

        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(1);
        this.setHealth(this.getMaxHealth());
    }

    @SideOnly(Side.SERVER)
    public void setZone(Zone zone) {
        this.zone = zone;
    }

    @SideOnly(Side.SERVER)
    public Zone getZone() {
        return zone;
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);

        compound.setInteger("Level", this.getLevel());

        compound.setInteger("Experience", this.getBaseExperience());
        compound.setFloat("CoefExperience", this.getCoefExperience());

        compound.setFloat("BaseVitalite", this.getBaseVitalite());
        compound.setFloat("Vitalite", this.getVitalite());
        compound.setFloat("CoefVitalite", this.getCoefVitalite());

        compound.setInteger("ResFixeNeutre",    MathHelper.floor(this.getResistanceFixe(ElementType.NEUTRE)));
        compound.setFloat("CoefResFixeNeutre", this.getCoefficientResistanceFixe(ElementType.NEUTRE));
        compound.setFloat("ResNeutre",         this.getResistance(ElementType.NEUTRE));
        compound.setFloat("CoefResNeutre",     this.getCoefficientResistance(ElementType.NEUTRE));

        compound.setInteger("ResFixeTerre",     MathHelper.floor(this.getResistanceFixe(ElementType.TERRE)));
        compound.setFloat("CoefResFixeTerre",  this.getCoefficientResistanceFixe(ElementType.TERRE));
        compound.setFloat("ResTerre",          this.getResistance(ElementType.TERRE));
        compound.setFloat("CoefResTerre",      this.getCoefficientResistance(ElementType.TERRE));

        compound.setInteger("ResFixeFeu",       MathHelper.floor(this.getResistanceFixe(ElementType.FEU)));
        compound.setFloat("CoefResFixeFeu",    this.getCoefficientResistanceFixe(ElementType.FEU));
        compound.setFloat("ResFeu",            this.getResistance(ElementType.FEU));
        compound.setFloat("CoefResFeu",        this.getCoefficientResistance(ElementType.FEU));

        compound.setInteger("ResFixeEau",       MathHelper.floor(this.getResistanceFixe(ElementType.EAU)));
        compound.setFloat("CoefResFixeEau",    this.getCoefficientResistanceFixe(ElementType.EAU));
        compound.setFloat("ResEau",            this.getResistance(ElementType.EAU));
        compound.setFloat("CoefResEau",        this.getCoefficientResistance(ElementType.EAU));

        compound.setInteger("ResFixeAir",       MathHelper.floor(this.getResistanceFixe(ElementType.AIR)));
        compound.setFloat("CoefResFixeAir",    this.getCoefficientResistanceFixe(ElementType.AIR));
        compound.setFloat("ResAir",            this.getResistance(ElementType.AIR));
        compound.setFloat("CoefResAir",        this.getCoefficientResistance(ElementType.AIR));

        compound.setInteger("ResCrit",         MathHelper.floor(this.getResistanceFixe(ElementType.CRITIQUE)));
        compound.setFloat("CoefResCrit",       this.getResistance(ElementType.CRITIQUE));
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);

        //this.setEntity();
        if(compound.hasKey("Level"))            this.setLevel(compound.getInteger("Level"));

        if(compound.hasKey("Experience"))       this.setExperience(compound.getInteger("Experience"), compound.getFloat("CoefExperience"));

        if(compound.hasKey("BaseVitalite"))     this.setBaseVitalite(compound.getFloat("BaseVitalite"));
        if(compound.hasKey("CoefVitalite"))     this.setCoefVitalite(compound.getFloat("CoefVitalite"));
        if(compound.hasKey("Vitalite"))         this.setVitalite(compound.getFloat("Vitalite"), true);

        if(compound.hasKey("ResFixeNeutre"))    this.setResistanceFixe(ElementType.NEUTRE, compound.getInteger("ResFixeNeutre"));
        if(compound.hasKey("CoefResFixeNeutre"))this.setCoefficientResistanceFixe(ElementType.NEUTRE, compound.getFloat("CoefResFixeNeutre"));
        if(compound.hasKey("ResNeutre"))        this.setResistance(ElementType.NEUTRE, compound.getFloat("ResNeutre"));
        if(compound.hasKey("CoefResNeutre"))    this.setCoefficientResistance(ElementType.NEUTRE, compound.getFloat("CoefResNeutre"));

        if(compound.hasKey("ResFixeTerre"))     this.setResistanceFixe(ElementType.TERRE, compound.getInteger("ResFixeTerre"));
        if(compound.hasKey("CoefResFixeTerre")) this.setCoefficientResistanceFixe(ElementType.TERRE, compound.getFloat("CoefResFixeTerre"));
        if(compound.hasKey("ResTerre"))         this.setResistance(ElementType.TERRE, compound.getFloat("ResTerre"));
        if(compound.hasKey("CoefResTerre"))     this.setCoefficientResistance(ElementType.TERRE, compound.getFloat("CoefResTerre"));

        if(compound.hasKey("ResFixeFeu"))       this.setResistanceFixe(ElementType.FEU, compound.getInteger("ResFixeFeu"));
        if(compound.hasKey("CoefResFixeFeu"))   this.setCoefficientResistanceFixe(ElementType.FEU, compound.getFloat("CoefResFixeFeu"));
        if(compound.hasKey("ResFeu"))           this.setResistance(ElementType.FEU, compound.getFloat("ResFeu"));
        if(compound.hasKey("CoefResFeu"))       this.setCoefficientResistance(ElementType.FEU, compound.getFloat("CoefResFeu"));

        if(compound.hasKey("ResFixeEau"))       this.setResistanceFixe(ElementType.EAU, compound.getInteger("ResFixeEau"));
        if(compound.hasKey("CoefResFixeEau"))   this.setCoefficientResistanceFixe(ElementType.EAU, compound.getFloat("CoefResFixeEau"));
        if(compound.hasKey("ResEau"))           this.setResistance(ElementType.EAU, compound.getFloat("ResEau"));
        if(compound.hasKey("CoefResEau"))       this.setCoefficientResistance(ElementType.EAU, compound.getFloat("CoefResEau"));

        if(compound.hasKey("ResFixeAir"))       this.setResistanceFixe(ElementType.AIR, compound.getInteger("ResFixeAir"));
        if(compound.hasKey("CoefResFixeAir"))   this.setCoefficientResistanceFixe(ElementType.AIR, compound.getFloat("CoefResFixeAir"));
        if(compound.hasKey("ResAir"))           this.setResistance(ElementType.AIR, compound.getFloat("ResAir"));
        if(compound.hasKey("CoefResAir"))       this.setCoefficientResistance(ElementType.AIR, compound.getFloat("CoefResAir"));

        if(compound.hasKey("ResCrit"))          this.setResistanceFixe(ElementType.CRITIQUE, compound.getInteger("ResCrit"));
        if(compound.hasKey("CoefResCrit"))      this.setCoefficientResistanceFixe(ElementType.CRITIQUE, compound.getFloat("CoefResCrit"));
    }

    @Override
    protected void damageEntity(DamageSource damageSrc, float damageAmount) {
        if(!this.isEntityInvulnerable(damageSrc)){
            damageAmount = ForgeHooks.onLivingHurt(this, damageSrc,damageAmount);
            if(damageAmount != 0.0f && isEntityAlive()){
                float vitalite = this.getVitalite();
                if(vitalite - damageAmount <= 0.0f){
                    //On tue le mob
                    this.setVitalite(0.0f);
                    this.setHealth(0.0f);

                    rewardEntity(damageSrc.getTrueSource());
                } else {
                    this.setVitalite(vitalite - damageAmount);
                    this.getCombatTracker().trackDamage(damageSrc, vitalite, damageAmount);
                    this.setAbsorptionAmount(this.getAbsorptionAmount() - damageAmount);
                }
            }
        }
    }

    @Override
    public void onDeath(DamageSource cause) {
        super.onDeath(cause);

        if(this.zone != null)
            this.zone.removeAndUpdate(this);
    }

    /**
     * <p>Gère le reward du joueur ayant tué l'entité</p>
     * @param entity
     */
    protected void rewardEntity(Entity entity){

        if(entity instanceof EntityPlayer){
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entity.getUniqueID());
            if(arkofusPlayer == null) return;

            //Ajout des items dans l'inventaire
            for(ItemStack itemStack : this.getLoots(arkofusPlayer)){
                arkofusPlayer.getInventory().addItemStackToInventory(itemStack);
                arkofusPlayer.getInventory().refreshNBT();
            }

            //Ajout de l'exp
            long rewardExp = MathHelper.lfloor(getRewardExperience() * ((1000+arkofusPlayer.getSagesse().getTotal())/1000));
            arkofusPlayer.addExperience(rewardExp);
            ArkofusServer.getMapPlayers().replace(entity.getUniqueID(), arkofusPlayer);
            arkofusPlayer.synchronise();
        }
    }

    /**
     * @see #getDropItem(Item, int, double, boolean, int, int, int) - Permet de définir si un item est lootable ou non.
     */
    protected ItemStack getDropItem(Item item, int prospection, double itemDropBase, boolean levelAffected, int minQuantity, int maxQuantity) {
        return getDropItem(item, prospection, itemDropBase, levelAffected, 1, minQuantity, maxQuantity);
    }

    /**
     * Effectue un random sur l'item et son taux de drop, si celui est supérieur à son taux de base,
     * un nouveau random est effectue sur la quantité de l'objet;
     * @param item - L'item à dropé
     * @param prospection - La prospection du joueur
     * @param itemDropBase - La probabilité de drop l'item de base
     * @param levelAffected - Aucune idée
     * @param coefficient - Le coefficient entre les différents stase de drop
     * @param minQuantity - La quantité minimum de l'item
     * @param maxQuantity - La quantité maximum de l'item
     * @return item
     */
    protected ItemStack getDropItem(Item item, int prospection, double itemDropBase, boolean levelAffected, int coefficient, int minQuantity, int maxQuantity) {
        if (item == null) return ItemStack.EMPTY;

        //drop = 50 + ((level - levelMIn) * 1)
        double random = this.random.nextDouble(0, 100);

        //On vérifie si le joueur peux le drop ( dans le pourcentage )
        if (levelAffected) itemDropBase = (itemDropBase + (getLevel() - this.levelMin)) * coefficient;
        boolean hasDrop = random <= (itemDropBase + (prospection / 1000D));

        ItemStack itemDrop = hasDrop ? new ItemStack(item) : ItemStack.EMPTY;
        if (hasDrop) {
            int qtt = this.random.nextInt(maxQuantity + (prospection / 100));
            if (qtt == 0) qtt += minQuantity;
            itemDrop.setCount(qtt);
        }

        return itemDrop;
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
    public void onLivingUpdate() {
        super.onLivingUpdate();
    }

    /**
     * <p>Retourne une nouvelle instance de l'entité pour éviter que l'UUID soit le même ou autres problèmes.</p>
     * @param world - Le monde ou l'entité doit apparaître
     * @return instance - Nouvelle instance
     */
    public abstract ArkofusEntity getNewInstance(World world);

    /**
     * Récupère la vitesse de mouvement de l'entité
     * @return movementSpeed
     */
    public double getMovementSpeed() {
        return this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue();
    }

    /**
     * Définis la rapidité du mob à bouger
     * @param movementSpeed - La valeur en points de mouvement sur dofus
     */
    protected void setMovementSpeed(double movementSpeed) {
        final double speed = 0.05 + (0.05 * movementSpeed);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(speed);
    }

    /* ---------------------- *
    *       NIVEAUX           *
     -------------------------*/

    /**
     * <p>Définis le niveau de l'entité</p>
     * @param levelMin - Niveau minimum
     * @param levelMax - Niveau maximum
     */
    protected void setLevel(int levelMin, int levelMax){
        this.levelMin = levelMin;
        this.levelMax = levelMax;

        int level = random.nextInt(levelMin, levelMax+1);

        this.dataManager.set(LEVEL, level);
    }

    /**
     * <p>Définis directement le niveau de l'entité</p>
     * @param level
     */
    private void setLevel(int level){
        this.dataManager.set(LEVEL, level);
    }

    public int getLevel() {
        return this.dataManager.get(LEVEL);
    }

    /**
     * <p>Retourne le niveau d'ecart</p>
     * @return ecartLevel
     */
    public int getEcartLevel(){
        return getLevel() - this.levelMin;
    }

    /* ---------------------- *
    *       EXPERIENCE        *
     -------------------------*/

    /**
     * <p>Définis l'experience que le joueur gagnera</p>
     * @param baseExperience - l'experience de base pour l'entité
     * @param coefExperience - Le coefficient de l'experience
     */
    protected void setExperience(int baseExperience, float coefExperience){
        this.setBaseExperience(baseExperience);
        this.setCoefExperience(coefExperience);
    }

    private void setBaseExperience(int baseExperience){
        this.dataManager.set(EXPERIENCE, baseExperience);
    }

    /**
     * <p>Retourne l'experience de l'entité</p>
     * @return experience
     */
    private int getBaseExperience(){
        return this.dataManager.get(EXPERIENCE);
    }

    /**
     * <p>Définis le coefficient de l'experience</p>
     * @param coefExperience - Le coefficient
     */
    protected void setCoefExperience(float coefExperience){
        this.dataManager.set(COEF_EXPERIENCE, coefExperience);
    }

    /**
     * <p>Retourne le coefficient de l'experience</p>
     * @return coefExperience
     */
    private float getCoefExperience(){
        return this.dataManager.get(COEF_EXPERIENCE);
    }

    /**
     * <p>Retour l'experience que le joueur va pouvoir gagner</p>
     * @return rewardExperience
     */
    public int getRewardExperience(){
        return Math.round(getBaseExperience() + (getCoefExperience() * getEcartLevel()));
    }

    /* ---------------------- *
    *        VITALITE         *
     -------------------------*/

    /**
     * <p>Définis la vitalité de l'entité</p>
     * @param baseVitalite - Base de la vitalité
     * @param coef - Coefficient de la vitalité
     */
    protected void setVitalite(float baseVitalite, float coef){
        this.setCoefVitalite(coef);
        this.setBaseVitalite(baseVitalite);
        this.setVitalite(baseVitalite + (coef * getEcartLevel()));
    }

    private void setVitalite(float vitalite){
        setVitalite(vitalite, false);
    }

    private void setVitalite(float vitalite, boolean resetMax){
        float max_vitalite = getMaxVitalite();
        this.dataManager.set(VITALITE, (resetMax ? max_vitalite : vitalite));
    }

    private void setBaseVitalite(float baseVitalite){
        this.dataManager.set(BASE_VITALITE, baseVitalite);
    }

    /**
     * <p>Retourne la base de la vitalité de l'entité</p>
     * @return baseVitalite
     */
    protected float getBaseVitalite(){
        return this.dataManager.get(BASE_VITALITE);
    }

    /**
     * <p>Retourne la vitalité de l'entité</p>
     * @return vitalite
     */
    public float getVitalite(){
        return this.dataManager.get(VITALITE);
    }

    /**
     * <p>Retourne la vitalité maximum que l'entité peut avoir</p>
     * @return maxVitalite
     */
    public float getMaxVitalite(){
        return (getBaseVitalite() + (getCoefVitalite() * getEcartLevel()));
    }

    public float getCoefVitalite(){
        return this.dataManager.get(COEF_VITALITE);
    }

    protected void setCoefVitalite(float coefVitalite){
        this.dataManager.set(COEF_VITALITE, coefVitalite);
    }

    /* ---------------------- *
    *        RESISTANCES      *
     -------------------------*/

    /**
     * Retourne la resistance donner en paramètres, cela ne s'applique qu'au resistances
     * en pourcentages.
     * @see #getResistance(ElementType)
     * @see #getCoefficientResistance(ElementType)
     * @see #getEcartLevel()
     * @param element - Enum class
     * @return double
     */
    public double getFinalResistance(ElementType element){
        return (this.getResistance(element) + (this.getCoefficientResistance(element) * getEcartLevel()));
    }

    /**
     * Retourne la resistance fixe de l'entité avec la resistance donner en paramètres.
     * @see #getResistanceFixe(ElementType)
     * @see #getCoefficientResistanceFixe(ElementType)
     * @see #getEcartLevel()
     * @param element - Enum class
     * @return double
     */
    public double getFinalResistanceFixe(ElementType element){
        return (this.getResistanceFixe(element) + (this.getCoefficientResistanceFixe(element) * getEcartLevel()));
    }

    /**
     * Définis la nouvelle resistance (pourcentage)
     * @param element - La resistance à modifié
     * @param value - La nouvelle valeur
     */
    protected void setResistance(ElementType element, float value){
        switch (element){
            case NEUTRE:
                this.dataManager.set(RES_NEUTRE, value);
                break;
            case TERRE:
                this.dataManager.set(RES_TERRE, value);
                break;
            case FEU:
                this.dataManager.set(RES_FEU, value);
                break;
            case EAU:
                this.dataManager.set(RES_EAU, value);
                break;
            case AIR:
                this.dataManager.set(RES_AIR, value);
                break;
        }
    }

    /**
     * Récupère la resistance via le dataManager
     * @param element - La resistance à récupérer
     * @return double - dataManager
     */
    private float getResistance(ElementType element){
        switch (element){
            case NEUTRE:
                return this.dataManager.get(RES_NEUTRE);
            case TERRE:
                return this.dataManager.get(RES_TERRE);
            case FEU:
                return this.dataManager.get(RES_FEU);
            case EAU:
                return this.dataManager.get(RES_EAU);
            case AIR:
                return this.dataManager.get(RES_AIR);
            default:
                return 0.0f;
        }
    }

    /**
     * Définis la nouvelle resistance fixe en passant
     * par le dataManager.
     * @param element - La resistance fixe à modifier
     * @param value - La nouvelle valeur
     */
    protected void setResistanceFixe(ElementType element, int value){

        switch (element){
            case NEUTRE:
                this.dataManager.set(RES_FIXE_NEUTRE, value);
                break;
            case TERRE:
                this.dataManager.set(RES_FIXE_TERRE, value);
                break;
            case FEU:
                this.dataManager.set(RES_FIXE_FEU, value);
                break;
            case EAU:
                this.dataManager.set(RES_FIXE_EAU, value);
                break;
            case AIR:
                this.dataManager.set(RES_FIXE_AIR, value);
                break;
            case CRITIQUE:
                this.dataManager.set(RES_CRIT, value);
        }

    }

    /**
     * Retourne la valeur de la resistance du dataManager.
     * @param element - la resistance fixe à récupérer.
     * @return int - resistance datamanager
     */
    private int getResistanceFixe(ElementType element){
        switch (element){
            case NEUTRE:
                return this.dataManager.get(RES_FIXE_NEUTRE);
            case TERRE:
                return this.dataManager.get(RES_FIXE_TERRE);
            case FEU:
                return this.dataManager.get(RES_FIXE_FEU);
            case EAU:
                return this.dataManager.get(RES_FIXE_EAU);
            case AIR:
                return this.dataManager.get(RES_FIXE_AIR);
            case CRITIQUE:
                return this.dataManager.get(RES_CRIT);
            default:
                return 0;
        }
    }

    /* --
     Coefficient
     -- */

    /**
     * Définis la nouvelle resistance (pourcentage)
     * @param element - La resistance à modifié
     * @param value - La nouvelle valeur
     */
    protected void setCoefficientResistance(ElementType element, float value){
        switch (element){
            case NEUTRE:
                this.dataManager.set(COEF_RES_NEUTRE, value);
                break;
            case TERRE:
                this.dataManager.set(COEF_RES_TERRE, value);
                break;
            case FEU:
                this.dataManager.set(COEF_RES_FEU, value);
                break;
            case EAU:
                this.dataManager.set(COEF_RES_EAU, value);
                break;
            case AIR:
                this.dataManager.set(COEF_RES_AIR, value);
                break;
        }
    }

    /**
     * Récupère le coefficient de la resistance via le dataManager
     * @param element - La resistance à récupérer
     * @return double - dataManager
     */
    private float getCoefficientResistance(ElementType element){
        switch (element){
            case NEUTRE:
                return this.dataManager.get(COEF_RES_NEUTRE);
            case TERRE:
                return this.dataManager.get(COEF_RES_TERRE);
            case FEU:
                return this.dataManager.get(COEF_RES_FEU);
            case EAU:
                return this.dataManager.get(COEF_RES_EAU);
            case AIR:
                return this.dataManager.get(COEF_RES_AIR);
            default:
                return 0.0f;
        }
    }

    /**
     * Définis le nouveau coefficient de la resistance fixe en passant
     * par le dataManager.
     * @param element - La resistance fixe qui suiras le coefficient
     * @param value - La nouvelle valeur
     */
    protected void setCoefficientResistanceFixe(ElementType element, float value){
        switch (element){
            case NEUTRE:
                this.dataManager.set(COEF_RES_FIXE_NEUTRE, value);
                break;
            case TERRE:
                this.dataManager.set(COEF_RES_FIXE_TERRE, value);
                break;
            case FEU:
                this.dataManager.set(COEF_RES_FIXE_FEU, value);
                break;
            case EAU:
                this.dataManager.set(COEF_RES_FIXE_EAU, value);
                break;
            case AIR:
                this.dataManager.set(COEF_RES_FIXE_AIR, value);
                break;
            case CRITIQUE:
                this.dataManager.set(COEF_RES_CRIT, value);
                break;
        }
    }

    /**
     * Retourne la valeur coefficient de la resistance du dataManager.
     * @param element - la resistance fixe à récupérer.
     * @return int - resistance datamanager
     */
    private float getCoefficientResistanceFixe(ElementType element){
        switch (element){
            case NEUTRE:
                return this.dataManager.get(COEF_RES_FIXE_NEUTRE);
            case TERRE:
                return this.dataManager.get(COEF_RES_FIXE_TERRE);
            case FEU:
                return this.dataManager.get(COEF_RES_FIXE_FEU);
            case EAU:
                return this.dataManager.get(COEF_RES_FIXE_EAU);
            case AIR:
                return this.dataManager.get(COEF_RES_FIXE_AIR);
            case CRITIQUE:
                return this.dataManager.get(COEF_RES_CRIT);
            default:
                return 0.0f;
        }
    }

    /**
     * Retourne le nom de l'entité en fonction de la traduction
     * @return name - En fonction de la traduction ; si vide retourne entity.<entity_name>.name
     */
    @Nonnull
    @Override
    public String getName(){
        String s = EntityList.getEntityString(this);

        if (s == null) {
            s = "generic";
        }

        return I18n.translateToLocal("entity." + s + ".name");
    }

    /**
     * Get the formatted ChatComponent that will be used for the sender's username in chat
     */
    @Nonnull
    @Override
    public ITextComponent getDisplayName() {
        TextComponentString textcomponentstring = new TextComponentString(this.getCustomNameTag());
        textcomponentstring.getStyle().setHoverEvent(this.getHoverEvent());
        textcomponentstring.getStyle().setInsertion(this.getCachedUniqueIdString());
        return textcomponentstring;
    }

    /**
     * Retourne le nom sur la tête de l'entité
     * @return customNameTag
     */
    @Nonnull
    @Override
    public String getCustomNameTag() {
        if(!entityName.equals("")){
            this.nameTag = TextFormatting.WHITE + entityName +
                    TextFormatting.WHITE + " - " +
                    TextFormatting.GOLD + "Lv. " +
                    TextFormatting.YELLOW + getLevel();
            return nameTag;
        }
        return "";
    }

    @Override
    public boolean getAlwaysRenderNameTagForRender() {
        return true;
    }

    @Override
    public boolean getAlwaysRenderNameTag() {
        return true;
    }

    @Override
    public boolean getCanSpawnHere() {
        return true;
    }

    @Override
    public String toString() {
        return new SpecialString(
                "name: " + getName(),
                "Vitalite: " + getVitalite(),
                "Experience: " + getRewardExperience()
        ).build();
    }
}
