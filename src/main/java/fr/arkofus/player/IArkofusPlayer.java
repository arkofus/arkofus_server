package fr.arkofus.player;

import fr.arkofus.characteristic.ElementType;
import fr.arkofus.characteristic.ICharacPrimary;
import fr.arkofus.characteristic.ICharacSecondary;
import fr.arkofus.characteristic.IPrimary;
import fr.arkofus.classe.IClasse;
import fr.arkofus.classe.spell.Damage;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.player.inventory.ArkofusInventory;
import fr.arkofus.player.jobs.Job;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.File;
import java.util.List;

/**
 * Class créée le 21/07/2018 à 17:22
 * par Jullian Dorian
 */
public interface IArkofusPlayer extends ICharacPrimary, ICharacSecondary {

    /**
     * <p>Retourne la classe du joueur</p>
     * @return classe
     */
    IClasse getClasse();

    /**
     * <p>Retourne l'inventaire du joueur</p>
     * @return inventory
     */
    ArkofusInventory getInventory();

    List<Job> getJobs();

    /**
     * <p>Retourne les kamas du joueur</p>
     * @return kamas
     */
    int getKamas();

    /**
     * <p>Verifie si le joueur est oméga</p>
     * @return true - Si le joueur est oméga
     */
    boolean isOmega();

    long getExperience();
    void setExperience(long value);
    long addExperience(long value);
    long removeExperience(long value);

    long getTotalExperience();
    void setTotalExperience(long value);
    long addTotalExperience(long value);

    default long getNeedExperience(int level){
        double result;

        if(level <= 50) {
            result =  (108.5 * level + 15 * Math.pow(level, 2.2) + 630 * Math.pow((level/20), 4));
            result = result * Math.pow(level, 0.88);
        } else if(level <= 100) {
            result =  (108.5 * level + 1.5 * Math.pow(level, 2.2) + 630 * Math.pow((level/20), 4));
            result = result * Math.pow(level, 1.09);
        } else if(level <= 150){
            result =  (108.5 * level + 1.5 * Math.pow(level, 2.2) + 630 * Math.pow((level/30), 4));
            result = result * Math.pow(level, 1.21);
        } else if(level <= 196){
            result = (225 * level + 1.5 * Math.pow(level, 2.2) + 630 * Math.pow((level/30), 4));
            result = result * Math.pow(level, 1.41);
        } else if(level <= 199) {
            result =  (270 * level + 1.5 * Math.pow(level, 2.2) + 630 * Math.pow((level/30), 4));
            result = result * Math.pow(level, 1.48);
        } else {
            result =  (550.5 * level + 1.5 * Math.pow(level, 2.2) + 630 * Math.pow((level/30), 4));
            result = result * Math.pow(level, 2.10);
        }

        return Math.round(result);
    }

    default long getNeedExperience(){
        return getNeedExperience(getLevel());
    }

    float getExperienceBar();

    /**
     * <p>Si le joueur est en combat</p>
     * @return true - Si il l'est
     */
    boolean isInFight();

    void setInFight(boolean isFight);

    /**
     * <p>Retourne les points naturel disponible.</p>
     * @return naturalPoints - Les points qui seront ajouté à un élement.
     */
    int getNaturalPoints();

    /**
     * <p>Ajoute un point</p>
     * @param value - Le montant à ajouter
     */
    void addNaturalPoints(double value);

    void removeNaturalPoints(double value);

    /**
     * <p>Retourne les points additionnels disponible.</p>
     * @return additionalPoints - Les points qui seront ajouté à un élement.
     */
    int getAdditionalPoints();

    /**
     * <p>Ajoute un point dans l'élement concerné.</p>
     * @param value - Le montant à ajouter
     */
    void addAdditionalPoints(double value);

    void removeAdditionalPoints(double value);

    /**
     * <p>Retourne les points de bonus</p>
     * @return bonusPoints
     */
    double getBonusPoints();

    /**
     * <p>Ajoute un point de bonus</p>
     * @param value - Les points à ajouter
     */
    void addBonusPoints(double value);

    /**
     * <p>Retourne la liste des invocations du joueur.</p>
     * @return invocations
     */
    List<ArkofusEntity> getInvocations();

    /**
     * <p>Retourne un Element primaire</p>
     * @param type enum class
     * @return primary
     */
    IPrimary getPrimary(ElementType type);

    /**
     * <p>Synchronise le joueur de l'autre côté</p>
     * <p>Client vers Serveur ; Serveur vers Client</p>
     */
    void synchronise();

    @SideOnly(Side.SERVER)
    File getFile();

}
