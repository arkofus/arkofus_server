package fr.arkofus.player.inventory;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.client.resources.I18n;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ReportedException;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 17/08/2018 à 11:33
 * par Jullian Dorian
 */
public class ArkofusInventory implements IInventory{

    /** An array of 72 item stacks indicating the main player inventory (including the visible bar). */
    private final NonNullList<ItemStack> mainInventory = NonNullList.<ItemStack>withSize(72, ItemStack.EMPTY);
    /** An Array of 6 spells (item) */
    private final NonNullList<ItemStack> spellInventory = NonNullList.<ItemStack>withSize(6, ItemStack.EMPTY);
    /** An array of 14 item stacks containing the currently worn armor pieces. */
    private final NonNullList<ItemStack> armorInventory = NonNullList.<ItemStack>withSize(14, ItemStack.EMPTY);
    private final NonNullList<ItemStack> offHandInventory = NonNullList.<ItemStack>withSize(1, ItemStack.EMPTY);

    private int startIndexMain = 0;
    private int startIndexArmor = mainInventory.size();
    private int endIndexArmor = startIndexArmor + armorInventory.size();
    private int indexHand = endIndexArmor;

    /** The index of the currently held item (0-8). */
    public int currentItem;
    /** The player whose inventory this is. */
    public EntityPlayer player;
    /** The stack currently held by the mouse cursor */
    private ItemStack itemStack;

    public String compound = "";
    public NBTTagCompound savedCompound = new NBTTagCompound();

    private boolean inventoryChanged;

    public ArkofusInventory(EntityPlayer entityPlayer){
        this.itemStack = ItemStack.EMPTY;
        this.player = entityPlayer;
    }
     /********************
     *       HOTBAR      *
     *********************/

    /**
     * Retourne le nombre possible de slot dans la hotbar
     * Contains : 6 of items, and 1 to weapon
     * @return hotbarSize
     */
    public int getHotbarSize(){
        return 7;
    }

    /**
     * Retourne l'item que le joueur a dans main
     * @return currentItem
     */
    public ItemStack getCurrentItem() {
        return itemStack;
    }

    /**
     * Set the current item on the slot
     * @param index
     */
    public void setCurrentItem(int index){
        this.currentItem = index;

        if(index == 0){
            //Return weapon
            this.itemStack = (armorInventory.get(3).isEmpty() ? ItemStack.EMPTY : armorInventory.get(3));
        } else {
            this.itemStack = (mainInventory.get(index - 1).isEmpty() ? ItemStack.EMPTY : mainInventory.get(index - 1));
        }

        if(player != null) {
            ItemStack itemStack1 = itemStack.copy();
            itemStack1.setCount(itemStack.getCount() > 64 ? 64 : itemStack.getCount());
            player.inventory.setInventorySlotContents(0, itemStack1);
            player.inventory.currentItem = 0;
        }

    }

    public boolean isHotbar(int index){
        return index >= 0 && index <= getHotbarSize();
    }


    /**
     * Switch the current item to the next one or the previous one
     */
    @SideOnly(Side.CLIENT)
    public void changeCurrentItem(int direction) {
        if (direction > 0) {
            direction = 1;
        }

        if (direction < 0) {
            direction = -1;
        }

        for (this.currentItem -= direction; this.currentItem < 0; this.currentItem += getHotbarSize()) {
            ;
        }

        while (this.currentItem >= getHotbarSize()) {
            this.currentItem -= getHotbarSize();
        }
        setCurrentItem(currentItem);
    }

    /**
     * Return the current slot index of the player
     * @return currentItem
     */
    public int getSlotIndex(){
        return this.currentItem;
    }

    /********************
    *     Inventaire    *
     *******************/

    @Override
    public void openInventory(EntityPlayer player) {
        ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(player.getUniqueID());
        if(arkofusPlayer != null){
            ArkofusInventory inventory = arkofusPlayer.getInventory();
            if(inventory != null && inventory.savedCompound.hasKey("Inventory")){
                this.readNBT(inventory.savedCompound.getTagList("Inventory", Constants.NBT.TAG_COMPOUND));
            }
        }
    }

    @Override
    public void closeInventory(EntityPlayer player) {
        NBTTagCompound nbtTagCompound = new NBTTagCompound();
        nbtTagCompound.setTag("Inventory", this.writeNBT());

        this.compound = nbtTagCompound.toString();
        this.savedCompound = nbtTagCompound;

        final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(player.getUniqueID());

        final ArkofusInventory arkofusInventory = this;

        if(arkofusInventory != null && arkofusPlayer!=null) {

            arkofusPlayer.setInventory(arkofusInventory);
            arkofusPlayer.save();
            arkofusPlayer.synchronise();

            ArkofusServer.getMapPlayers().replace(player.getUniqueID(), arkofusPlayer);
        }
    }

    /********************
    *       FUNCTIONS   *
     ********************/

    /**
     * Si les items peuvent fusionner
     * @return bool
     */
    public boolean canMergeStacks(ItemStack current, ItemStack add){
        return !current.isEmpty() && !add.isEmpty() &&
                this.stackEqualExact(current, add) &&
                current.isStackable() &&
                current.getCount() + add.getCount() <= current.getMaxStackSize() &&
                current.getCount() < getInventoryStackLimit();
    }

    /**
     * Checks item, NBT, and meta if the item is not damageable
     */
    private boolean stackEqualExact(ItemStack stack1, ItemStack stack2) {
        return stack1.getItem() == stack2.getItem() &&
                (!stack1.getHasSubtypes() || stack1.getMetadata() == stack2.getMetadata()) &&
                ItemStack.areItemStackTagsEqual(stack1, stack2);
    }


    /**
     * Ajoute un item dans l'inventaire, seulement dans l'inventaire principale.
     * Une fois ajouté reload les NBT
     */
    public boolean addItemStackToInventory(ItemStack itemStack) {

        if(itemStack.isEmpty()) return false;

        else {
            try {
                if(itemStack.isItemDamaged()){
                    int j = this.getFirstEmptyStack();

                    if(j != -1){
                        itemStack.setAnimationsToGo(5);
                        this.mainInventory.set(j, itemStack);
                        itemStack.setCount(0);
                        return true;
                    }
                    return false;
                } else {
                    int i;

                    while (true){
                        i = itemStack.getCount();
                        itemStack.setCount(this.storePartialItemStack(itemStack));
                        if(itemStack.isEmpty() || itemStack.getCount() >= i) {
                            break;
                        }
                    }

                    return itemStack.getCount() < i;
                }

            } catch (Throwable throwable) {
                CrashReport crashReport = CrashReport.makeCrashReport(throwable, "Add an item on Arkofus Inventory");
                CrashReportCategory category = crashReport.makeCategory("Add item");
                category.addCrashSection("Item ID", Item.getIdFromItem(itemStack.getItem()));
                category.addCrashSection("Item data", itemStack.getMetadata());
                category.addDetail("Item name", itemStack::getDisplayName);
                throw new ReportedException(crashReport);
            }
        }
    }

    private int storePartialItemStack(ItemStack itemStack) {
        Item item = itemStack.getItem();
        int i = itemStack.getCount();
        int j = this.storeItemStack(itemStack);

        if (j == -1) {
            j = this.getFirstEmptyStack();
        }

        if (j == -1) {
            return i;
        } else {
            ItemStack itemStack1 = this.getStackInSlot(j);

            if (itemStack1.isEmpty()) {
                itemStack1 = itemStack.copy(); // Forge: Replace Item clone above to preserve item capabilities when picking the item up.
                itemStack1.setCount(i);

                if (itemStack.hasTagCompound()) {
                    itemStack1.setTagCompound(itemStack.getTagCompound().copy());
                }

                this.setInventorySlotContents(j, itemStack1);
            }

            int k = i;

            if (i > itemStack1.getMaxStackSize() - itemStack1.getCount()) {
                k = itemStack1.getMaxStackSize() - itemStack1.getCount();
            }

            if (k > this.getInventoryStackLimit() - itemStack1.getCount()) {
                k = this.getInventoryStackLimit() - itemStack1.getCount();
            }

            if (k == 0) {
                return i;
            } else {
                i = i - k;
                itemStack1.grow(k);
                itemStack1.setAnimationsToGo(5);
                return i;
            }
        }
    }

    /**
     * stores an itemstack in the users inventory
     * @return slot
     */
    private int storeItemStack(ItemStack itemStackIn) {
        if (this.canMergeStacks(this.getStackInSlot(this.currentItem), itemStackIn)) {
            return this.currentItem;
        } else if (this.canMergeStacks(this.getStackInSlot(this.indexHand), itemStackIn)) {
            return this.indexHand;
        } else {
            for (int i = 0; i < this.mainInventory.size(); i++) {
                if (this.canMergeStacks((ItemStack)this.mainInventory.get(i), itemStackIn)) {
                    return i;
                }
            }

            return -1;
        }
    }

    public int getFirstEmptyStack(){
        for(int i = 0 ; i < mainInventory.size(); i++){
            ItemStack itemStack = mainInventory.get(i);
            if(itemStack.isEmpty())
                return i;
        }
        return -1;
    }

    public NBTTagList writeNBT(){
        NBTTagList nbtTagList = new NBTTagList();

        for(int i = 0; i < mainInventory.size(); i++){
            ItemStack itemStack = mainInventory.get(i);
            if(!itemStack.isEmpty()){
                NBTTagCompound compound = new NBTTagCompound();
                compound.setByte("Slot", (byte) i);
                itemStack.writeToNBT(compound);

                nbtTagList.appendTag(compound);
            }
        }

        for(int i = 0; i < armorInventory.size(); i++){
            ItemStack itemStack = armorInventory.get(i);
            if(!itemStack.isEmpty()){
                NBTTagCompound compound = new NBTTagCompound();
                //Le slot commencera à partir du début d'index de l'armure
                compound.setByte("Slot", (byte) (i + this.startIndexArmor));
                itemStack.writeToNBT(compound);


                nbtTagList.appendTag(compound);
            }
        }

        for(int i = 0; i < offHandInventory.size(); i++){
            ItemStack itemStack = offHandInventory.get(i);
            if(!itemStack.isEmpty()){
                NBTTagCompound compound = new NBTTagCompound();
                //Le slot commencera à partir du début d'index de la main
                compound.setByte("Slot", (byte) (i + this.indexHand));
                itemStack.writeToNBT(compound);

                nbtTagList.appendTag(compound);
            }
        }

        return nbtTagList;
    }

    public void readNBT(NBTTagList nbtTagList){
        clear();

        for(int i = 0; i < nbtTagList.tagCount(); i++){

            NBTTagCompound tagCompound = nbtTagList.getCompoundTagAt(i);
            int j = tagCompound.getByte("Slot") & 255;
            ItemStack itemStack = new fr.arkofus.utils.ItemStackHelper(tagCompound).getItemStack();

            this.setInventorySlotContents(j, itemStack.copy());

        }

        NBTTagCompound save = new NBTTagCompound();
        save.setTag("Inventory", nbtTagList);
        this.savedCompound = save;
        this.compound = savedCompound.toString();

    }

    /**
     * Effectue une lecture de l'inventaire, sauvegarde les compounds & le string.
     * Fini par une écriture de l'inventaire et sauvegarde l'ancienne lecture
     */
    public void refreshNBT(){

        NBTTagCompound compound = new NBTTagCompound();
        //On écrit l'inventaire
        compound.setTag("Inventory", this.writeNBT());
        //---------
        this.compound = compound.toString();
        this.savedCompound = compound;
        //---------
        NBTTagList nbtTagList = this.savedCompound.getTagList("Inventory", Constants.NBT.TAG_COMPOUND);
        this.readNBT(nbtTagList);

    }

    /********************
    *       INVENTORY   *
     ********************/

    /**
     * Returns the number of slots in the inventory.
     */
    @Override
    public int getSizeInventory() {
        return mainInventory.size() + armorInventory.size() + offHandInventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(ItemStack itemStack : mainInventory){
            if(itemStack.isEmpty())
                return true;
        }
        return false;
    }

    /**
     * Returns the stack in the given slot.
     *
     * @param index
     */
    @Override
    public ItemStack getStackInSlot(int index) {
        switch (getPartWithIndex(index)){
            case MAIN:
                //System.out.println("Return main");
                return mainInventory.get(index);
            case ARMOR:
                //System.out.println("Return armor");
                return armorInventory.get(index - this.startIndexArmor);
            case HAND:
                //System.out.println("Return offhand");
                return offHandInventory.get(index - this.indexHand);
            default:
                return ItemStack.EMPTY;
        }
    }

    /**
     * Retourne la partie qui est associé à l'index.
     * @see Part
     * @param index
     * @return part
     */
    private Part getPartWithIndex(int index){
        if (index >= this.startIndexMain && index < this.startIndexArmor) {
            return Part.MAIN;
        } else if (index >= this.startIndexArmor && index < this.endIndexArmor) {
            return Part.ARMOR;
        } else if(index >= this.indexHand && index < this.indexHand + 1){
            return Part.HAND;
        } else {
            return Part.ERROR;
        }
    }

    private enum Part{
        MAIN,
        ARMOR,
        HAND,
        SPELL,
        ERROR
    }

    /**
     * Removes up to a specified number of items from an inventory slot and returns them in a new stack.
     *
     * @param index
     * @param count
     */
    @Override
    public ItemStack decrStackSize(int index, int count) {
        switch (getPartWithIndex(index)){
            case MAIN:
                return ItemStackHelper.getAndSplit(mainInventory, index, count);
            case ARMOR:
                return ItemStackHelper.getAndSplit(armorInventory, index - this.startIndexArmor, count);
            case HAND:
                return ItemStackHelper.getAndSplit(mainInventory, index - this.indexHand, count);
            default:
                return ItemStack.EMPTY;
        }
    }

    /**
     * Removes a stack from the given slot and returns it.
     *
     * @param index
     */
    @Override
    public ItemStack removeStackFromSlot(int index) {

        switch (getPartWithIndex(index)){
            case MAIN:
                ItemStack itemStack = mainInventory.get(index).copy();
                mainInventory.remove(index);
                return itemStack;
            case ARMOR:
                itemStack = armorInventory.get(index - this.startIndexArmor).copy();
                armorInventory.remove(index - this.startIndexArmor);
                return itemStack;
            case HAND:
                itemStack = offHandInventory.get(index - this.indexHand).copy();
                offHandInventory.remove(index- this.indexHand);
                return itemStack;
            default:
                return ItemStack.EMPTY;
        }

    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
     *
     * @param index
     * @param itemStack
     */
    @Override
    public void setInventorySlotContents(int index, ItemStack itemStack) {
        switch (getPartWithIndex(index)){
            case MAIN:
                this.mainInventory.set(index, itemStack.copy());
                break;
            case ARMOR:
                this.armorInventory.set(index - this.startIndexArmor, itemStack.copy());
                break;
            case HAND:
                this.offHandInventory.set(index - this.indexHand, itemStack.copy());
                break;
            default:
                break;
        }
    }

    /**
     * Returns the maximum stack size for a inventory slot. Seems to always be 64, possibly will be extended.
     */
    @Override
    public int getInventoryStackLimit() {
        return 200;
    }

    /**
     * For tile entities, ensures the chunk containing the tile entity is saved to disk later - the game won't think it
     * hasn't changed and skip it.
     */
    @Override
    public void markDirty(){
        this.inventoryChanged = true;
    }

    /**
     * Don't rename this method to canInteractWith due to conflicts with Container
     *
     * @param player
     */
    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return false;
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot. For
     * guis use Slot.isItemValid
     *
     * @param index
     * @param stack
     */
    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return true;
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {}

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
        mainInventory.clear();
        armorInventory.clear();
        offHandInventory.clear();
    }

    /**
     * Get the name of this object. For players this returns their username
     */
    @Override
    public String getName() {
        return "container.arkofus.inventory";
    }

    /**
     * Returns true if this thing is named
     */
    @Override
    public boolean hasCustomName() {
        return true;
    }

    /**
     * Get the formatted ChatComponent that will be used for the sender's username in chat
     */
    @Override
    public ITextComponent getDisplayName() {
        return new TextComponentString(TextFormatting.GOLD + I18n.format(getName()));
    }

}
