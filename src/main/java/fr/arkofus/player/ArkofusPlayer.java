package fr.arkofus.player;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.arkofus.ArkofusServer;
import fr.arkofus.characteristic.ElementType;
import fr.arkofus.characteristic.IPrimary;
import fr.arkofus.characteristic.Primary;
import fr.arkofus.classe.IClasse;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.json.adapter.ArkofusPlayerAdapter;
import fr.arkofus.packet.client.CPacketUpdateHealth;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.packet.client.CPacketSynchronise;
import fr.arkofus.packet.client.CPacketSetSlot;
import fr.arkofus.packet.client.CPacketWindowItems;
import fr.arkofus.player.inventory.ArkofusInventory;
import fr.arkofus.player.jobs.Job;
import fr.arkofus.player.jobs.JobMiner;
import fr.arkofus.utils.SpecialString;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraft.server.management.UserListOpsEntry;
import net.minecraft.util.IntHashMap;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Class créée le 21/07/2018 à 16:59
 * par Jullian Dorian
 */
public class ArkofusPlayer implements IArkofusPlayer, IContainerListener {

    /**
     * <p>L'entité du joueur qui est etendu.</p>
     */
    private EntityPlayer expended;

    /**
     * <p>La classe du joueur</p>
     */
    private IClasse classe;

    /**
     * <p>L'inventaire du joueur</p>
     */
    private ArkofusInventory inventory;

    /**
     * <p>Liste des métiers</p>
     */
    private List<Job> jobs;

    /** Les kamas du joueur */
    private int kamas;

    /**
     * <p>Le niveau du joueur</p>
     */
    private int level;

    /**
     * <p>Si le joueur est oméga.
     * Si c'est le cas le comportement du niveau et de l'exp seront différent.</p>
     */
    private boolean isOmega = false;

    private long experience;
    private long totalExperience;

    private boolean isInFight = false;

    private int naturalPoints;
    private int additionalPoints;

    /** Les points de vie du joueur */
    private double health;

    private IPrimary vitalite;
    private IPrimary sagesse;
    private IPrimary force;
    private IPrimary intelligence;
    private IPrimary agilite;
    private IPrimary chance;

    private int puissance;
    private float critital;
    private int care;
    private List<ArkofusEntity> invocations = new ArrayList<>();
    private int maxInvocation;
    private int prospection;

    private double damage;
    private double damageCritical;
    private double damageNeutre, damageTerre, damageFeu, damageAir, damageEau;
    private double thorn;
    private double damageTrap;
    private double masteryWeapon;
    private int puissanceTrap;
    private int resFixeNeutre, resFixeTerre,resFixeEau,resFixeFeu,resFixeAir;
    private float resNeutre, resTerre, resEau, resFeu, resAir;
    private int resCritical, resKnockback;

    private final IntHashMap<Short> pendingTransactions = new IntHashMap();

    public ArkofusPlayer(){
        this(null);
    }

    public ArkofusPlayer(EntityPlayer expended){
        this.expended = expended;
        this.inventory = new ArkofusInventory(expended);
        this.jobs = Arrays.asList(new JobMiner());
        this.level = getMinLevel();

        this.vitalite = new Primary();
            vitalite.addBonus(65);
        this.sagesse = new Primary();
        this.force = new Primary();
        this.intelligence = new Primary();
        this.chance = new Primary();
        this.agilite = new Primary();

        this.maxInvocation = 1;
    }

    public void setEntityPlayer(EntityPlayer entityPlayer) {
        this.expended = entityPlayer;
        this.inventory.player = entityPlayer;
    }

    public EntityPlayer getEntityPlayer() {
        return expended;
    }

    /**
     * <p>Défini la nouvelle classe du joueur</p>
     * @param classe - La nouvelle classe
     */
    public void setClasse(IClasse classe){
        this.classe = classe;
    }

    /**
     * <p>Retourne la classe du joueur</p>
     *
     * @return classe
     */
    @Override
    public IClasse getClasse() {
        return classe;
    }

    /**
     * <p>Retourne l'inventaire du joueur</p>
     *
     * @return inventory
     */
    @Override
    public ArkofusInventory getInventory() {
        return inventory;
    }

    public void setInventory(ArkofusInventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public List<Job> getJobs(){
        return jobs;
    }

    public void setJobs(List<Job> jobs){
        this.jobs = jobs;
    }

    /**
     * Retourne le métier selon le nom
     * @param name
     * @return job
     */
    public Job getJob(String name){
        for (Job j : this.jobs) {
            if(j.getName().equalsIgnoreCase(name))
                return j;
        }
        return null;
    }

    /**
     * <p>Retourne les kamas du joueur</p>
     *
     * @return kamas
     */
    @Override
    public int getKamas() {
        return kamas > 2000000000 ? 2000000000 : kamas;
    }

    public void setKamas(int kamas) {
        this.kamas = kamas;
    }

    /**
     * <p>Verifie si le joueur est oméga</p>
     *
     * @return true - Si le joueur est oméga
     */
    @Override
    public boolean isOmega() {
        return isOmega;
    }

    @Override
    public long getExperience() {
        return experience;
    }

    @Override
    public void setExperience(long value) {
        this.experience = value;
    }

    @Override
    public long addExperience(long value) {
        //Faire le code changement de level etc...

        addTotalExperience(value);

        if(getExperience() + value < getNeedExperience()){
            this.experience += value;

        } else if(getExperience() + value == getNeedExperience()){
            this.experience = 0;

            addLevel(1);
        } else {

            long overExperience = getExperience() + value;
            int addedLevel = 0;

            do {
                if(overExperience < getNeedExperience()) break;
                overExperience -= getNeedExperience();
                addedLevel++;

            } while (overExperience > getNeedExperience());

            setExperience(overExperience);
            addLevel(addedLevel);
        }

        return getExperience();
    }

    @Override
    public long removeExperience(long value) {
        //Idem

        return 0;
    }

    @Override
    public long getTotalExperience() {
        return totalExperience;
    }

    @Override
    public void setTotalExperience(long value) {
        this.totalExperience = value;
    }

    @Override
    public long addTotalExperience(long value) {
        return this.totalExperience += value;
    }

    @Override
    public float getExperienceBar() {
        return (float) (this.experience / getNeedExperience());
    }

    /**
     * <p>Si le joueur est en combat</p>
     *
     * @return true - Si il l'est
     */
    @Override
    public boolean isInFight() {
        return this.isInFight;
    }

    @Override
    public void setInFight(boolean inFight) {
        isInFight = inFight;
    }

    /**
     * <p>Retourne les points naturel disponible.</p>
     *
     * @return naturalPoints - Les points qui seront ajouté à un élement.
     */
    @Override
    public int getNaturalPoints() {
        return naturalPoints;
    }

    /**
     * <p>Défini par défaut les points naturels du joueur</p>
     * @param naturalPoints
     */
    public void setNaturalPoints(int naturalPoints){
        this.naturalPoints = naturalPoints;
    }

    /**
     * <p>Ajoute un point</p>
     *
     * @param value - Le montant à ajouter
     */
    @Override
    public void addNaturalPoints(double value) {
        this.naturalPoints += value;
    }

    @Override
    public void removeNaturalPoints(double value) {
        this.naturalPoints -= value;
    }

    /**
     * <p>Retourne les points additionnels disponible.</p>
     *
     * @return additionalPoints - Les points qui seront ajouté à un élement.
     */
    @Override
    public int getAdditionalPoints() {
        return additionalPoints;
    }

    /**
     * <p>Défini les points additionels qui seront ajouté</p>
     * @param additionalPoints
     */
    public void setAdditionalPoints(int additionalPoints){
        this.additionalPoints = additionalPoints;
    }

    /**
     * <p>Ajoute un point dans l'élement concerné.</p>
     *
     * @param value - Le montant à ajouter
     */
    @Override
    public void addAdditionalPoints(double value) {
        this.additionalPoints += value;
    }

    @Override
    public void removeAdditionalPoints(double value) {
        this.additionalPoints -= value;
    }

    /**
     * <p>Retourne les points de bonus</p>
     *
     * @return bonusPoints
     */
    @Override
    public double getBonusPoints() {
        return 0;
    }

    /**
     * <p>Ajoute un point de bonus</p>
     *
     * @param value - Les points à ajouter
     */
    @Override
    public void addBonusPoints(double value) {

    }

    /**
     * <p>Renvoie le niveau</p>
     *
     * @return level
     */
    @Override
    public int getLevel() {
        return level;
    }

    /**
     * <p>Définit le nouveau niveau</p>
     *
     * @param value - La nouvelle valeur qui sera le niveau
     */
    @Override
    public void setLevel(int value) {
        this.level = value;
        if(level > 200){
            isOmega = true;
        }
    }

    /**
     * <p>Ajoute un niveau</p>
     *
     * @param value - La quantité de niveau a ajouter
     * @return level - Retourne le niveau
     */
    @Override
    public int addLevel(int value) {
        this.level += value;

        addNaturalPoints(5);
        vitalite.addBonus(5);

        getEntityPlayer().sendMessage(new TextComponentString("Vous avez up !"));


        return getLevel();
    }

    /**
     * <p>Enlève un niveau</p>
     *
     * @param value - La quantité de niveau a enlever
     * @return level - Retourne le niveau
     */
    @Override
    public int removeLevel(int value) {
        return getLevel();
    }

    /**
     * <p>Défini le niveau minimum du level atteignable.</p>
     *
     * @param value - Le level min
     */
    @Deprecated
    @Override
    public void setMinLevel(int value) {}

    /**
     * <p>Défini le niveau maximum du level atteignable.</p>
     *
     * @param value - Le level max
     */
    @Deprecated
    @Override
    public void setMaxLevel(int value) {}

    public double getHealth() {
        return health;
    }

    public double getMaxHealth(){
        return getVitalite().getTotal();
    }

    public void setHealth(double health, boolean sendClient) {
        this.health = health;
        if(sendClient)
            RegistryPacket.getNetwork().sendTo(new CPacketUpdateHealth(health), (EntityPlayerMP) this.expended);
    }

    @Override
    public IPrimary getVitalite() {
        return vitalite;
    }

    @Override
    public IPrimary getSagesse() {
        return sagesse;
    }

    @Override
    public IPrimary getAgilite() {
        return agilite;
    }

    @Override
    public IPrimary getForce() {
        return force;
    }

    @Override
    public IPrimary getIntelligence() {
        return intelligence;
    }

    @Override
    public IPrimary getChance() {
        return chance;
    }

    /**
     * <p>Retourne la puissance (fixe)</p>
     *
     * @return puissance
     */
    @Override
    public int getPuissance() {
        return puissance;
    }

    /**
     * <p>Defini la puissance (fixe)</p>
     *
     * @param value - La nouvelle valeur
     */
    @Override
    public void setPuissance(int value) {
        this.puissance = value;
    }

    /**
     * <p>Augmente la probabilité de faire un coup critique, gérer sur 100%. 0 = 0% ; 1 = 100%</p>
     *
     * @return critical
     */
    @Override
    public float getCritical() {
        return critital;
    }

    /**
     * <p>Défini le nouveau pourcentage de faire un coup critique.</p>
     *
     * @param value - La nouvelle valeur
     */
    @Override
    public void setCritical(float value) {
        if(value>1)
            critital = 1f;
        else if(value<0)
            critital = 0f;
        else
            critital = value;
    }

    /**
     * <p>Retourne les soins (fixe)</p>
     *
     * @return care
     */
    @Override
    public int getCare() {
        return care;
    }

    @Override
    public void setCare(int value) {
        this.care = value;
    }

    /**
     * <p>Retourne la valeur d'invocation disponible.</p>
     *
     * @return maxInvocation
     */
    @Override
    public int getMaxInvocation() {
        return maxInvocation;
    }

    @Override
    public void setMaxInvocation(int value) {
        this.maxInvocation = value;
    }

    @Override
    public List<ArkofusEntity> getInvocations() {
        return invocations;
    }

    /**
     * <p>Synchronise le joueur de l'autre côté</p>
     * <p>Client vers Serveur ; Serveur vers Client</p>
     */
    @Override
    public void synchronise() {

        String toJson = this.toJson();
        CPacketSynchronise syncPlayerPacket = new CPacketSynchronise(toJson);

        System.out.println("Envoie depuis le server -- Synchronisation ");

        RegistryPacket.getNetwork().sendTo(syncPlayerPacket, (EntityPlayerMP) getEntityPlayer());
    }

    /**
     * <p>Retourne la prospection</p>
     *
     * @return prospection
     */
    @Override
    public int getProspection() {
        int chance = fr.arkofus.utils.Math.round(getChance().getTotal());

        int prospectionChance = fr.arkofus.utils.Math.round(chance / 10);

        return 100 + prospectionChance + prospection;
    }

    @Override
    public void setProspection(int value) {
        this.prospection = value;
    }

    /*
    * ---RESISTANCE ET DOMMAGES
     */

    @Override
    public double getDamage() {
        return damage;
    }

    @Override
    public void setDamage(double damage) {
        this.damage = damage;
    }

    @Override
    public double getDamageCritical() {
        return damageCritical;
    }

    @Override
    public void setDamageCritical(double damageCritical) {
        this.damageCritical = damageCritical;
    }

    @Override
    public double getDamageNeutre() {
        return damageNeutre;
    }

    @Override
    public void setDamageNeutre(double damageNeutre) {
        this.damageNeutre = damageNeutre;
    }

    @Override
    public double getDamageTerre() {
        return damageTerre;
    }

    @Override
    public void setDamageTerre(double damageTerre) {
        this.damageTerre = damageTerre;
    }

    @Override
    public double getDamageFeu() {
        return damageFeu;
    }

    @Override
    public void setDamageFeu(double damageFeu) {
        this.damageFeu = damageFeu;
    }

    @Override
    public double getDamageEau() {
        return damageEau;
    }

    @Override
    public void setDamageEau(double damageEau) {
        this.damageEau = damageEau;
    }

    @Override
    public double getDamageAir() {
        return damageAir;
    }

    @Override
    public void setDamageAir(double damageAir) {
        this.damageAir = damageAir;
    }

    @Override
    public double getThorn() {
        return thorn;
    }

    @Override
    public void setThorn(double thorn) {
        this.thorn = thorn;
    }

    @Override
    public double getMasteryWeapon() {
        return masteryWeapon;
    }

    @Override
    public void setMasteryWeapon(double masteryWeapon) {
        this.masteryWeapon = masteryWeapon;
    }

    /**
     * <p>Retourne les dommages d'un élement</p>
     *
     * @param type enum class
     * @return damage of element
     */
    @Override
    public double getDamageElement(ElementType type) {
        switch (type){
            case AIR:
                return getDamageAir();
            case EAU:
                return getDamageEau();
            case TERRE:
                return getDamageTerre();
            case FEU:
                return getDamageFeu();
            case CRITIQUE:
                return getDamageCritical();
            default:
                return 0.0d;
        }
    }

    /**
     * <p>Retourne les dommages fixes du piège</p>
     * <p>Augmente les dommages fixes du piège</p>
     *
     * @return damageTrap
     */
    @Override
    public double getDamageTrap() {
        return damageTrap;
    }

    @Override
    public void setDamageTrap(int damageTrap) {
        this.damageTrap = damageTrap;
    }

    /**
     * <p>Retourne la puissance des dommages du piege</p>
     * <p>Augmente la puissance du piege, 10 de puissance = 1 dommage fixe.</p>
     *
     * @return puissanceTrap
     */
    @Override
    public double getPuissanceTrap() {
        return puissanceTrap;
    }

    @Override
    public void setPuissanceTrap(int puissanceTrap) {
        this.puissanceTrap = puissanceTrap;
    }

    @Override
    public int getResFixeNeutre() {
        return resFixeNeutre;
    }

    @Override
    public void setResFixeNeutre(int resFixeNeutre) {
        this.resFixeNeutre = resFixeNeutre;
    }

    @Override
    public float getResNeutre() {
        return resNeutre;
    }

    @Override
    public void setResNeutre(float resNeutre) {
        this.resNeutre = resNeutre;
    }

    @Override
    public int getResFixeTerre() {
        return resFixeTerre;
    }

    @Override
    public void setResFixeTerre(int resFixeTerre) {
        this.resFixeTerre = resFixeTerre;
    }

    @Override
    public float getResTerre() {
        return resTerre;
    }

    @Override
    public void setResTerre(float resTerre) {
        this.resTerre = resTerre;
    }

    @Override
    public int getResFixeFeu() {
        return resFixeFeu;
    }

    @Override
    public void setResFixeFeu(int resFixeFeu) {
        this.resFixeFeu = resFixeFeu;
    }

    @Override
    public float getResFeu() {
        return resFeu;
    }

    @Override
    public void setResFeu(float resFeu) {
        this.resFeu = resFeu;
    }

    @Override
    public int getResFixeEau() {
        return resFixeEau;
    }

    @Override
    public void setResFixeEau(int resFixeEau) {
        this.resFixeEau = resFixeEau;
    }

    @Override
    public float getResEau() {
        return resEau;
    }

    @Override
    public void setResEau(float resEau) {
        this.resEau = resEau;
    }

    @Override
    public int getResFixeAir() {
        return resFixeAir;
    }

    @Override
    public void setResFixeAir(int resFixeAir) {
        this.resFixeAir = resFixeAir;
    }

    @Override
    public float getResAir() {
        return resAir;
    }

    @Override
    public void setResAir(float resAir) {
        this.resAir = resAir;
    }

    @Override
    public int getResCritical() {
        return resCritical;
    }

    @Override
    public void setResCritical(int resCritical) {
        this.resCritical = resCritical;
    }

    @Override
    public int getResKnockback() {
        return resKnockback;
    }

    @Override
    public void setResKnockback(int resKnockback) {
        this.resKnockback = resKnockback;
    }

    @Override
    public IPrimary getPrimary(ElementType type) {
        switch (type){
            case EAU:
                return getChance();
            case FEU:
                return getIntelligence();
            case TERRE:
            case NEUTRE:
                return getForce();
            case AIR:
                return getAgilite();
            default:
                return null;
        }
    }

    /**
     * <p>Renvoie le GSON personnalisé de la class</p>
     * @return gson
     */
    private static Gson getGson(){
        return new GsonBuilder()
                .registerTypeAdapter(ArkofusPlayer.class, new ArkofusPlayerAdapter())
                .setPrettyPrinting()
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .create();
    }

    /**
     * <p>Transforme le joueur </p>
     * @return string
     */
    public String toJson(){
        return getGson().toJson(this);
    }

    /**
     * <p>Convertir le texte json en ArkofusPlayer</p>
     * @param json - Le contenu en json
     * @return arkofusPlayer
     */
    public static ArkofusPlayer fromJson(String json){
        return (ArkofusPlayer) getGson().fromJson(json, ArkofusPlayer.class);
    }

    /**
     * <p>Sauvegarde le joueur dans son fichier du nom : UUID.json.
     * Stockée dans le répertoire : config/arkofus/players/</p>
     * @return true - Si la sauvegarde marche
     */
    public boolean save(){

        try {
            if(!getFile().exists())
                getFile().createNewFile();

            FileWriter fileWriter = new FileWriter(getFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(toJson());
            bufferedWriter.flush();

            bufferedWriter.close();
            fileWriter.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public File getFile() {

        if(expended == null)
            throw new NullPointerException("Impossible de récupérer le fichier car l'EntityPlayer est nulle.");

        return new File(ArkofusServer.modConfiguration.playersRoot, expended.getUniqueID().toString() + ".json");
    }

    @Override
    public String toString(){
        return new SpecialString(
          "Name: " + expended.getName(),
          "Classe: " + classe.getName(),
          "--Stats--",
          "Level : " + getLevel(),
          "Experience : " + getExperience(),
          "Total Experience : " + getTotalExperience(),
          "Experience Bar : " + getExperienceBar(),
          "Prospection : " + getProspection(),
          "Invocation : " + getMaxInvocation(),
          "Soins : " + getCare(),
          "--Primaire--",
          "Vitalité : " + getVitalite().getTotal(),
          "Sagesse : " + getSagesse().getTotal(),
          "Force : " + getForce().getTotal(),
          "Intelligence : " + getIntelligence().getTotal(),
          "Agilite : " + getAgilite().getTotal(),
          "Chance : " + getChance().getTotal(),
          "--Secondaires--"
        ).build();
    }

    /**
     * <p>Si le joueur est autorisé ou non à utilisé le sort</p>
     * @param spell - Le sort à utiliser
     * @return true - si le joueur peut l'utiliser
     */
    public boolean canUseSpell(ISpell<?> spell) {
        UserListOpsEntry userOp = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOppedPlayers().getEntry(getEntityPlayer().getGameProfile());
        int perm_op = userOp != null ? userOp.getPermissionLevel() : 0;

        if(perm_op < 4)
            return getClasse().getSpells().contains(spell.getName());

        return true;
    }

    @SideOnly(Side.SERVER)
    public void damage(double amount) {
        double health = this.health - amount;
        if(health <= 0) health = 0;
        setHealth(health, true);
        expended.setDead();
    }

    /**
     * update the crafting window inventory with the items in the list
     *
     * @param containerToSend
     * @param itemsList
     */
    @Override
    public void sendAllContents(Container containerToSend, NonNullList<ItemStack> itemsList) {
        RegistryPacket.getNetwork().sendTo(new CPacketWindowItems(containerToSend.windowId, itemsList), (EntityPlayerMP) expended);
        RegistryPacket.getNetwork().sendTo(new CPacketSetSlot(-1, -1, ItemStack.EMPTY), (EntityPlayerMP) expended);
    }

    /**
     * Sends the contents of an inventory slot to the client-side Container. This doesn't have to match the actual
     * contents of that slot.
     *
     * @param containerToSend
     * @param slotInd
     * @param stack
     */
    @Override
    public void sendSlotContents(Container containerToSend, int slotInd, ItemStack stack) {
        RegistryPacket.getNetwork().sendTo(new CPacketSetSlot(containerToSend.windowId, slotInd, stack), (EntityPlayerMP) expended);
    }

    /**
     * Sends two ints to the client-side Container. Used for furnace burning time, smelting progress, brewing progress,
     * and enchanting level. Normally the first int identifies which variable to update, and the second contains the new
     * value. Both are truncated to shorts in non-local SMP.
     *
     * @param containerIn
     * @param varToUpdate
     * @param newValue
     */
    @Override
    public void sendWindowProperty(Container containerIn, int varToUpdate, int newValue) {
        System.out.println("Send window property");
    }

    @Override
    public void sendAllWindowProperties(Container containerIn, IInventory inventory) {
        System.out.println("sendAllWindow Properties");
    }
}
