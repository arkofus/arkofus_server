package fr.arkofus.player.jobs;

import fr.arkofus.packet.client.CPacketUpdateJob;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;

/**
 * Class créée le 29/10/2018 à 12:54
 * par Jullian Dorian
 */
public abstract class Job implements IJob{

    protected int level;

    protected long experience;
    protected long totalExperience;

    public Job(){
        this(1, 0, 0);
    }

    public Job(int level, long experience, long totalExperience){
        this.level = level;
        this.experience = experience;
        this.totalExperience = totalExperience;
    }

    @Override
    public int getLevel() {
        return level;
    }

    public void setLevel(int level){
        this.level = level <= getMaxLevel() && level > 0 ? level : 1;
    }

    protected void addLevel(int level){
        this.level = this.level < getMaxLevel() ? this.level + level : getMaxLevel();
    }

    @Override
    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience){
        this.experience = experience;
    }

    @Override
    public long getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(long totalExperience){
        this.totalExperience = totalExperience;
    }

    public void giveExperience(long experience, EntityPlayer entityPlayer){

        long newExperience = getExperience() + experience;

        this.totalExperience += experience;

        if(newExperience < getNeedExperience()){
            setExperience(newExperience);
        } else if(newExperience == getNeedExperience()){
            setExperience(0);
            addLevel(1);
        } else {

            int addLevel = 0;

            do{
                if(newExperience < getNeedExperience()) break;
                newExperience -= getNeedExperience();
                addLevel++;
            }while (newExperience < getNeedExperience());

            setExperience(newExperience);
            addLevel(addLevel);

        }
        entityPlayer.sendMessage(new TextComponentString("Exp win : "+experience));
        RegistryPacket.getNetwork().sendTo(new CPacketUpdateJob(this), (EntityPlayerMP) entityPlayer);
    }

    @Override
    public long getNeedExperience(int level) {

        final long multiplier = 20;
        long ecart;
        long needExperience = 0;

        for(int x = 1; x <= level; x++){
            ecart = x * multiplier;
            needExperience += ecart;
        }

        return needExperience;
    }

}
