package fr.arkofus.player.jobs;

import fr.arkofus.Constants;
import net.minecraft.util.ResourceLocation;

/**
 * Class créée le 29/10/2018 à 12:56
 * par Jullian Dorian
 */
public class JobMiner extends Job {

    public JobMiner(){
        this(1, 0, 0);
    }

    public JobMiner(int level, long experience, long totalExperience) {
        super(level, experience, totalExperience);
    }

    @Override
    public String getName() {
        return "miner";
    }

    /**
     * Retourne l'icon du métier
     *
     * @return icon
     */
    @Override
    public ResourceLocation getIcon() {
        return new ResourceLocation(Constants.MODID, "textures/icon/icon_miner.png");
    }

}
