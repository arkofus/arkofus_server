package fr.arkofus.player.jobs;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 29/10/2018 à 12:47
 * par Jullian Dorian
 */
public interface IJob {

    /**
     * Retourne le nom du métier
     * @return name
     */
    String getName();

    /**
     * Retourne le niveau du métier
     * @return level
     */
    int getLevel();

    /**
     * Retourne l'experience du metier
     * @return experience
     */
    long getExperience();

    /**
     * L'experience total acquise
     * @return totalExperience
     */
    long getTotalExperience();

    /**
     * @see IJob#getNeedExperience(int) - level
     */
    default long getNeedExperience(){
        return getNeedExperience(getLevel());
    }

    /**
     * Retourne l'experience nécessaire pour le prochain niveau
     * @param level - le niveau
     * @return experienceNeed
     */
    long getNeedExperience(int level);

    /**
     * Retourne l'icon du métier
     * @return icon
     */
    @SideOnly(Side.CLIENT)
    ResourceLocation getIcon();

    /**
     * Retourne le niveau maximum du métier
     * @return maxLevel
     */
    default int getMaxLevel(){
        return 200;
    }
}
