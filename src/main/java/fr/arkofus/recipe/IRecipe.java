package fr.arkofus.recipe;

import net.minecraft.item.ItemStack;

/**
 * Class créée le 24/11/2018 à 14:30
 * par Jullian Dorian
 */
public interface IRecipe {

    /**
     * Retourne la matrice des itemStack à positionner
     * @return matrice
     */
    ItemStack[] getMatrice();

    /**
     * L'itemstack qui sera retourner dans le slot Result
     * @return result
     */
    ItemStack getResult();

    /**
     * Retourne le nom de la recette
     * @return name
     */
    String getName();

}
