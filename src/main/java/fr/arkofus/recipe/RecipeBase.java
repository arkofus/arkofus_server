package fr.arkofus.recipe;

import net.minecraft.item.ItemStack;

/**
 * Class créée le 24/11/2018 à 14:32
 * par Jullian Dorian
 */
public class RecipeBase implements IRecipe {

    private String name;
    private ItemStack result;
    private ItemStack[] itemStacks;

    public RecipeBase(String name, ItemStack result, ItemStack... itemStacks){
        this.name = name;
        this.result = result;
        this.itemStacks = new ItemStack[8];

        if(itemStacks.length > 8)
            throw new IndexOutOfBoundsException("You can't over index 8 of a recipe");

        for(int i =0;i < this.itemStacks.length; i++){
            if(i < itemStacks.length)
                this.itemStacks[i] = itemStacks[i].copy();
            else
                this.itemStacks[i] = ItemStack.EMPTY;
        }
    }

    /**
     * Retourne la matrice des itemStack à positionner
     *
     * @return matrice
     */
    @Override
    public ItemStack[] getMatrice() {
        return this.itemStacks;
    }

    /**
     * Retourne le nom de la recette
     *
     * @return name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * L'itemstack qui sera retourner dans le slot Result
     *
     * @return result
     */
    @Override
    public ItemStack getResult() {
        return this.result;
    }
}
