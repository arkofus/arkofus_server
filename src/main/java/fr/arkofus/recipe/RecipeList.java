package fr.arkofus.recipe;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 24/11/2018 à 14:40
 * par Jullian Dorian
 */
public class RecipeList {

    private static List<IRecipe> recipes = new ArrayList<>();

    static {
        addRecipe(new RecipeBase("empty", ItemStack.EMPTY, ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY,ItemStack.EMPTY));
    }

    public static void addRecipe(String name, ItemStack result, ItemStack... matrice){
        addRecipe(new RecipeBase(name, result, matrice));
    }

    private static void addRecipe(IRecipe recipe){
        recipes.add(recipe);
    }

    public static List<IRecipe> getRecipes() {
        return recipes;
    }

    public static IRecipe getRecipeByName(String name){
        for(IRecipe recipe : getRecipes()){
            if(recipe.getName().equals(name))
                return recipe;
        }
        return getRecipes().get(0);
    }

    public static IRecipe getRecipeByResult(ItemStack result, boolean get_count){
        for(IRecipe recipe : getRecipes()){
            ItemStack recipeResult = recipe.getResult();
            if(get_count) {
                if (recipeResult.isItemEqual(result) && recipeResult.getCount() == result.getCount())
                    return recipe;
            }else{
                if (recipeResult.isItemEqual(result))
                    return recipe;
            }
        }
        return getRecipes().get(0);
    }

}
