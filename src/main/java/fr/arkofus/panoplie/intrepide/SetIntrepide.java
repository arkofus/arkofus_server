package fr.arkofus.panoplie.intrepide;

import fr.arkofus.panoplie.SetBase;

/**
 * Class créée le 13/11/2018 à 16:36
 * par Jullian Dorian
 */
public class SetIntrepide extends SetBase {

    public SetIntrepide(){
        super("Intrépide", 8);
    }

}
