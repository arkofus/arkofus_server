package fr.arkofus.panoplie;

import fr.arkofus.item.base.IItemBase;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.NonNullList;

/**
 * Class créée le 12/11/2018 à 10:45
 * par Jullian Dorian
 */
public abstract class SetBase {

    protected final String name;
    protected final NonNullList<Item> items;

    public SetBase(String name, int nbItems){
        this.name = name;
        this.items = NonNullList.withSize(nbItems, Items.AIR);
    }

    /**
     * Fil the all items on the panoplie
     * @param items
     * @return this
     */
    public SetBase fill(Item... items){
        for(int i = 0; i < items.length; i++) {
            add(i, items[i]);
        }
        for(Item item : this.items) {
            if(item instanceof IItemBase)
                ((IItemBase) item).setSet(this);
        }
        return this;
    }

    public boolean containsItem(Item item){
        if(item instanceof IItemBase){
            for(Item item1 : this.items){
                if(item == item1)
                    return true;
            }
        }
        return false;
    }

    /**
     * <p>Retourne le nom de la panoplie</p>
     * @return name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Add an Item on the panoplie
     * @param item
     */
    protected void add(int i, Item item){
        this.items.set(i, item);
    }

    /**
     * Returns the items of the Set
     * @return items
     */
    public Item[] getItems() {
        Item[] items = new Item[this.items.size()];

        for(int i = 0; i < this.items.size(); i++)
            items[i] = this.items.get(i);

        return items;
    }
}
