package fr.arkofus;

import fr.arkofus.classe.RegistryClasse;
import fr.arkofus.classe.spell.RegistrySpell;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.proxy.CommonProxy;
import fr.arkofus.server.ModConfiguration;
import fr.arkofus.server.zone.Zone;
import fr.arkofus.server.zone.ZoneManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

import java.io.File;
import java.util.*;

/**
 * Class créée le 21/07/2018 à 15:30
 * par Jullian Dorian
 */
@Mod(modid = Constants.MODID, name = Constants.NAME, version = Constants.VERSION, acceptedMinecraftVersions = Constants.ACCEPTED_VERSIONS)
public class ArkofusServer {

    @Mod.Instance(Constants.MODID)
    public static ArkofusServer INSTANCE;

    @SidedProxy(serverSide = Constants.SERVER_SIDE)
    public static CommonProxy proxy;

    /**
     * Retourne la liste des entités spawn
     */
    public static Set<ArkofusEntity> arkofusEntities = new HashSet<>();

    private static Map<UUID, ArkofusPlayer> players = new HashMap<>();

    private static File configurationPath = null;
    public static File getConfigurationPath() {
        return configurationPath;
    }

    public static ModConfiguration modConfiguration;
    public static RegistrySpell registrySpell;
    public static RegistryClasse registryClasse;

    private static ZoneManager zoneManager;

    @Mod.EventHandler
    public void onPreInitialization(FMLPreInitializationEvent event){

        proxy.onPreInitialization(event);

        modConfiguration = new ModConfiguration(event.getModConfigurationDirectory());
        modConfiguration.preInitialization();

        registrySpell = new RegistrySpell();
        registrySpell.preInitialization();

        registryClasse = new RegistryClasse();
        registryClasse.preInitialization();
    }

    @Mod.EventHandler
    public void onInitialization(FMLInitializationEvent event){

        proxy.onInitialization(event);

        RegistryPacket registryPacket = new RegistryPacket(NetworkRegistry.INSTANCE.newSimpleChannel(Constants.MODID));
        registryPacket.initialization();
    }

    @Mod.EventHandler
    public void onPostInitialization(FMLPostInitializationEvent event){

        proxy.onPostInitialization(event);

    }

    @Mod.EventHandler
    public void onServerStarting(FMLServerStartingEvent event){

        proxy.serverStarting(event);

        zoneManager = new ZoneManager();
        zoneManager.serverStarting();

    }

    /**
     * Retourne la liste des Joueurs pour clé l'UUID
     * @return players
     */
    public static Map<UUID, ArkofusPlayer> getMapPlayers() {
        return players;
    }

    /**
     * Retourne la zone Manager
     * @return zoneManager
     */
    public static ZoneManager getZoneManager() {
        return zoneManager;
    }
}
