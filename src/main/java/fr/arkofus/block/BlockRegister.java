package fr.arkofus.block;

import fr.arkofus.Constants;
import fr.arkofus.block.decoration.*;
import fr.arkofus.block.jobs.miner.MinerBlockRegister;
import fr.arkofus.item.ItemRegister;
import fr.arkofus.item.base.armor.ItemBoot;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemBlockSpecial;
import net.minecraft.item.ItemSlab;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 29/10/2018 à 14:37
 * par Jullian Dorian
 */
public class BlockRegister {

    public static final BlockGrass GRASS_INCARNAM = new BlockGrass("grass_incarnam");
    public static final BlockSlabGrassHalf SLAB_GRASS_INCARNAM_HALF = new BlockSlabGrassHalf("slab_grass_incarnam_half");
    public static final BlockSlabGrassDouble SLAB_GRASS_INCARNAM_DOUBLE = new BlockSlabGrassDouble("slab_grass_incarnam_double");

    public static final BlockLog LOG_BLUE = new BlockLog("log_blue");
    public static final BlockLog LOG_ROSE = new BlockLog("log_rose");

    public static final BlockLeaves LEAVES_BLUE = new BlockLeaves("leaves_blue");
    public static final BlockLeaves LEAVES_ROSE = new BlockLeaves("leaves_rose");

    public static final BlockTallGrass TALLGRASS_SMALL_YELLOW = new BlockTallGrass("tallgrass_small_yellow");
    public static final BlockTallGrass TALLGRASS_MEDIUM_YELLOW = new BlockTallGrass("tallgrass_medium_yellow");

    public static final BlockRock STONE_INCARNAM = new BlockRock("stone_incarnam");

    public static final BlockSlabStoneHalf SLAB_STONE_INCARNAM_HALF = new BlockSlabStoneHalf("slab_stone_incarnam_half");
    public static final BlockSlabStoneDouble SLAB_STONE_INCARNAM_DOUBLE = new BlockSlabStoneDouble("slab_stone_incarnam_double");

    public static final BlockRock COBBLESTONE_INCARNAM = new BlockRock("cobblestone_incarnam");

    public static final BlockSlabStoneHalf SLAB_COBBLESTONE_INCARNAM_HALF = new BlockSlabStoneHalf("slab_cobblestone_incarnam_half");
    public static final BlockSlabStoneDouble SLAB_COBBLESTONE_INCARNAM_DOUBLE = new BlockSlabStoneDouble("slab_cobblestone_incarnam_double");

    public static final BlockBench BENCH = new BlockBench("bench");
    public static final BlockStool STOOL = new BlockStool("stool");
    public static final BlockTable TABLE = new BlockTable("table");
    public static final BlockWheelBarrow WHEEL_BARROW = new BlockWheelBarrow("wheelbarrow");

    public static void commonPreInitialization(){
        registerBlock(GRASS_INCARNAM);
        registerBlock(LOG_BLUE);
        registerBlock(LOG_ROSE);
        registerBlock(STONE_INCARNAM);
        registerBlock(COBBLESTONE_INCARNAM);

        registerBlock(LEAVES_BLUE);
        registerBlock(LEAVES_ROSE);

        registerBlock(TALLGRASS_SMALL_YELLOW);
        registerBlock(TALLGRASS_MEDIUM_YELLOW);

        registerSlabBlock(SLAB_GRASS_INCARNAM_HALF, SLAB_GRASS_INCARNAM_DOUBLE);
        registerSlabBlock(SLAB_STONE_INCARNAM_HALF, SLAB_STONE_INCARNAM_DOUBLE);
        registerSlabBlock(SLAB_COBBLESTONE_INCARNAM_HALF, SLAB_COBBLESTONE_INCARNAM_DOUBLE);

        registerBlock(BENCH);
        registerBlock(STOOL);
        registerBlock(TABLE);
        registerBlock(WHEEL_BARROW);

        MinerBlockRegister.blocks.forEach(BlockRegister::registerBlock);
    }

    @SideOnly(Side.CLIENT)
    public static void clientPreInitialization(){
        MinerBlockRegister.blocks.forEach(BlockRegister::renderBlock);

        //renderBlock(SLAB_GRASS_INCARNAM_DOUBLE);
        //renderBlock(SLAB_GRASS_INCARNAM_HALF);
    }

    private static void registerBlock(Block block){
        GameRegistry.register(block, new ResourceLocation(Constants.MODID, block.getUnlocalizedName().substring(Constants.SUBSTRING)));
        ItemRegister.registerItemBlock(block);
    }

    @SideOnly(Side.CLIENT)
    private static void renderBlock(Block block){
        Item item = Item.getItemFromBlock(block);
        String resourceName = item.getUnlocalizedName();

        resourceName = Constants.MODID + ":" + resourceName.substring(Constants.SUBSTRING);
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(resourceName, "inventory"));
    }

    private static void registerSlabBlock(BlockSlab slab_half, BlockSlab slab_double){
        final ItemSlab itemBlock = new ItemSlab(slab_half, slab_half, slab_double);

        GameRegistry.register(slab_half, new ResourceLocation(Constants.MODID, slab_half.getUnlocalizedName().substring(Constants.SUBSTRING)));
        ItemRegister.registerItem(itemBlock);

        GameRegistry.register(slab_double, new ResourceLocation(Constants.MODID, slab_double.getUnlocalizedName().substring(Constants.SUBSTRING)));
    }

}
