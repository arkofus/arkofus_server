package fr.arkofus.block.decoration;

import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 20/11/2018 à 21:37
 * par Jullian Dorian
 */
public class BlockSlabGrassHalf extends BlockSlabBase {

    public BlockSlabGrassHalf(String name){
        super(name, Material.GRASS);
    }

    @Override
    public boolean isDouble() {
        return false;
    }
}
