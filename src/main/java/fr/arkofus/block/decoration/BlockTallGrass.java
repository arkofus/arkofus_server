package fr.arkofus.block.decoration;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

/**
 * Class créée le 20/11/2018 à 17:15
 * par Jullian Dorian
 */
public class BlockTallGrass extends BlockBush {

    public BlockTallGrass(String name) {
        super(name);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return new AxisAlignedBB(0d, 0d, 0d, 1d, 0.05d, 1d);
    }
}
