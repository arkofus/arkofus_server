package fr.arkofus.block.decoration;

import fr.arkofus.block.BlockBase;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;

/**
 * Class créée le 19/11/2018 à 19:40
 * par Jullian Dorian
 */
public class BlockLeaves extends BlockBase {

    public BlockLeaves(String name) {
        super(name, Material.LEAVES);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }
}
