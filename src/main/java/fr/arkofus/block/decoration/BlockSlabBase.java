package fr.arkofus.block.decoration;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 20/11/2018 à 21:17
 * par Jullian Dorian
 */
public abstract class BlockSlabBase extends net.minecraft.block.BlockSlab {

    protected ThreadLocalRandom random;

    public BlockSlabBase(String name, Material material){
        super(material);

        this.setUnlocalizedName(name);
        this.setBlockUnbreakable();
        this.setResistance(-1f);
        this.random = ThreadLocalRandom.current();

        IBlockState state = this.blockState.getBaseState();
        if(!this.isDouble()){
            this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
            state = state.withProperty(HALF, EnumBlockHalf.BOTTOM);
        }
        this.setDefaultState(state);
        this.useNeighborBrightness = true;
        this.disableStats();
        this.setSoundType(material == Material.ROCK || material == Material.BARRIER ? SoundType.STONE :
                material == Material.ANVIL ? SoundType.ANVIL :
                        material == Material.CACTUS || material == Material.GRASS || material == Material.LEAVES
                                || material == Material.VINE ? SoundType.PLANT :
                                material == Material.CARPET ? SoundType.CLOTH :
                                        material == Material.SAND ? SoundType.SAND :
                                                material == Material.WOOD ? SoundType.WOOD :
                                                        SoundType.STONE);
    }

    @Override
    public String getUnlocalizedName(int meta) {
        return this.getUnlocalizedName();
    }

    @Override
    public IProperty<?> getVariantProperty() {
        return HALF;
    }

    @Override
    public Comparable<?> getTypeForItem(ItemStack stack) {
        return EnumBlockHalf.BOTTOM;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        if(!this.isDouble())
            return this.getDefaultState().withProperty(HALF, EnumBlockHalf.values()[meta % EnumBlockHalf.values().length]);
        return this.getDefaultState();
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        if(this.isDouble()) return 0;
        return ((EnumBlockHalf) state.getValue(HALF)).ordinal() +1;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Items.AIR;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[]{HALF});
    }
}
