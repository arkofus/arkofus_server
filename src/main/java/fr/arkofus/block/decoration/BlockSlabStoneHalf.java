package fr.arkofus.block.decoration;

import net.minecraft.block.material.Material;

/**
 * Class créée le 04/12/2018 à 13:23
 * par Jullian Dorian
 */
public class BlockSlabStoneHalf extends BlockSlabBase {

    public BlockSlabStoneHalf(String name) {
        super(name, Material.ROCK);
    }

    @Override
    public boolean isDouble() {
        return false;
    }
}
