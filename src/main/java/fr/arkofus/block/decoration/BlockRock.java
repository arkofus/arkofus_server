package fr.arkofus.block.decoration;

import fr.arkofus.block.BlockBase;
import net.minecraft.block.material.Material;

/**
 * Class créée le 19/11/2018 à 20:10
 * par Jullian Dorian
 */
public class BlockRock extends BlockBase {

    public BlockRock(String name) {
        super(name, Material.ROCK);
    }

}
