package fr.arkofus.block.decoration;

import net.minecraft.block.material.Material;

/**
 * Class créée le 20/11/2018 à 21:37
 * par Jullian Dorian
 */
public class BlockSlabGrassDouble extends BlockSlabBase {

    public BlockSlabGrassDouble(String name){
        super(name, Material.GRASS);
    }

    @Override
    public boolean isDouble() {
        return true;
    }
}
