package fr.arkofus.block;

import fr.arkofus.block.decoration.BlockBush;
import fr.arkofus.block.decoration.BlockLeaves;
import fr.arkofus.creativetabs.ArkofusTabs;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.IPlantable;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 17/11/2018 à 15:08
 * par Jullian Dorian
 */
public class BlockBase extends Block {

    private final String name;
    protected final ThreadLocalRandom random;

    public BlockBase(String name, Material material) {
        super(material);

        this.name = name;
        this.random = ThreadLocalRandom.current();
        this.setUnlocalizedName(name);
        this.setResistance(-1f);
        this.setBlockUnbreakable();
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.disableStats();
        this.setSoundType(material == Material.ROCK || material == Material.BARRIER ? SoundType.STONE :
                material == Material.ANVIL ? SoundType.ANVIL :
                        material == Material.CACTUS || material == Material.GRASS || material == Material.LEAVES
                                || material == Material.VINE ? SoundType.PLANT :
                                material == Material.CARPET ? SoundType.CLOTH :
                                        material == Material.SAND ? SoundType.SAND :
                                                material == Material.WOOD ? SoundType.WOOD :
                                                        SoundType.STONE);
    }

    @Override
    public boolean canSustainLeaves(IBlockState state, IBlockAccess world, BlockPos pos) {
        final IBlockState plant = world.getBlockState(pos.up());
        return plant.getBlock() instanceof BlockLeaves;
    }

    @Override
    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable) {
        final IBlockState plant = world.getBlockState(pos.up());
        return plant.getBlock() instanceof BlockBush;
    }

    public String getName() {
        return name;
    }
}
