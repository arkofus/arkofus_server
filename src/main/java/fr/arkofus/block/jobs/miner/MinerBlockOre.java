package fr.arkofus.block.jobs.miner;

import fr.arkofus.ArkofusServer;
import fr.arkofus.block.BlockBase;
import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.item.tool.ItemPickaxeMiner;
import fr.arkofus.packet.client.CPacketAddItemInventory;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.player.jobs.JobMiner;
import fr.arkofus.utils.Math;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 29/10/2018 à 14:07
 * par Jullian Dorian
 */
public abstract class MinerBlockOre extends BlockBase {

    private static final PropertyInteger AGE = PropertyInteger.create("age", 0, 1);

    private float $hardness;

    public MinerBlockOre(String name, float hardness) {
        super(name, Material.ROCK);

        this.$hardness = hardness;

        this.setDefaultState(this.blockState.getBaseState().withProperty(getAgeProperty(), 1));
        this.setTickRandomly(true);
        this.setCreativeTab(ArkofusTabs.Type.JOB.getCreativeTab());
    }

    /**
     * Le niveau ou le block pourra être cassé
     * @return level
     */
    protected abstract int getLevelHarvested();

    /**
     * Retourne les drops de block
     * @return drops
     */
    protected abstract List<ItemStack> getDrops();

    /**
     * Retourne l'experience reçu (pour le niveau harvested)
     * @return experience
     */
    protected abstract long getExperience();

    private PropertyInteger getAgeProperty() {
        return AGE;
    }

    private int getMaxAge(){
        return 1;
    }

    private int getAge(IBlockState state){
        return state.getValue(getAgeProperty());
    }

    public IBlockState withAge(int age) {
        return this.getDefaultState().withProperty(getAgeProperty(), age);
    }

    public boolean isMaxAge(IBlockState state) {
        return state.getValue(getAgeProperty()) >= this.getMaxAge();
    }

    /*
    On définit le drop sur de l'air (pour éviter que le joueur l'ait en double)
     */
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Items.AIR;
    }

    @Override
    public float getBlockHardness(IBlockState blockState, World worldIn, BlockPos pos) {
        if(isMaxAge(blockState)){
            return this.$hardness;
        } else {
            return this.blockHardness;
        }
    }

    @Override
    public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune) {
        return 0;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {

        if(!player.isCreative()) {
            if (!world.isRemote) {

                ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(player.getUniqueID());

                //si le minerai n'est pas âgé on arrête la fonction
                if(!isMaxAge(state) || arkofusPlayer == null) return false;
                if(!(player.getHeldItemMainhand().getItem() instanceof ItemPickaxeMiner)) return false;

                JobMiner jobMiner = (JobMiner) arkofusPlayer.getJob("miner");
                if(jobMiner != null) {
                    //detection si le joueur à le level pour casser le block (return true si il ne peux pas)
                    if (jobMiner.getLevel() >= getLevelHarvested()) {

                        long experience = getExperience() - Math.round(jobMiner.getLevel() / 5d);
                        //On give l'experience
                        jobMiner.giveExperience(experience, player);

                        //On give un alea de la ressource
                        getDrops().forEach(item -> {
                            ItemStack itemStack = item.copy();
                            arkofusPlayer.getInventory().addItemStackToInventory(item);
                            RegistryPacket.getNetwork().sendTo(new CPacketAddItemInventory(String.valueOf(Item.getIdFromItem(itemStack.getItem())), itemStack.getCount()), (EntityPlayerMP) player);
                        });
                        arkofusPlayer.getInventory().refreshNBT();
                        ArkofusServer.getMapPlayers().replace(player.getUniqueID(), arkofusPlayer);
                        //RegistryPacket.getNetwork().sendTo(new CPacketUpdateInventory(arkofusPlayer.getInventory().savedCompound), (EntityPlayerMP) player);
                        return world.setBlockState(pos, state.withProperty(this.getAgeProperty(), 0), 3);
                    } else {
                        player.sendMessage(new TextComponentString( "Vous n'avez pas le niveau requis pour casser ce minerai."));
                        return false;
                    }
                }
            }
            return false;
        } else {
            return super.removedByPlayer(state, world, pos, player, willHarvest);
        }
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
        super.updateTick(worldIn, pos, state, rand);

        int i = this.getAge(state);

        if (i < this.getMaxAge()) {

            float r = rand.nextFloat();

            if(ForgeHooks.onCropsGrowPre(worldIn, pos, state, r <= 0.6f)) {
                worldIn.setBlockState(pos, this.withAge(i + 1), 2);
                ForgeHooks.onCropsGrowPost(worldIn, pos, state, worldIn.getBlockState(pos));
            }
        }
    }

    /**
     * Convert the given metadata into a BlockState for this Block
     */
    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return this.withAge(meta);
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    @Override
    public int getMetaFromState(IBlockState state)
    {
        return this.getAge(state);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, AGE);
    }
}
