package fr.arkofus.block.jobs.miner;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 29/10/2018 à 14:31
 * par Jullian Dorian
 */
public class MinerTinBlockOre extends MinerBlockOre {

    public MinerTinBlockOre() {
        super("tin_ore", 3.75f);
    }

    /**
     * Le niveau ou le block pourra être cassé
     *
     * @return level
     */
    @Override
    protected int getLevelHarvested() {
        return 100;
    }

    /**
     * Retourne les drops de block
     *
     * @return drops
     */
    @Override
    protected List<ItemStack> getDrops() {

        ItemStack fer = new ItemStack(Items.IRON_INGOT);
        fer.setCount(random.nextInt(2, 8));

        ItemStack block_fer = ItemStack.EMPTY;
        if(random.nextFloat() < 0.20){
            block_fer = new ItemStack(Blocks.IRON_BLOCK);
            block_fer.setCount(1);
        }

        return Arrays.asList(fer, block_fer);
    }

    /**
     * Retourne l'experience reçu (pour le niveau harvested)
     *
     * @return experience
     */
    @Override
    protected long getExperience() {
        return 18;
    }
}
