package fr.arkofus.block.jobs.miner;

import fr.arkofus.Constants;
import fr.arkofus.block.BlockBase;
import fr.arkofus.client.gui.GuiHandler;
import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.utils.ContainerHelper;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;

import java.lang.reflect.InvocationTargetException;


/**
 * Class créée le 17/11/2018 à 15:05
 * par Jullian Dorian
 */
public class BlockMeule extends BlockBase {

    public BlockMeule() {
        super("block_meule", Material.ROCK);
        this.setCreativeTab(ArkofusTabs.Type.JOB.getCreativeTab());

        this.setLightOpacity(1);
    }

    @Override
    public boolean isTranslucent(IBlockState state) {
        return true;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    /**
     * Called when the block is right clicked by a player.
     *
     * @param worldIn
     * @param pos
     * @param state
     * @param playerIn
     * @param hand
     * @param facing
     * @param hitX
     * @param hitY
     * @param hitZ
     */
    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(!worldIn.isRemote){
            try {
                ContainerHelper.openGui(playerIn, Constants.MODID, GuiHandler.GUI_MEULE);
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchFieldException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }


}
