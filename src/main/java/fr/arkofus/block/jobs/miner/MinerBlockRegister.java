package fr.arkofus.block.jobs.miner;

import net.minecraft.block.Block;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 29/10/2018 à 14:38
 * par Jullian Dorian
 */
public class MinerBlockRegister {

    public final static MinerBlockOre IRON_ORE = new MinerIronBlockOre();
    public final static MinerBlockOre COPPER_ORE = new MinerCopperBlockOre();
    public final static MinerBlockOre BRONZE_ORE = new MinerBronzeBlockOre();
    public final static MinerBlockOre COBALT_ORE = new MinerCobaltBlockOre();
    public final static MinerBlockOre MANGANESE_ORE = new MinerManganeseBlockOre();
    public final static MinerBlockOre TIN_ORE = new MinerTinBlockOre();
    public final static MinerBlockOre SILICATE_ORE = new MinerSilicateBlockOre();
    public final static MinerBlockOre SILVER_ORE = new MinerSilverBlockOre();
    public final static MinerBlockOre BAUXITE_ORE = new MinerBauxiteBlockOre();
    public final static MinerBlockOre GOLD_ORE = new MinerGoldBlockOre();
    public final static MinerBlockOre DOLOMITE_ORE = new MinerDolomiteBlockOre();
    public final static MinerBlockOre OBSIDIAN_ORE = new MinerObsidianBlockOre();
    public final static MinerBlockOre SEPIOLITE_ORE = new MinerSepioliteBlockOre();

    public final static BlockMeule MEULE = new BlockMeule();

    public static List<Block> blocks = Arrays.asList(IRON_ORE,COPPER_ORE,BRONZE_ORE,
            COBALT_ORE,MANGANESE_ORE,TIN_ORE,SILICATE_ORE,SILVER_ORE,BAUXITE_ORE,GOLD_ORE,
            DOLOMITE_ORE,OBSIDIAN_ORE,SEPIOLITE_ORE, MEULE);

}
