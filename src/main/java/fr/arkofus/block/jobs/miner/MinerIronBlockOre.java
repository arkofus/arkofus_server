package fr.arkofus.block.jobs.miner;

import fr.arkofus.item.ressources.ItemRessourceRegister;
import net.minecraft.item.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 29/10/2018 à 14:31
 * par Jullian Dorian
 */
public class MinerIronBlockOre extends MinerBlockOre {

    public MinerIronBlockOre() {
        super("iron_ore", 2.5f);
    }

    /**
     * Le niveau ou le block pourra être cassé
     *
     * @return level
     */
    @Override
    protected int getLevelHarvested() {
        return 0;
    }

    /**
     * Retourne les drops de block
     *
     * @return drops
     */
    @Override
    protected List<ItemStack> getDrops() {

        ItemStack iron = new ItemStack(ItemRessourceRegister.IRON);
        System.out.println("Iron ?:" + iron.getDisplayName());
        iron.setCount(random.nextInt(2, 8));

        return Arrays.asList(iron);
    }

    /**
     * Retourne l'experience reçu (pour le niveau harvested)
     *
     * @return experience
     */
    @Override
    protected long getExperience() {
        return 8;
    }
}
