package fr.arkofus.classe.spell;

import fr.arkofus.entities.spell.EntitySpell;
import fr.arkofus.item.base.spell.ItemSpell;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.util.List;

/**
 * Class créée le 22/07/2018 à 16:12
 * par Jullian Dorian
 */
public interface ISpell<T extends EntitySpell>{

    /**
     * <p>Retourne le nom du sort</p>
     * @return name
     */
    String getName();

    /**
     * <p>Retourne la description du sort</p>
     * @return description
     */
    String getDescription();

    /**
     * <p>Retourne l'item associé au sort</p>
     * @return item
     */
    ItemSpell getItem();

    /**
     * <p>Retourne l'entité qui sera envoyé (Une entité projectile)</p>
     * @return entity
     */
    T getEntity(World world, EntityPlayer entityPlayer);

    /**
     * <p>Retourne le niveau du sort en fonction du niveau du joueur</p>
     * @return level
     */
    int getLevel();

    /**
     * <p>Retourne un tableau de chiffre pour la reach, 0 => minimum ; 1 => maximum</p>
     * @return reach
     */
    int[] getReach();

    default int getMinReach(){
        return getReach()[0];
    }

    default int getMaxReach(){
        return getReach()[1];
    }

    /**
     * <p>Retourne l'effet de zone du sort</p>
     * @return effectZone
     */
    EffectZone getEffectZone();

    List<Damage> getDamages();

    /**
     * <p>Retourne vrai si le sort possède une zone</p>
     * @return effectZone != null
     */
    default boolean hasEffectZone(){
        return getEffectZone() != null;
    }

    /**
     * <p>Retourne l'apparition probable d'un coup critique (1 = 100%)</p>
     * @return critical
     */
    float getCritical();

    /**
     * <p>Retourne si le sort ne peut-être lancé en ligne uniquement</p>
     * @return false par défaut
     */
    default boolean isLine(){
        return false;
    }

    /**
     * <p>Retourne vrai si la reach est modifiable par l'équipement</p>
     * @return reachModifiable
     */
    boolean isReachModifiable();

    /**
     * <p>Retourne vrai le sort est une </p>
     * @return isSecondSpell
     */
    boolean isSecondSpell();

    /**
     * <p>Retourne vrai si le sort est une invocation</p>
     * @return isInvocation
     */
    boolean isInvocation();

    /**
     * <p>Display l'entité dans le monde</p>
     * @param world - Monde du joueur
     * @param entity - Sort à afficher
     */
    default void display(World world, EntitySpell entity){
        world.spawnEntity(entity);
    }

    /**
     * <p>Le temps maximum que met le sort avant de pouvoir être relancé</p>
     * @return maxCooldown
     */
    int getMaxCooldown();

    /**
     * <p>La limitation maximum du sort sur la même entité (Autrement dit il fera 0 dommages)</p>
     * @return limitPerCible
     */
    int getLimitPerCible();

    /**
     * <p>La limite du lancé du sort avant de faire apparaître le cooldown</p>
     * @return limitToDelay
     */
    int getLimitToDelay();

    /**
     * <p>Prépare et instancie les variables pour avoir les informations du sort</p>
     * @param entityPlayer L'entité joueur
     * @param arkofusPlayer Le compte du joueur
     */
    void prepare(EntityPlayer entityPlayer, ArkofusPlayer arkofusPlayer);

}
