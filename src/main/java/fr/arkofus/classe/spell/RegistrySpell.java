package fr.arkofus.classe.spell;

import fr.arkofus.classe.enutrof.CoinsThrowingSpell;
import fr.arkofus.entities.spell.EntitySpell;
import fr.arkofus.utils.SpellList;

import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 09/08/2018 à 19:47
 * par Jullian Dorian
 */
public class RegistrySpell {

    private SpellList spells;

    public RegistrySpell(){
        spells = new SpellList(400);
    }

    public void preInitialization(){
        //Enutrof
        registerSpell(new CoinsThrowingSpell());
    }

    private void registerSpell(ISpell<? extends EntitySpell> spell){
        spells.add(spell);
    }

    public SpellList getRegistries() {
        return spells;
    }
}
