package fr.arkofus.classe.spell;

import fr.arkofus.characteristic.ElementType;
import net.minecraft.util.math.MathHelper;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 13/08/2018 à 13:46
 * par Jullian Dorian
 */
public class Damage {

    private float minDamage,maxDamage;
    private float damageCritical;
    private ElementType type;

    public Damage(float minDamage, float maxDamage, float damageCritical, ElementType type){
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.damageCritical = damageCritical;
        this.type = type;
    }

    /**
     * <p>Retourne un chiffre random de dommage compris entre le min et le max</p>
     * @return damage
     */
    public float getDamage(){
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return random.nextInt(MathHelper.floor(minDamage), MathHelper.floor(maxDamage) + 1);
    }

    public float getMinDamage() {
        return minDamage;
    }

    public float getMaxDamage() {
        return maxDamage;
    }

    public float getDamageCritical() {
        return damageCritical;
    }

    public ElementType getType() {
        return type;
    }

}
