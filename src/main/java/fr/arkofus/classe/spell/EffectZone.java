package fr.arkofus.classe.spell;

/**
 * Class créée le 09/08/2018 à 10:58
 * par Jullian Dorian
 */
public class EffectZone {

    private Type type;

    public EffectZone(Type zone, int reach){
        zone.setReach(reach);
        this.type = zone;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return getType().toString();
    }

    public enum Type{

        CROSS("Croix", "Croix de ", 1),
        CROSS_IN_OUT("Croix", "(Hors centre) Croix de ", 1),
        LINE("Ligne", "Ligne de ", 1),
        CIRCLE("Cercle", "Cercle de ", 1);

        private String name;
        private String text;
        private int reach;

        Type(String name, String text, int reach){
            this.name = name;
            this.text = text;
            this.reach = reach;
        }

        public String getName() {
            return name;
        }

        public String getText() {
            return text;
        }

        public int getReach() {
            return reach;
        }

        public void setReach(int reach) {
            this.reach = reach;
        }

        @Override
        public String toString() {
            return getText() + getReach() + " blocs";
        }
    }

}
