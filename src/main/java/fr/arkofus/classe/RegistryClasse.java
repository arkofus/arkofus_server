package fr.arkofus.classe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.arkofus.ArkofusServer;
import fr.arkofus.json.adapter.ClasseAdapter;
import fr.arkofus.utils.Reader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 03/08/2018 à 15:04
 * par Jullian Dorian
 */
public class RegistryClasse {

    private List<Classe> classes;

    private int id;

    public RegistryClasse(){
        this.classes = new ArrayList<>();
        this.id = 0;
    }

    @SideOnly(Side.SERVER)
    public void preInitialization() {

        for(File f : ArkofusServer.modConfiguration.classesRoot.listFiles()){
            if(f.exists()){
                if(f.getName().contains("json")){
                    Reader reader = new Reader(f);
                    String content = reader.read();

                    Classe classe = Classe.fromJson(content);
                    classe.setId(id);

                    classes.add(classe);

                    System.out.println("La classe : " + classe.getName() + " a bien été enregistré");
                    id++;
                }
            }
        }
    }

    public List<Classe> getClasses() {
        return classes;
    }
}
