package fr.arkofus.classe.enutrof;

import fr.arkofus.characteristic.ElementType;
import fr.arkofus.classe.spell.Damage;
import fr.arkofus.classe.spell.EffectZone;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.spell.EntityCoinsThrowing;
import fr.arkofus.item.ItemRegister;
import fr.arkofus.item.base.spell.ItemSpell;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 09/08/2018 à 19:02
 * par Jullian Dorian
 */
public class CoinsThrowingSpell implements ISpell<EntityCoinsThrowing> {

    private ArkofusPlayer arkofusPlayer;
    private EntityPlayer entityPlayer;

    public CoinsThrowingSpell(){}

    @Override
    public void prepare(EntityPlayer entityPlayer, ArkofusPlayer arkofusPlayer) {
        this.entityPlayer = entityPlayer;
        this.arkofusPlayer = arkofusPlayer;
    }

    /**
     * <p>Retourne le nom du sort</p>
     *
     * @return name
     */
    @Override
    public String getName() {
        return "coins_throwing";
    }

    @Override
    public String getDescription() {
        return "Occasionne des dommages eau à longue portée.";
    }

    /**
     * <p>Retourne l'item associé au sort</p>
     *
     * @return item
     */
    @Override
    public ItemSpell getItem() {
        return ItemRegister.ITEM_SPELL_COINS_THROWING;
    }

    /**
     * <p>Retourne l'entité qui sera envoyé (Une entité projectile)</p>
     *
     * @return entity
     */
    @Override
    public EntityCoinsThrowing getEntity(World world, EntityPlayer entityPlayer) {
        return new EntityCoinsThrowing(world, entityPlayer, this);
    }

    /**
     * <p>Retourne le niveau du sort en fonction du niveau du joueur</p>
     *
     * @return level
     */
    @Override
    public int getLevel() {
        if(arkofusPlayer.getLevel() >= 40)
            return 3;
        if(arkofusPlayer.getLevel() >= 20)
            return 2;
        if(arkofusPlayer.getLevel() >= 1)
            return 1;
        return 0;
    }

    /**
     * <p>Retourne un tableau de chiffre pour la reach, 0 => minimum ; 1 => maximum</p>
     *
     * @return reach
     */
    @Override
    public int[] getReach() {
        switch (getLevel()){
            case 3:
                return new int[]{0, 12};
            case 2:
                return new int[]{0, 10};
            case 1:
                return new int[]{0, 8};
            default:
                return new int[]{0,0};
        }
    }

    /**
     * <p>Retourne l'effet de zone du sort</p>
     *
     * @return effectZone
     */
    @Override
    public EffectZone getEffectZone() {
        return null;
    }

    @Override
    public List<Damage> getDamages() {

        float minDamage = getLevel() == 1 ? 7 : (getLevel() == 2 ? 9 : 11);
        float maxDamage = minDamage + 2.0f;

        Damage first = new Damage(minDamage, maxDamage, maxDamage + 1, ElementType.EAU);

        return Arrays.asList(first);
    }

    /**
     * <p>Retourne l'apparition probable d'un coup critique (1 = 100%)</p>
     *
     * @return critical
     */
    @Override
    public float getCritical() {
        return 0.150f;
    }

    /**
     * <p>Retourne vrai si la reach est modifiable par l'équipement</p>
     *
     * @return true
     */
    @Override
    public boolean isReachModifiable() {
        return true;
    }

    /**
     * <p>Retourne vrai le sort est une </p>
     *
     * @return false
     */
    @Override
    public boolean isSecondSpell() {
        return false;
    }

    @Override
    public boolean isInvocation() {
        return false;
    }

    /**
     * <p>Le temps maximum que met le sort avant de pouvoir être relancé</p>
     *
     * @return maxCooldown
     */
    @Override
    public int getMaxCooldown() {
        return 6;
    }

    /**
     * <p>La limitation maximum du sort sur la même entité (Autrement dit il fera 0 dommages)</p>
     *
     * @return limitPerCible
     */
    @Override
    public int getLimitPerCible() {
        return 3;
    }

    /**
     * <p>La limite du lancé du sort avant de faire apparaître le cooldown</p>
     *
     * @return limitToDelay
     */
    @Override
    public int getLimitToDelay() {
        return 4;
    }
}