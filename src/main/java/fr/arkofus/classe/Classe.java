package fr.arkofus.classe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.spell.EntitySpell;
import fr.arkofus.json.adapter.ClasseAdapter;
import fr.arkofus.utils.SpellList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 21/07/2018 à 17:18
 * par Jullian Dorian
 */
public class Classe implements IClasse{

    private int id = -1;
    private String name;
    private String description;
    private ResourceLocation icon;
    private String iconName;
    private SpellList spells;

    public Classe(String name, String description, String iconName, SpellList spells){
        this.name = name;
        this.description = description;
        this.icon = new ResourceLocation("textures/classes/" + iconName);
        this.iconName = iconName;
        this.spells = spells;
    }

    public Classe(int id, String name, ResourceLocation icon){
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.description = "";
        this.spells = new SpellList();
    }

    public void addSpell(ISpell<? extends EntitySpell> spell){
        this.spells.add(spell);
    }

    /**
     * <p>Retourne l'ID de classe</p>
     *
     * @return id
     */
    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Retourne le nom de la classe</p>
     *
     * @return name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <p>Retourne la description de la classe</p>
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <p>Retourne l'icon de la classe</p>
     *
     * @return icon
     */
    @Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getIcon() {
        return icon;
    }

    @Override
    public String getIconName() {
        return iconName;
    }

    /**
     * <p>Retourne la liste des sorts de la classe</p>
     *
     * @return spells
     */
    @Override
    public SpellList getSpells() {
        return spells;
    }

    private static Gson getGson(){
        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .disableHtmlEscaping()
                .registerTypeAdapter(Classe.class, new ClasseAdapter())
                .create();
    }

    public String toJson(){
        return getGson().toJson(this);
    }

    public static Classe fromJson(String content){
        return getGson().fromJson(content, Classe.class);
    }

}
