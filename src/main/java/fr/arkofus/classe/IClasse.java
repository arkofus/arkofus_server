package fr.arkofus.classe;

import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.spell.EntitySpell;
import fr.arkofus.utils.SpellList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 22/07/2018 à 16:11
 * par Jullian Dorian
 */
public interface IClasse {

    /**
     * <p>Retourne l'ID de classe</p>
     * @return id
     */
    int getId();

    /**
     * <p>Retourne le nom de la classe</p>
     * @return name
     */
    String getName();

    /**
     * <p>Retourne la description de la classe</p>
     * @return description
     */
    String getDescription();

    /**
     * <p>Retourne l'icone de la classe</p>
     * @return icon
     */
    @SideOnly(Side.CLIENT)
    ResourceLocation getIcon();

    /**
     * <p>Retourne le nom de l'icone</p>
     * @return iconName
     */
    String getIconName();

    /**
     * <p>Retourne la liste des sorts de la classe</p>
     * @return spells
     */
    SpellList getSpells();

    default void addSpell(SpellList list, ISpell<? extends EntitySpell> spell){
        list.add(spell);
    }

}
