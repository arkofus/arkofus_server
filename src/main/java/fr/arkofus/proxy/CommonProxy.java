package fr.arkofus.proxy;

import fr.arkofus.ArkofusServer;
import fr.arkofus.block.BlockRegister;
import fr.arkofus.client.gui.GuiHandler;
import fr.arkofus.entities.EntityRegister;
import fr.arkofus.item.ItemRegister;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

/**
 * Class créée le 21/07/2018 à 15:48
 * par Jullian Dorian
 */
public abstract class CommonProxy implements IProxy {

    @Override
    public void onPreInitialization(FMLPreInitializationEvent event) {

        ItemRegister.commonPreInitialization();
        BlockRegister.commonPreInitialization();
    }

    @Override
    public void onInitialization(FMLInitializationEvent event) {
        EntityRegister.commonInitialization();

        NetworkRegistry.INSTANCE.registerGuiHandler(ArkofusServer.INSTANCE, new GuiHandler());
    }

    @Override
    public void onPostInitialization(FMLPostInitializationEvent event) {

    }

    public abstract void serverStarting(FMLServerStartingEvent event);

}
