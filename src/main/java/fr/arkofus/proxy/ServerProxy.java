package fr.arkofus.proxy;

import fr.arkofus.server.commands.*;
import fr.arkofus.server.event.*;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

/**
 * Class créée le 21/07/2018 à 15:48
 * par Jullian Dorian
 */
public class ServerProxy extends CommonProxy {

    @Override
    public void onPreInitialization(FMLPreInitializationEvent event) {
        super.onPreInitialization(event);
    }

    @Override
    public void onInitialization(FMLInitializationEvent event) {
        super.onInitialization(event);
        FMLCommonHandler.instance().bus().register(new PlayerLoggedInListener());
        FMLCommonHandler.instance().bus().register(new PlayerLoggedOutListener());
        FMLCommonHandler.instance().bus().register(new EntityListener());
        FMLCommonHandler.instance().bus().register(new PlayerListener());
        FMLCommonHandler.instance().bus().register(new WorldListener());
    }

    @Override
    public void onPostInitialization(FMLPostInitializationEvent event) {
        super.onPostInitialization(event);
    }

    @Override
    public void serverStarting(FMLServerStartingEvent event) {
        new CommandRegister(event).registerCommands();
    }
}
