package fr.arkofus.proxy;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Class créée le 21/07/2018 à 15:49
 * par Jullian Dorian
 */
public interface IProxy {

    void onPreInitialization(FMLPreInitializationEvent event);

    void onInitialization(FMLInitializationEvent event);

    void onPostInitialization(FMLPostInitializationEvent event);

}
