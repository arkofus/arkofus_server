package fr.arkofus.quest;

import net.minecraft.item.ItemStack;

/**
 * Class créée le 24/11/2018 à 12:21
 * par Jullian Dorian
 */
public class Quest {

    private String name;
    private String description;

    private ItemStack reward;
    private boolean canRepeat = false;

    public Quest(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setReward(ItemStack reward) {
        this.reward = reward;
    }

    public ItemStack getReward() {
        return reward;
    }

    public boolean isCanRepeat() {
        return canRepeat;
    }
}
