package fr.arkofus.characteristic;

/**
 * Class créée le 21/07/2018 à 17:46
 * par Jullian Dorian
 */
public interface ICharacSecondary {

    /*
    * --- Primaires ---
     */

    IPrimary getVitalite();

    IPrimary getSagesse();

    IPrimary getAgilite();

    IPrimary getForce();

    IPrimary getIntelligence();

    IPrimary getChance();

    /**
     * <p>Retourne la puissance (fixe)</p>
     * @return puissance
     */
    int getPuissance();

    /**
     * <p>Defini la puissance (fixe)</p>
     * @param value - La nouvelle valeur
     */
    void setPuissance(int value);

    /**
     * <p>Augmente la probabilité de faire un coup critique, gérer sur 100%. 0 = 0% ; 1 = 100%</p>
     * @return critical
     */
    float getCritical();

    /**
     * <p>Défini le nouveau pourcentage de faire un coup critique.</p>
     * @param value - La nouvelle valeur
     */
    void setCritical(float value);

    /*
    * --- SECONDAIRES ---
     */

    /**
     * <p>Retourne les soins (fixe)</p>
     * @return care
     */
    int getCare();

    void setCare(int value);

    /**
     * <p>Retourne la valeur d'invocation disponible.</p>
     * @return maxInvocation
     */
    int getMaxInvocation();
    
    void setMaxInvocation(int value);

    /**
     * <p>Retourne la prospection</p>
     * @return prospection
     */
    int getProspection();
    
    void setProspection(int value);
    
    /*
    * --- DOMMAGES ---
     */
    
    double getDamage();

    void setDamage(double damage);
    
    double getDamageCritical();

    void setDamageCritical(double damageCritical);

    double getDamageNeutre();

    void setDamageNeutre(double damageNeutre);

    double getDamageTerre();

    void setDamageTerre(double damageTerre);

    double getDamageFeu();

    void setDamageFeu(double damageFeu);

    double getDamageEau();

    void setDamageEau(double damageEau);

    double getDamageAir();

    void setDamageAir(double damageAir);

    double getThorn();

    void setThorn(double thorn);

    double getMasteryWeapon();

    void setMasteryWeapon(double masteryWeapon);

    /**
     * <p>Retourne les dommages d'un élement</p>
     * @param type enum class
     * @return damage of element
     */
    double getDamageElement(ElementType type);

    /**
     * <p>Retourne les dommages fixes du piège</p>
     * <p>Augmente les dommages fixes du piège</p>
     * @return damageTrap
     */
    double getDamageTrap();

    void setDamageTrap(int damageTrap);

    /**
     * <p>Retourne la puissance des dommages du piege</p>
     * <p>Augmente la puissance du piege, 10 de puissance = 1 dommage fixe.</p>
     * @return puissanceTrap
     */
    double getPuissanceTrap();

    void setPuissanceTrap(int puissanceTrap);
    
    /*
    * --- RESISTANCES ---
     */

    int getResFixeNeutre();

    void setResFixeNeutre(int resFixeNeutre);

    float getResNeutre();

    void setResNeutre(float resNeutre);

    int getResFixeTerre();

    void setResFixeTerre(int resFixeTerre);

    float getResTerre();

    void setResTerre(float resTerre);

    int getResFixeFeu();

    void setResFixeFeu(int resFixeFeu);

    float getResFeu();

    void setResFeu(float resFeu);

    int getResFixeEau();

    void setResFixeEau(int resFixeEau);

    float getResEau();

    void setResEau(float resEau);

    int getResFixeAir();

    void setResFixeAir(int resFixeAir);

    float getResAir();

    void setResAir(float resAir);

    int getResCritical();

    void setResCritical(int resCritical);

    int getResKnockback();

    void setResKnockback(int resKnockback);

}
