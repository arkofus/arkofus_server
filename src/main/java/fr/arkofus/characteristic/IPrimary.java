package fr.arkofus.characteristic;

/**
 * Class créée le 21/07/2018 à 17:53
 * par Jullian Dorian
 */
public interface IPrimary {

    /**
     * <p>Retourne les points naturel de l'élement</p>
     * @return natural
     */
    double getNatural();

    /**
     * <p>Défini les nouveaux points de naturel</p>
     * @param points
     */
    void setNatural(double points);

    /**
     * <p>Ajoute un point de naturel.</p>
     * @param value - La value à ajouter
     * @return value - Retourne la value du paramètre.
     */
    double addNatural(double value);

    /**
     * <p>Enlève un point de naturel.</p>
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    double removeNatural(double value);

    /**
     * <p>Retourne les points additionnels de l'élement</p>
     * @return additional
     */
    double getAdditional();

    /**
     * <p>Défini les nouveaux points d'additions</p>
     * @param additional
     */
    void setAdditional(double additional);

    /**
     * <p>Ajoute un point additionnel.</p>
     * @param value - La value à ajouter
     * @return value - Retourne la value du paramètre.
     */
    double addAdditional(double value);

    /**
     * <p>Enlève un point additionnel.</p>
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    double removeAdditional(double value);

    /**
     * <p>Défini la valeur du bonus</p>
     * @param value - La nouvelle valeur
     */
    void setBonus(double value);

    /**
     * <p>Retourne les points bonus de l'élement.</p>
     * @return bonus
     */
    double getBonus();

    /**
     * <p>Ajoute un point bonus</p>
     * @param value - La valeur à ajouter
     * @return value - Retourne la value du paramètre.
     */
    double addBonus(double value);

    /**
     * <p>Enlève un point de bonus.</p>
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    double removeBonus(double value);

    /**
     * <p>Retourne la totalité des valeurs; Naturel - Additionnel - Bonus</p>
     * @return total
     */
    double getTotal();

}
