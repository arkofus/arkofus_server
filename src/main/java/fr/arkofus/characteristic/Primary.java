package fr.arkofus.characteristic;

/**
 * Class créée le 21/07/2018 à 17:52
 * par Jullian Dorian
 */
public class Primary implements IPrimary {

    private double natural;
    private double additional;
    private double bonus;

    public Primary(){}

    /**
     * <p>Retourne les points naturel de l'élement</p>
     *
     * @return natural
     */
    @Override
    public double getNatural() {
        return this.natural;
    }

    /**
     * <p>Défini les nouveaux points de naturel</p>
     *
     * @param points
     */
    @Override
    public void setNatural(double points) {
        this.natural = points;
    }

    /**
     * <p>Ajoute un point de naturel.</p>
     *
     * @param value - La value à ajouter
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double addNatural(double value) {
        this.natural += value;
        return value;
    }

    /**
     * <p>Enlève un point de naturel.</p>
     *
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double removeNatural(double value) {
        this.natural -= value;
        return value;
    }

    /**
     * <p>Retourne les points additionnels de l'élement</p>
     *
     * @return additional
     */
    @Override
    public double getAdditional() {
        return this.additional;
    }

    /**
     * <p>Défini les nouveaux points d'additions</p>
     *
     * @param additional
     */
    @Override
    public void setAdditional(double additional) {
        this.additional = additional;
    }

    /**
     * <p>Ajoute un point additionnel.</p>
     *
     * @param value - La value à ajouter
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double addAdditional(double value) {
        this.additional += value;
        return value;
    }

    /**
     * <p>Enlève un point additionnel.</p>
     *
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double removeAdditional(double value) {
        this.additional -= value;
        return value;
    }

    /**
     * <p>Défini la valeur du bonus</p>
     *
     * @param value - La nouvelle valeur
     */
    @Override
    public void setBonus(double value) {
        this.bonus = value;
    }

    /**
     * <p>Retourne les points bonus de l'élement.</p>
     *
     * @return bonus
     */
    @Override
    public double getBonus() {
        return this.bonus;
    }

    /**
     * <p>Ajoute un point bonus</p>
     *
     * @param value - La valeur à ajouter
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double addBonus(double value) {
        this.bonus += value;
        return value;
    }

    /**
     * <p>Enlève un point de bonus.</p>
     *
     * @param value - La value à enlever
     * @return value - Retourne la value du paramètre.
     */
    @Override
    public double removeBonus(double value) {
        this.bonus -= value;
        return value;
    }

    /**
     * <p>Retourne la totalité des valeurs; Naturel - Additionnel - Bonus</p>
     *
     * @return total
     */
    @Override
    public double getTotal() {
        return (getAdditional() + getNatural() + getBonus());
    }
}
