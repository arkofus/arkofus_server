package fr.arkofus.characteristic;

/**
 * Class créée le 14/08/2018 à 16:30
 * par Jullian Dorian
 */
public enum ElementType {

    CRITIQUE("Critique"),
    EAU("Eau"),
    FEU("Feu"),
    TERRE("Terre"),
    NEUTRE("Neutre"),
    AIR("Air");

    private String name;

    ElementType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

}
