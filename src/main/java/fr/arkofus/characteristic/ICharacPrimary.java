package fr.arkofus.characteristic;

/**
 * Class créée le 21/07/2018 à 17:32
 * par Jullian Dorian
 */
public interface ICharacPrimary {

    /**
     * <p>Renvoie le niveau</p>
     * @return level
     */
    int getLevel();

    /**
     * <p>Définit le nouveau niveau</p>
     * @param value - La nouvelle valeur qui sera le niveau
     */
    void setLevel(int value);

    /**
     * <p>Ajoute un niveau</p>
     * @param value - La quantité de niveau a ajouter
     * @return level - Retourne le niveau
     */
    int addLevel(int value);

    /**
     * <p>Enlève un niveau</p>
     * @param value - La quantité de niveau a enlever
     * @return level - Retourne le niveau
     */
    int removeLevel(int value);

    /**
     * <p>Renvoie le niveau minimum</p>
     * @return minLevel
     */
    default int getMinLevel(){
        return 1;
    }

    /**
     * <p>Défini le niveau minimum du level atteignable.</p>
     * @param value - Le level min
     */
    void setMinLevel(int value);

    /**
     * <p>Renvoie le niveau maximum</p>
     * @return maxLevel
     */
    default int getMaxLevel(){
        return 200;
    }

    /**
     * <p>Défini le niveau maximum du level atteignable.</p>
     * @param value - Le level max
     */
    void setMaxLevel(int value);

}
