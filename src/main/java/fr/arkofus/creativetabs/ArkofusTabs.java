package fr.arkofus.creativetabs;

import fr.arkofus.item.ItemRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

/**
 * Class créée le 30/07/2018 à 16:38
 * par Jullian Dorian
 */
public abstract class ArkofusTabs extends CreativeTabs{

    private final String name;

    public ArkofusTabs(String name){
        super(name);

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public enum Type {
        CLASSES(new ArkofusTabs("classes_tab") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemRegister.TAB_CLASSE);
            }
        }),
        WEAPONS(new ArkofusTabs("weapons_tab") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemRegister.TAB_WEAPON);
            }
        }),
        JOB(new ArkofusTabs("jobs_tab") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemRegister.TAB_JOB);
            }
        }),
        RESSOURCES(new ArkofusTabs("ressources_tab") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemRegister.TAB_RESOURCE);
            }
        }),
        EQUIPMENT(new ArkofusTabs("equipment_tab") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemRegister.TAB_EQUIPMENT);
            }
        });

        private final ArkofusTabs creativeTab;

        Type(ArkofusTabs creativeTab) {
            this.creativeTab = creativeTab;
        }

        public ArkofusTabs getCreativeTab() {
            return creativeTab;
        }

        @Override
        public String toString() {
            return creativeTab.getName();
        }
    }
}
