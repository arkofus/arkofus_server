package fr.arkofus.utils;

import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.server.FMLServerHandler;

/**
 * Class créée le 09/12/2018 à 19:06
 * par Jullian Dorian
 */
public class WorldHelper {

    public static World findWorldByName(String name){
        World ret = null;

        MinecraftServer minecraftServer = FMLServerHandler.instance().getServer();
        System.out.println("size world: " + minecraftServer.worlds.length);

        for(WorldServer worldServer : minecraftServer.worlds){

            System.out.println("world name: "+ worldServer.getWorldInfo().getWorldName());

            if(worldServer.getWorldInfo().getWorldName().equalsIgnoreCase(name)){
                System.out.println("finded");
                ret = worldServer;
                break;
            }

        }

        return ret;
    }

}
