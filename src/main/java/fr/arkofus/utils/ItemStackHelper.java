package fr.arkofus.utils;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

/**
 * Class créée le 05/11/2018 à 14:26
 * par Jullian Dorian
 */
public class ItemStackHelper {

    private ItemStack itemStack;

    /**
     * Create a new itemStack from the NBTCompound
     * @param compound Nbt
     */
    public ItemStackHelper(NBTTagCompound compound){

        final Item item = compound.hasKey("id", 8) ? Item.getByNameOrId(compound.getString("id")) : Item.getItemFromBlock(Blocks.AIR); //Forge fix tons of NumberFormatExceptions that are caused by deserializing EMPTY ItemStacks.
        final int stackSize = compound.hasKey("Count") ? (int) (compound.getByte("Count") & 0xFF) : 1;
        final int itemDamage = java.lang.Math.max(0, compound.getShort("Damage"));

        this.itemStack = new ItemStack(compound);
        if(!itemStack.isEmpty()){
            this.itemStack.setCount(stackSize);
        } else {
            this.itemStack = new ItemStack(item, stackSize, itemDamage);

            if (compound.hasKey("tag", 10)) {
                itemStack.setTagCompound(compound.getCompoundTag("tag"));

                if (this.itemStack.getItem() != null) {
                    this.itemStack.getItem().updateItemStackNBT(compound);
                }
            }

        }
    }

    /**
     * Build the ItemStack from the NBT
     * @return itemStack
     */
    public ItemStack getItemStack(){
        return itemStack;
    }

}
