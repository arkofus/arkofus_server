package fr.arkofus.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class créée le 28/07/2018 à 11:09
 * par Jullian Dorian
 */
public class Reader {

    private File file;

    public Reader(File file){
        this.file = file;
    }

    public String read(){

        try{

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            StringBuilder content = new StringBuilder();

            String line;

            while((line = bufferedReader.readLine()) != null)
                content.append(line);

            bufferedReader.close();
            fileReader.close();

            return content.toString();

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return "";
    }

}
