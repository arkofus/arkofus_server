package fr.arkofus.utils;

import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.spell.EntitySpell;

/**
 * Class créée le 03/08/2018 à 14:29
 * par Jullian Dorian
 */
public class SpellList {

    private ISpell<? extends EntitySpell>[] list;

    private final int DEFAULT_CAPACITY = 50;
    private int capacity;

    private int index;

    private ISpell<? extends EntitySpell> last_find;

    public SpellList(){
        list = new ISpell[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        index = 0;
    }

    public SpellList(int capacity){
        list = new ISpell[capacity];
        this.capacity = capacity;
        index = 0;
    }

    /**
     * <p>Récupère le sort selon l'index</p>
     * @param index
     * @return spell
     */
    public ISpell<? extends EntitySpell> get(int index) {
        if(index >= 0 && index < capacity){
            return list[index];
        } else {
            throw new ArrayIndexOutOfBoundsException("Impossible de récupérer un sort, l'index est trop grand ou trop petit");
        }
    }

    /**
     * <p>Récupère le sort selon le nom</p>
     * @param name - Le nom du sort
     * @return spell | null - si le nom n'est pas trouvé
     */
    public ISpell<? extends EntitySpell> get(String name){
        if(contains(name)) {
            for (ISpell<? extends EntitySpell> s : list) {
                if (s.getName().equalsIgnoreCase(name)) {
                    setLastFind(s);
                    return s;
                }
            }
        }
        return null;
    }

    public boolean add(ISpell<? extends EntitySpell> spell) {
        if(index == 50)
            throw new ArrayIndexOutOfBoundsException("Impossible d'ajouter un sort, la capacité est atteinte");

        list[index] = spell;
        index++;
        return true;
    }

    public boolean contains(ISpell<? extends EntitySpell> spell){
        for (ISpell<? extends EntitySpell> s : list){
            if(s == spell || s.getClass() == spell.getClass()) {
                setLastFind(s);
                return true;
            }
        }
        return false;
    }

    public boolean contains(String name){
        if(size() == 0) return false;

        for (ISpell<? extends EntitySpell> s : list){
            System.out.println(s.getName());
            if(s.getName().equalsIgnoreCase(name)) {
                setLastFind(s);
                return true;
            }
        }
        return false;
    }

    public ISpell<? extends EntitySpell>[] getList(){
        return list;
    }

    public int size() {
        return this.list.length;
    }

    private void setLastFind(ISpell<? extends EntitySpell> last_find) {
        this.last_find = last_find;
    }

    public ISpell<? extends EntitySpell> getLastFind() {
        return last_find;
    }
}
