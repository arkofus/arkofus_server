package fr.arkofus.utils;

import org.lwjgl.input.Keyboard;

/**
 * Class créée le 15/11/2018 à 11:49
 * par Jullian Dorian
 */
public class KeyHelper {

    public static boolean isCtrlKeyDown(){
        return Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_LMETA);
    }

    public static boolean isAltKeyDown(){
        return Keyboard.isKeyDown(Keyboard.KEY_LBRACKET) || Keyboard.isKeyDown(Keyboard.KEY_LMENU);
    }

    public static boolean isShiftKeyDown(){
        return Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
    }

    public static boolean isCtrlRightKeyDown(){
        return Keyboard.isKeyDown(Keyboard.KEY_RCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RMETA);
    }

}
