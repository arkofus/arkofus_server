package fr.arkofus.utils;

/**
 * Class créée le 22/07/2018 à 13:21
 * par Jullian Dorian
 */
public final class Math {

    public Math(){}

    public static int round(double value){
        return (int) value;
    }

    public static float clamp(double finalDamage) {
        return Float.valueOf(Double.toString(finalDamage));
    }
}
