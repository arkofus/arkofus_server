package fr.arkofus.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * Class créée le 08/11/2018 à 08:54
 * par Jullian Dorian
 */
public class ClassHelper {

    private Object object;
    private Constructor constructor;
    private Field field;

    public ClassHelper(Object o){
        this.object = o;
        this.constructor = null;
        this.field = null;
    }

    public Constructor findDeclaredConstructor(int count){
        for(Constructor constructor : ((Class) object).getDeclaredConstructors()){
            if(constructor.getParameterCount() == count){
                this.constructor = constructor;
            }
        }
        this.constructor.setAccessible(true);
        return this.constructor;
    }

    public Field findDeclaredField(String field) throws NoSuchFieldException {
        this.field = ((Class) object).getDeclaredField(field);
        this.field.setAccessible(true);
        return this.field;
    }

    public Object newInstance(Object... params) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        return this.constructor.newInstance(params);
    }

}
