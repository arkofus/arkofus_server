package fr.arkofus.utils;


import com.google.common.collect.Maps;
import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.channel.embedded.EmbeddedChannel;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.world.World;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.network.FMLEmbeddedChannel;
import net.minecraftforge.fml.common.network.FMLOutboundHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.internal.FMLMessage;
import net.minecraftforge.fml.common.network.internal.FMLMessage.OpenGui;
import net.minecraftforge.fml.common.network.internal.FMLNetworkHandler;
import net.minecraftforge.fml.relauncher.Side;

import java.lang.reflect.*;
import java.util.EnumMap;

/**
 * Class créée le 07/11/2018 à 17:58
 * par Jullian Dorian
 */
public class ContainerHelper {

    /**
     * @see FMLNetworkHandler#openGui(EntityPlayer, Object, int, World, int, int, int) Is the same Method
     */
    public static void openGui(EntityPlayer entityPlayer, Object mod, int modGuiId) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        final World world = entityPlayer.getEntityWorld();
        final int x = entityPlayer.getPosition().getX();
        final int y = entityPlayer.getPosition().getY();
        final int z = entityPlayer.getPosition().getZ();

        ModContainer mc = FMLCommonHandler.instance().findContainerFor(mod);
        if (entityPlayer instanceof EntityPlayerMP && !(entityPlayer instanceof FakePlayer)) {
            EntityPlayerMP entityPlayerMP = (EntityPlayerMP) entityPlayer;
            //Retourne le container du GuiHandler#server
            Container remoteGuiContainer = NetworkRegistry.INSTANCE.getRemoteGuiContainer(mc, entityPlayerMP, modGuiId, world, x, y, z);
            if (remoteGuiContainer != null) {
                entityPlayerMP.getNextWindowId();
                entityPlayerMP.closeContainer();
                int windowId = entityPlayerMP.currentWindowId;

                /*
                On récupère le constructeur de la classe qui permet d'init le OpenGui
                 */
                ClassHelper classGui = new ClassHelper(OpenGui.class);
                classGui.findDeclaredConstructor(6);
                OpenGui openGui = (OpenGui) classGui.newInstance(windowId, mc.getModId(), modGuiId, x, y, z);

                /*
                On récupère le field du channelPair
                 */
                ClassHelper classNetHandler = new ClassHelper(FMLNetworkHandler.class);
                Field field = classNetHandler.findDeclaredField("channelPair");

                Object o = field.get(null);
                EnumMap<Side, FMLEmbeddedChannel> channelpair = (EnumMap<Side, FMLEmbeddedChannel>) o;

                EmbeddedChannel embeddedChannel = channelpair.get(Side.SERVER);
                embeddedChannel.attr(FMLOutboundHandler.FML_MESSAGETARGET).set(FMLOutboundHandler.OutboundTarget.PLAYER);
                embeddedChannel.attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(entityPlayerMP);
                embeddedChannel.writeOutbound(openGui);
                entityPlayerMP.openContainer = remoteGuiContainer;
                entityPlayerMP.openContainer.windowId = windowId;
                System.out.println("Add event listener custom with custom open Gui");
                final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());
                entityPlayerMP.openContainer.addListener(arkofusPlayer);
                net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.event.entity.player.PlayerContainerEvent.Open(entityPlayer, entityPlayer.openContainer));
            }
        }
        else if (entityPlayer instanceof FakePlayer)
        {
            // NO OP - I won't even log a message!
        }
        else if (FMLCommonHandler.instance().getSide().equals(Side.CLIENT))
        {
            Object guiContainer = NetworkRegistry.INSTANCE.getLocalGuiContainer(mc, entityPlayer, modGuiId, world, x, y, z);
            FMLCommonHandler.instance().showGuiScreen(guiContainer);
        }
        else
        {
            FMLLog.fine("Invalid attempt to open a local GUI on a dedicated server. This is likely a bug. GUI ID: %s,%d", mc.getModId(), modGuiId);
        }
    }

}
