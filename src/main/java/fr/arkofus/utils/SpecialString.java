package fr.arkofus.utils;

import fr.arkofus.item.base.armor.IArmorCharacteristic;
import fr.arkofus.item.base.weapon.IWeaponCharacteristic;
import fr.arkofus.item.base.weapon.ItemWeapon;
import net.minecraft.item.Item;
import net.minecraft.util.text.TextFormatting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class créée le 22/07/2018 à 17:04
 * par Jullian Dorian
 */
public class SpecialString {

    private String string;

    public SpecialString(String... strings){
        string = "";
        for (String s : strings){
            string += "\n" + s;
        }
    }

    public static Collection<String> convertDescription(String text, int length_line){
        return convertDescription(text, length_line, "");
    }

    public static Collection<String> convertDescription(String text, int length_line, String color){

        final String[] splits = text.split(" ");
        List<String> texts = new ArrayList<>();
        int cw = 0;
        StringBuilder currentLine = new StringBuilder();

        for(String s : splits){
            if(cw == 0)
                currentLine.append(color);
            if(cw < length_line) {
                currentLine.append(s);
                currentLine.append(" ");
            } else {
                cw = 0;
                texts.add(currentLine.toString());
                currentLine = new StringBuilder();
            }
            cw++;
        }
        texts.add(currentLine.toString());

        return texts;
    }

    public static List<String> getStringCharacs(Item item) {
        List<String> list = new ArrayList<>();

        if(item instanceof IWeaponCharacteristic) {
            final ItemWeapon itemWeapon = (ItemWeapon) item;
            for(IWeaponCharacteristic.IWeaponDamage weaponDamage : itemWeapon.getPropertiesAttack())
                list.add(weaponDamage.getMinAttack() + " - " + weaponDamage.getMaxAttack() + " | " + weaponDamage.getElement());

            if(itemWeapon.isHunter())
                list.add(TextFormatting.AQUA + "Arme de chasse");
            list.add("");
        }


        if(item instanceof IArmorCharacteristic){
            list = getStringCharac(list,"Vitalité", ((IArmorCharacteristic) item).getVitalite());
            list = getStringCharac(list,"Sagesse", ((IArmorCharacteristic) item).getSagesse());
            list = getStringCharac(list,"Force", ((IArmorCharacteristic) item).getForce());
            list = getStringCharac(list,"Intelligence", ((IArmorCharacteristic) item).getIntelligence());
            list = getStringCharac(list,"Chance", ((IArmorCharacteristic) item).getChance());
            list = getStringCharac(list,"Agilite", ((IArmorCharacteristic) item).getAgilite());
            list = getStringCharac(list,"Prospection", ((IArmorCharacteristic) item).getProspection());
            list = getStringCharac(list,"Puissance", ((IArmorCharacteristic) item).getPuissance());
        }

        return list;
    }

    public static List<String> getStringCharac(List<String> list, String name, int value){
        if(value < 0){
            list.add(String.format("%s*%d %s", TextFormatting.RED, value, name));
        }

        if(value > 0)
            list.add(String.format("%s*%d %s", TextFormatting.GREEN, value, name));

        return list;
    }

    public String build(){
        return string;
    }

}
