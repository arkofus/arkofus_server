package fr.arkofus;

import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

/**
 * Class créée le 21/07/2018 à 15:35
 * par Jullian Dorian
 */
public class Constants {

    /**
     * <p>L'ID du mod</p>
     */
    public static final String MODID = "arkofus";

    public static final int SUBSTRING = 4 + 1;

    /**
     * <p>La version actuel du mod</p>
     */
    public static final String VERSION = "0.0.5";

    /**
     * <p>Le nom du mod qui sera afficher dans le menu</p>
     */
    static final String NAME = "Arkofus Server";

    /**
     * <p>Les versions de minecraft accepté par le mod</p>
     */
    static final String ACCEPTED_VERSIONS = "[1.11, 1.11.2]";

    /**
     * <p>Le chemin de la class du Proxy côté Serveur</p>
     */
    public static final String SERVER_SIDE = "fr.arkofus.proxy.ServerProxy";

}
