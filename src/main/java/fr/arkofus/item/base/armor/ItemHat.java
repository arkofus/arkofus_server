package fr.arkofus.item.base.armor;

import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 12/11/2018 à 10:52
 * par Jullian Dorian
 */
public class ItemHat extends ItemArmor {

    public ItemHat(String name, String description){
        super(name, 1, ItemType.HAT, EntityEquipmentSlot.HEAD, description);
    }

    public ItemHat(String name, int level, String description){
        super(name, level, ItemType.HAT, EntityEquipmentSlot.HEAD, description);
    }

    @Override
    @SideOnly(Side.CLIENT)
    protected ModelBiped getModel() {
        return null;
    }

    @Override
    @SideOnly(Side.CLIENT)
    protected String getTextureModel() {
        return null;
    }

    @Override
    public ArmorType getTypeArmor() {
        return ArmorType.CHAPEAU;
    }
}
