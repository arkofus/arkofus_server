package fr.arkofus.item.base.armor;

/**
 * Class créée le 14/11/2018 à 16:00
 * par Jullian Dorian
 */
public interface IArmorCharacteristic {

    /*
    Stats primaire
     */
    default int getVitalite(){
        return 0;
    }

    default int getSagesse(){
        return 0;
    }

    default int getForce(){
        return 0;
    }

    default int getIntelligence(){
        return 0;
    }

    default int getChance(){
        return 0;
    }

    default int getAgilite(){
        return 0;
    }

    default int getPuissance(){
        return 0;
    }

    default int getProspection(){
        return 0;
    }

    default int getDommage(){
        return 0;
    }

    default int getReach(){
        return 0;
    }

    default float getCritical(){
        return 0f;
    }

    /*
    Stats dommages
     */
    default int getDommageNeutre(){
        return 0;
    }

    default int getDommageTerre(){
        return 0;
    }

    default int getDommageEau(){
        return 0;
    }

    default int getDommageFeu(){
        return 0;
    }

    default int getDommageAir(){
        return 0;
    }

}
