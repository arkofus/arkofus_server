package fr.arkofus.item.base.armor;

import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.inventory.EntityEquipmentSlot;

/**
 * Class créée le 13/11/2018 à 17:14
 * par Jullian Dorian
 */
public class ItemCape extends ItemArmor {

    public ItemCape(String name, String description){
        this(name, 1, description);
    }

    public ItemCape(String name, int level, String description){
        super(name, level, ItemType.CAPE, EntityEquipmentSlot.CHEST, description);
    }

    /**
     * <p>Get the model the itemArmor, if you want the default layers, set null</p>
     *
     * @return model
     */
    @Override
    protected ModelBiped getModel() {
        return null;
    }

    /**
     * <p>Return the texture of the model</p>
     *
     * @return path
     */
    @Override
    protected String getTextureModel() {
        return null;
    }

    /**
     * <p>Retourne le type d'armure pour être placé sur les slots de l'inventaire</p>
     *
     * @return armorType
     */
    @Override
    public ArmorType getTypeArmor() {
        return ArmorType.CAPE;
    }
}
