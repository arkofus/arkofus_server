package fr.arkofus.item.base.armor;

import fr.arkofus.Constants;
import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.IItemBase;
import fr.arkofus.panoplie.SetBase;
import fr.arkofus.utils.KeyHelper;
import fr.arkofus.utils.SpecialString;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Class créée le 12/11/2018 à 10:45
 * par Jullian Dorian
 */
public abstract class ItemArmor extends net.minecraft.item.ItemArmor implements IItemBase, IArmorCharacteristic {

    private final String name;
    private final String description;
    private final int level;
    private final ItemType itemType;
    private final int pods;

    private SetBase set;

    private static final ArmorMaterial VOID = EnumHelper.addArmorMaterial("empty_armor", Constants.MODID + ":empty_armor",
            -1, new int[]{0,0,0,0}, 0, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0f);

    static {
        assert VOID != null;
        VOID.repairMaterial = new ItemStack(Blocks.BEDROCK);
    }

    public ItemArmor(String name, ItemType itemType, EntityEquipmentSlot equipmentSlotIn) {
        this(name, 1, itemType, equipmentSlotIn, "");
    }

    public ItemArmor(String name, int level, ItemType itemType, EntityEquipmentSlot equipmentSlotIn) {
        this(name, level, itemType, equipmentSlotIn, "");
    }

    public ItemArmor(String name, int level, ItemType itemType, EntityEquipmentSlot equipmentSlotIn, String description) {
        super(VOID, 0, equipmentSlotIn);
        this.name = name;
        this.level = level;
        this.itemType = itemType;
        this.description = description;
        this.pods = 1;
        this.set = null;

        this.setUnlocalizedName(name);
        this.setMaxStackSize(1);
        this.setCreativeTab(ArkofusTabs.Type.EQUIPMENT.getCreativeTab());
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     *
     * @param stack
     * @param playerIn
     * @param tooltip
     * @param advanced
     */
    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        super.addInformation(stack, playerIn, tooltip, advanced);

        tooltip.add(TextFormatting.DARK_GRAY + "Niveau : " + getLevel());
        tooltip.add("");

        tooltip.add("Type : " + getType());
        tooltip.add("Pods : " + getPods());

        tooltip.add("");

        if(KeyHelper.isAltKeyDown()) {
            if (!getDescription().isEmpty()) {
                tooltip.addAll(SpecialString.convertDescription(getDescription(), 7));
            } else {
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "Cet objet n'est pas encore assez connu");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "des artisans pour que ses caractéristiques");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "puissent être révélées aux éventuels acheteurs.");
            }
        } else if(KeyHelper.isShiftKeyDown()){
            if(hasSet()) {
                tooltip.add("Panoplie : " + getSet().getName());
            }
        } else if(KeyHelper.isCtrlKeyDown()) {
            List<String> list = SpecialString.getStringCharacs(this);
            if(!list.isEmpty()) tooltip.addAll(list);
        } else {
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur CTRL pour afficher les caractéristiques.");
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur SHIFT pour afficher les bonus.");
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur ALT pour afficher la description.");
        }
    }

    @Override
    public void setSet(SetBase set) {
        this.set = set;
    }

    @Override
    public SetBase getSet() {
        return set;
    }

    /**
     * <p>Get the model the itemArmor, if you want the default layers, set null</p>
     * @return model
     */
    protected abstract ModelBiped getModel();

    /**
     * <p>Return the texture of the model</p>
     * @return path
     */
    protected abstract String getTextureModel();

    @Nullable
    @Override
    public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped defaultModel) {
        if(!itemStack.isEmpty() && getModel() != null){

            ModelBiped modelBiped = getModel();

            modelBiped.isSneak = defaultModel.isSneak;
            modelBiped.isRiding = defaultModel.isRiding;
            modelBiped.isChild = defaultModel.isChild;

            return modelBiped;
        }

        return null;
    }

    @Nullable
    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String type) {
        String s = !getTextureModel().isEmpty()
                ? String.format("%s:textures/models/armor/%s.png", Constants.MODID, getTextureModel())
                : super.getArmorTexture(stack, entity, slot, type);
        return s;
    }

    @Override
    public boolean hasOverlay(ItemStack stack) {
        return false;
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * <p>Retourne la description de l'item</p>
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * <p>Retourne le niveau de l'item</p>
     *
     * @return level
     */
    @Override
    public int getLevel() {
        return this.level;
    }

    /**
     * <p>Retourne le type de l'item</p>
     *
     * @return type
     * @see ItemType
     */
    @Override
    public ItemType getType() {
        return this.itemType;
    }

    /**
     * <p>Retourne le nombre de pods que prend l'item à l'unité</p>
     *
     * @return pods
     */
    @Override
    public int getPods() {
        return this.pods;
    }

}
