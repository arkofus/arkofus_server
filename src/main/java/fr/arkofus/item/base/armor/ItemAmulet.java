package fr.arkofus.item.base.armor;

import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;

import java.util.Collection;

/**
 * Class créée le 13/11/2018 à 17:20
 * par Jullian Dorian
 */
public class ItemAmulet extends ItemBase implements IArmorCharacteristic{

    public ItemAmulet(String name, String description){
        super(name, "", 1, ItemType.AMULET);

        this.setCreativeTab(ArkofusTabs.Type.EQUIPMENT.getCreativeTab());
        this.setMaxStackSize(1);
    }

    /**
     * <p>Si des choses ont besoin d'être ajouté</p>
     *
     * @return content
     */
    @Override
    protected Collection<String> getContent() {
        return null;
    }

    @Override
    public ArmorType getTypeArmor() {
        return ArmorType.AMULETTE;
    }
}
