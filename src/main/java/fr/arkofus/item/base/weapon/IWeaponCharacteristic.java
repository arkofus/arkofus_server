package fr.arkofus.item.base.weapon;

import fr.arkofus.characteristic.ElementType;
import fr.arkofus.item.base.armor.IArmorCharacteristic;

import java.util.List;

/**
 * Class créée le 14/11/2018 à 16:00
 * par Jullian Dorian
 */
public interface IWeaponCharacteristic extends IArmorCharacteristic {

    List<IWeaponDamage> getPropertiesAttack();

    IWeaponDamage getWeaponDamage();

    float getAttack(IWeaponDamage weaponDamage);

    float getAttackCritical(IWeaponDamage weaponDamage);

    default boolean isHunter(){
        return false;
    }

    public static interface IWeaponDamage{

        default boolean stillLife(){
            return false;
        }

        ElementType getElement();

        int getMinAttack();

        int getMaxAttack();

        float getCriticalDamage();

    }

}
