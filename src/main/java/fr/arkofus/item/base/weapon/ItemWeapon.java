package fr.arkofus.item.base.weapon;

import fr.arkofus.ArkofusServer;
import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.utils.Math;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 31/07/2018 à 10:55
 * par Jullian Dorian
 */
public abstract class ItemWeapon extends ItemBase implements IWeaponCharacteristic {

    public ItemWeapon(String name, String description, int level, ItemType type, int pods){
        super(name, description, level, type, pods);

        this.setMaxStackSize(1);
        this.setCreativeTab(ArkofusTabs.Type.WEAPONS.getCreativeTab());
    }

    @Override
    public ArmorType getTypeArmor() {
        return ArmorType.ARME;
    }

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    @Override
    public boolean isFull3D() {
        return true;
    }

    /**
     * Si le joueur peux casser un block en creative
     * @param world - Map ou se trouve le block
     * @param pos - La position du block
     * @param stack - L'item actuel qui casse le block
     * @param player - L'entité ayant l'item en main
     * @return false
     */
    @Override
    public boolean canDestroyBlockInCreative(World world, BlockPos pos, ItemStack stack, EntityPlayer player) {
        return false;
    }

    /**
     * ItemStack sensitive version of {@link #canHarvestBlock(IBlockState)}
     *
     * @param state - The block trying to harvest
     * @param stack - The itemstack used to harvest the block
     * @return false - if no can harvest the block
     */
    @Override
    public boolean canHarvestBlock(IBlockState state, ItemStack stack) {
        return false;
    }

    /**
     * Current implementations of this method in child classes do not use the entry argument beside ev. They just raise
     * the damage on the stack.
     *
     * @param stack - L'ItemStack actuel
     * @param target - L'entité frappé
     * @param attacker - L'entité qui frappe
     */
    @Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker) {
        this.actionWithItem(stack, target, attacker);
        return true;
    }

    /**
     * @see #hitEntity(ItemStack, EntityLivingBase, EntityLivingBase)
     */
    private void actionWithItem(ItemStack itemStack, EntityLivingBase entityTarget, EntityLivingBase entityAttacker){

        //Si aucune des entités n'est dans une map côté serveur, on ne fait rien.
        if(!entityAttacker.isServerWorld() || !entityTarget.isServerWorld()) return;

        //Si l'attacker est un joueur
        if(entityAttacker instanceof EntityPlayer){
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityAttacker.getUniqueID());

            //Si la cible est un EntityArkofus
            if(entityTarget instanceof ArkofusEntity) {
                ArkofusEntity entityArkofus = (ArkofusEntity) entityTarget;

                //Si le joueur est introuvable on ne fait rien, ce qui ne fait aucun dégat.
                //Si l'entité frappé n'est pas une EntityArkofus on return zéro dégât
                if (arkofusPlayer == null || entityArkofus == null) return;

                if (itemStack.getItem() instanceof ItemWeapon) {
                    //On récupère notre ItemWeapon
                    ItemWeapon itemWeapon = (ItemWeapon) itemStack.getItem();
                    IWeaponDamage weaponDamage = itemWeapon.getWeaponDamage();
                    double base = (double) itemWeapon.getAttack(weaponDamage);

                    double damageA = base * ((100 + arkofusPlayer.getMasteryWeapon()) / 100);
                    double damageB = (100 + base + arkofusPlayer.getPuissance()) / 100;

                    double finalDamage = damageA * damageB + arkofusPlayer.getDamage();

                    //On récupère le nb de coup critique du joueur avec les valeurs
                    float critical = arkofusPlayer.getCritical();
                    boolean isCritical = ThreadLocalRandom.current().nextFloat() <= critical;

                    if (isCritical) finalDamage += itemWeapon.getAttackCritical(weaponDamage) + arkofusPlayer.getDamageCritical();

                    entityArkofus.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) entityAttacker), Math.clamp(finalDamage));

                    if (isCritical) entityAttacker.sendMessage(new TextComponentString(
                            TextFormatting.RED + "[Coup Critique] " + TextFormatting.RESET + TextFormatting.GREEN +
                                    "Vous avez infligé " + finalDamage + " points de dégât."));
                    else
                        entityAttacker.sendMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez infligé " + finalDamage + " points de dégât."));

                }
            }

        }

    }

}
