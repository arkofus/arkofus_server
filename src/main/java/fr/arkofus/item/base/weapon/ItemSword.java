package fr.arkofus.item.base.weapon;

import fr.arkofus.item.ItemType;

import java.util.Collection;

/**
 * Class créée le 31/07/2018 à 10:58
 * par Jullian Dorian
 */
public abstract class ItemSword extends ItemWeapon {

    public ItemSword(String name, String description){
        this(name, 1, 1, description);
    }

    public ItemSword(String name, int level, int pods, String description) {
        super(name, description, level, ItemType.SWORD, pods);
    }

    /**
     * <p>Si des choses ont besoin d'être ajouté</p>
     *
     * @return content
     */
    @Override
    protected Collection<String> getContent() {
        return null;
    }

}
