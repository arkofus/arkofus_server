package fr.arkofus.item.base.weapon;

import fr.arkofus.characteristic.ElementType;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 14/11/2018 à 16:24
 * par Jullian Dorian
 */
public class ItemSwordTulante extends ItemSword {

    public ItemSwordTulante() {
        super("sword_tulante", "");
    }

    @Override
    public List<IWeaponDamage> getPropertiesAttack() {
        return Arrays.asList(new IWeaponDamage() {
            @Override
            public ElementType getElement() {
                return ElementType.NEUTRE;
            }

            @Override
            public int getMinAttack() {
                return 5;
            }

            @Override
            public int getMaxAttack() {
                return 6;
            }

            @Override
            public float getCriticalDamage() {
                return 5;
            }
        });
    }

    @Override
    public IWeaponDamage getWeaponDamage() {
        return getPropertiesAttack().get(this.random.nextInt(getPropertiesAttack().size()));
    }

    @Override
    public float getAttackCritical(IWeaponDamage weaponDamage) {
        return weaponDamage.getCriticalDamage();
    }

    @Override
    public float getAttack(IWeaponDamage weaponDamage) {
        return (float) this.random.nextInt(weaponDamage.getMinAttack(), weaponDamage.getMaxAttack()+1);
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
