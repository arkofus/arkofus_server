package fr.arkofus.item.base;

import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import fr.arkofus.panoplie.SetBase;
import fr.arkofus.utils.KeyHelper;
import fr.arkofus.utils.SpecialString;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 30/07/2018 à 16:13
 * par Jullian Dorian
 */
public abstract class ItemBase extends Item implements IItemBase {

    private final String name;
    private final String description;
    private final int level;
    private final ItemType itemType;
    private final int pods;

    private SetBase set;

    protected final ThreadLocalRandom random;

    public ItemBase(String name, String description, ItemType itemType){
        this(name, description, 1, itemType, 1);
    }

    public ItemBase(String name, String description, int level, ItemType itemType){
        this(name, description, level, itemType, 1);
    }

    public ItemBase(String name, String description, int level, ItemType itemType, int pods){
        this.name = name;
        this.description = description;
        this.level = level;
        this.itemType = itemType;
        this.pods = pods;
        this.set = null;

        //Instance minecraft
        this.setUnlocalizedName(name);
        this.setMaxStackSize(200);
        this.setCreativeTab((CreativeTabs) null);
        this.random = ThreadLocalRandom.current();
    }

    /**
     * <p>Retourne le nom de l'item</p>
     *
     * @return name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <p>Retourne la description de l'item</p>
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <p>Retourne le niveau de l'item</p>
     *
     * @return level
     */
    @Override
    public int getLevel() {
        return level;
    }

    /**
     * <p>Retourne le type de l'item</p>
     *
     * @return type
     * @see ItemType
     */
    @Override
    public ItemType getType() {
        return itemType;
    }

    /**
     * <p>Retourne le nombre de pods que prend l'item à l'unité</p>
     *
     * @return pods
     */
    @Override
    public int getPods() {
        return pods;
    }

    @Override
    public void setSet(SetBase set) {
        this.set = set;
    }

    @Override
    public SetBase getSet() {
        return set;
    }

    /**
     * <p>Si des choses ont besoin d'être ajouté</p>
     * @return content
     */
    protected abstract Collection<String> getContent();

    /**
     * Called when a player drops the item into the world,
     * returning false from this will prevent the item from
     * being removed from the players inventory and spawning
     * in the world
     *
     * @param item   The item stack, before the item is removed.
     * @param player The player that dropped the item
     */
    @Override
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player) {
        return false;
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     *
     * @param stack
     * @param playerIn
     * @param tooltip
     * @param advanced
     */
    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        super.addInformation(stack, playerIn, tooltip, advanced);

        tooltip.add(TextFormatting.DARK_GRAY + "Niveau : " + getLevel());
        tooltip.add("");

        tooltip.add("Type : " + getType());
        tooltip.add("Pods : " + getPods());

        tooltip.add("");

        if(KeyHelper.isCtrlKeyDown()) {
            if(hasSet()) {
                tooltip.add("Panoplie : " + getSet().getName());
            }
        } else if(KeyHelper.isAltKeyDown()){
            if(!getDescription().isEmpty()){
                tooltip.addAll(SpecialString.convertDescription(getDescription(), 7));
            } else {
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "Cet objet n'est pas encore assez connu");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "des artisans pour que ses caractéristiques");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "puissent être révélées aux éventuels acheteurs.");
            }
        } else {
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur CTRL pour afficher les caractéristiques.");
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur SHIFT pour afficher les bonus.");
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur ALT pour afficher la description.");
        }
    }

    /**
     * <p>Retourne le type d'armure pour être placé sur les slots de l'inventaire</p>
     *
     * @return armorType
     */
    @Override
    public ArmorType getTypeArmor() {
        return ArmorType.UNAUTHORIZED;
    }
}
