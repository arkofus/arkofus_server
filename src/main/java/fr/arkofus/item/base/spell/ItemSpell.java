package fr.arkofus.item.base.spell;

import com.google.common.collect.Lists;
import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.utils.KeyHelper;
import fr.arkofus.utils.SpecialString;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.Collection;
import java.util.List;

/**
 * Class créée le 30/07/2018 à 16:13
 * par Jullian Dorian
 */
public abstract class ItemSpell extends ItemBase implements IItemSpell{

    public ItemSpell(String name, String description, int level){
        super(name, description, level, ItemType.SPELL);

        //Minecraft instance
        this.setDamage(new ItemStack(this), 0);
        this.setMaxDamage(-1);
        this.setMaxStackSize(1);
        this.setCreativeTab(ArkofusTabs.Type.CLASSES.getCreativeTab());
    }

    @Override
    public boolean isSpell() {
        return true;
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player) {
        return false;
    }

    @Override
    protected Collection<String> getContent() {
        return Lists.newArrayList();
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {

        ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(playerIn.getUniqueID());

        ISpell<?> spell = getSpell();
        spell.prepare(playerIn, arkofusPlayer);

        tooltip.add(TextFormatting.DARK_GRAY + "Niveau : " + getLevel());
        tooltip.add("");

        tooltip.add("Portée " + (spell.isReachModifiable() ? "(Modifiable)" : "") + " : " + spell.getMinReach() + " - " + spell.getMaxReach());
        if(spell.hasEffectZone()) tooltip.add("Zone d'effet : " + spell.getEffectZone());
        tooltip.add("Critique : " + (spell.getCritical() * 100) + "%");
        if(spell.isLine()) tooltip.add("Lancer en ligne uniquement");
        tooltip.add("Délais de récupération : " + spell.getMaxCooldown() + "s");
        tooltip.add("(Bientôt) Limitation par cible : " + spell.getLimitPerCible()); //Peux en avoir 3
        tooltip.add("Utilisation avant délais : " + spell.getLimitToDelay());   //Peux en avoir 5
        /*
        tooltip.add("Coût : " + getType());                         => DEVIENT =>   RIEN
        tooltip.add("Portée [(modifiable)] : " + getReach());        => DEVIENT =>  IDEM
        tooltip.add("Zone d'effet : ");                             => DEVIENT =>   IDEM
        tooltip.add("Critique : ");                                 => DEVIENT =>   IDEM
        tooltip.add("Classe : " + "Enutrof");                       => DEVIENT =>   RIEN
        tooltip.add("Lancer en ligne uniquement");                  => DEVIENT =>   IDEM
        tooltip.add("Limitation par tour par cible : ");            => DEVIENT =>   Limitation par cible
        tooltip.add("Cumul max des effets :");                      => DEVIENT => RIEN
        tooltip.add("Utilisations par tour : ");                    => DEVIENT =>   Utilisation avant délais1
         */

        tooltip.add("");

        if(KeyHelper.isAltKeyDown()){
            if(!getDescription().isEmpty()){
                tooltip.addAll(SpecialString.convertDescription(getDescription(), 7));
            } else {
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "Cet objet n'est pas encore assez connu");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "des artisans pour que ses caractéristiques");
                tooltip.add(TextFormatting.YELLOW + "" + TextFormatting.ITALIC + "puissent être révélées aux éventuels acheteurs.");
            }
        } else {
            tooltip.add(TextFormatting.YELLOW + "Appuyer sur ALT pour afficher la description.");
        }

    }

    /**
     * <p>Action effectue quand le joueur va effectuer un clic droit</p>
     * @param world Map ou se situe le joueur
     * @param entityPlayer Joueur
     * @param hand Main utilisé
     * @return actionResult
     */
    public abstract ActionResult<ItemStack> itemRightClick(World world, EntityPlayer entityPlayer, EnumHand hand, ItemStack itemStack);

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {

        ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(playerIn.getUniqueID());

        ISpell<?> spell = getSpell();
        spell.prepare(playerIn, arkofusPlayer);

        ItemStack itemStack = playerIn.getHeldItem(handIn);

        boolean canUse = false;
        int seconds = 0;

        if(!itemStack.hasTagCompound()){

            NBTTagCompound itemCompound = new NBTTagCompound();
            itemCompound.setInteger("maxCooldown", spell.getMaxCooldown() * 20); //Pour l'avoir en tick
            itemCompound.setInteger("cooldown", 0); //Le cooldown en tick
            //itemCompound.setInteger("limitPerCible", spell.getLimitPerCible());
            //itemCompound.setInteger("useOnCible", 0);
            itemCompound.setInteger("uses", 0);
            itemCompound.setInteger("limitToDelay", spell.getLimitToDelay());
            itemCompound.setBoolean("canUse", true); //Si le sort peut être lancé
            itemStack.setTagCompound(itemCompound);
            canUse = true;
        } else {
            canUse = itemStack.getTagCompound().getBoolean("canUse");
            if(!canUse) seconds = itemStack.getTagCompound().getInteger("cooldown");
        }

        if(canUse){
            int uses =  itemStack.getTagCompound().getInteger("uses") + 1;
            //int useOnCible =  itemStack.getTagCompound().getInteger("useOnCible") + 1;

            if(uses >= itemStack.getTagCompound().getInteger("limitToDelay")){
                itemStack.getTagCompound().setBoolean("canUse", false);
                //On effectue le onUpdate
                //On appelle la methode
                itemRightClick(worldIn, playerIn, handIn, itemStack);
                playerIn.getCooldownTracker().setCooldown(this, spell.getMaxCooldown() * 20);
                //On retourne l'item
                return new ActionResult<>(EnumActionResult.SUCCESS, itemStack);
            } else {
                itemStack.getTagCompound().setInteger("uses", uses);
                //On retourne le sort
                return itemRightClick(worldIn, playerIn, handIn, itemStack);
            }
        }

        playerIn.sendMessage(new TextComponentString("Vous ne pouvez pas lancer le sort maintenant, encore : " + (spell.getMaxCooldown() - (seconds / 20)) + "s"));
        return new ActionResult<>(EnumActionResult.SUCCESS, itemStack);
    }

    @Override
    public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        super.onUpdate(stack, worldIn, entityIn, itemSlot, isSelected);

        if(stack.hasTagCompound()) {
            if(stack.getTagCompound().hasKey("canUse")) {
                if (!stack.getTagCompound().getBoolean("canUse")) {

                    int maxCool = stack.getTagCompound().getInteger("maxCooldown");
                    int cooldown = stack.getTagCompound().getInteger("cooldown");

                    int c = cooldown + 1 < maxCool ? cooldown + 1 : 0;

                    stack.getTagCompound().setInteger("cooldown", c);

                    if (c == 0) {
                        stack.getTagCompound().setInteger("uses", 0);
                        stack.getTagCompound().setBoolean("canUse", true);
                    }
                }
            }
        }
    }

}
