package fr.arkofus.item.base.spell;

import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.IItemBase;

/**
 * Class créée le 30/07/2018 à 16:12
 * par Jullian Dorian
 */
public interface IItemSpell extends IItemBase {

    @Override
    default ItemType getType(){
        return ItemType.SPELL;
    }

    ISpell<?> getSpell();
}
