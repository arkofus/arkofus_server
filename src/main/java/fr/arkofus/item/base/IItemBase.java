package fr.arkofus.item.base;

import fr.arkofus.item.ArmorType;
import fr.arkofus.item.ItemType;
import fr.arkofus.panoplie.SetBase;

/**
 * Class créée le 30/07/2018 à 16:05
 * par Jullian Dorian
 */
public interface IItemBase {

    /**
     * <p>Retourne le nom de l'item</p>
     * @return name
     */
    String getName();

    /**
     * <p>Retourne la description de l'item</p>
     * @return description
     */
    String getDescription();

    /**
     * <p>Retourne le niveau de l'item</p>
     * @return level
     */
    int getLevel();

    /**
     * <p>Retourne le type de l'item</p>
     * @see ItemType
     * @return type
     */
    ItemType getType();

    /**
     * <p>Retourne le nombre de pods que prend l'item à l'unité</p>
     * @return pods
     */
    int getPods();

    /**
     * <p>Applique une panoplie à l'item</p>
     * @param set
     */
    void setSet(SetBase set);

    /**
     * <p>Retourne le set associé</p>
     * @return set
     */
    SetBase getSet();

    default boolean hasSet(){
        return getSet() != null;
    }

    /**
     * <p>Retourne le type d'armure pour être placé sur les slots de l'inventaire</p>
     * @return armorType
     */
    default ArmorType getTypeArmor(){
        return ArmorType.UNAUTHORIZED;
    }

    /**
     * <p>Retourne le nombre total de pods en fonction de la quantité des items</p>
     * @param qttItem - La quantité de l'item en main
     * @return totalPods
     */
    default int getTotalPods(int qttItem){
        return getPods() * qttItem;
    }

    /**
     * <p>Retourne la valeur true ou false pour savoir si l'item est un objet de quête ou non</p>
     * <p>Si c'est le cas il sera donc indestructible et sera placé dans les objets de quête</p>
     * @return isItemQuest
     */
    default boolean isItemQuest(){
        return false;
    }

    /**
     * <p>Retourne vrai si l'item est un sort</p>
     * @return false par défaut
     */
    default boolean isSpell(){
        return false;
    }
}
