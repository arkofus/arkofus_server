package fr.arkofus.item.base.tool;

import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.weapon.ItemWeapon;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class créée le 30/10/2018 à 16:13
 * par Jullian Dorian
 */
public abstract class ItemPickaxe extends ItemWeapon {

    public ItemPickaxe(String name, String description) {
        super(name, description, 1, ItemType.PICKAXE, 1);
    }

    @Override
    protected Collection<String> getContent() {
        return new ArrayList<>();
    }


}
