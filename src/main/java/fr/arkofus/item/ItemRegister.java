package fr.arkofus.item;

import fr.arkofus.Constants;
import fr.arkofus.item.armor.intrepide.*;
import fr.arkofus.item.base.armor.*;
import fr.arkofus.item.icon.ItemIcon;
import fr.arkofus.item.ressources.ItemRessourceRegister;
import fr.arkofus.item.spell.ItemSpellCoinsThrowing;
import fr.arkofus.item.tool.ItemPickaxeMiner;
import fr.arkofus.item.base.weapon.ItemSwordTulante;
import fr.arkofus.panoplie.intrepide.SetIntrepide;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Class créée le 30/07/2018 à 16:03
 * par Jullian Dorian
 */
public class ItemRegister {

    public static final ItemIcon TAB_CLASSE = new ItemIcon("tab_classe");
    public static final ItemIcon TAB_WEAPON = new ItemIcon("tab_weapon");
    public static final ItemIcon TAB_JOB = new ItemIcon("tab_job");
    public static final ItemIcon TAB_RESOURCE = new ItemIcon("tab_resource");
    public static final ItemIcon TAB_EQUIPMENT = new ItemIcon("tab_equipment");

    public static final ItemPickaxeMiner PICKAXE_MINER = new ItemPickaxeMiner();

    public static final ItemHatIntrepide        HAT_INTREPIDE = new ItemHatIntrepide();
    public static final ItemSwordTulante        SWORD_TULANTE = new ItemSwordTulante();
    public static final ItemCapeIntrepide       CAPE_INTREPIDE = new ItemCapeIntrepide();
    public static final ItemRingIntrepide       RING_INTREPIDE = new ItemRingIntrepide();
    public static final ItemAmuletIntrepide     AMULET_INTREPIDE = new ItemAmuletIntrepide();
    public static final ItemBootIntrepide       BOOT_INTREPIDE = new ItemBootIntrepide();
    public static final ItemCeintureIntrepide   CEINTURE_INTREPIDE = new ItemCeintureIntrepide();
    public static final ItemShieldIntrepide     SHIELD_INTREPIDE = new ItemShieldIntrepide();

    public static final ItemSpellCoinsThrowing ITEM_SPELL_COINS_THROWING = new ItemSpellCoinsThrowing("item_spell_coins_throwing", "", 1);

    public static final SetIntrepide SET_INTREPIDE = new SetIntrepide();

    /**
     *
     */
    public static void commonPreInitialization(){
        registerItems(TAB_CLASSE, TAB_WEAPON, TAB_JOB, TAB_RESOURCE, TAB_EQUIPMENT);

        ItemRessourceRegister.items.forEach(ItemRegister::registerItem);

        registerItem(PICKAXE_MINER);
        registerItem(ITEM_SPELL_COINS_THROWING);

        SET_INTREPIDE.fill(HAT_INTREPIDE, SWORD_TULANTE, CAPE_INTREPIDE, RING_INTREPIDE, AMULET_INTREPIDE, BOOT_INTREPIDE, CEINTURE_INTREPIDE, SHIELD_INTREPIDE);
        registerItems(SET_INTREPIDE.getItems());
    }

    /**
     *
     */
    @SideOnly(Side.CLIENT)
    public static void clientPreInitialization(){
        registerRenders(TAB_CLASSE, TAB_WEAPON, TAB_JOB, TAB_RESOURCE);

        ItemRessourceRegister.items.forEach(ItemRegister::registerRender);

        registerRender(PICKAXE_MINER);
        registerRender(ITEM_SPELL_COINS_THROWING);

        registerRenders(SET_INTREPIDE.getItems());
    }

    /**
     * <p>SIDE COMMON ; Enregistre l'item dans le jeu</p>
     * @param item - L'item à enregistrer
     */
    public static void registerItem(Item item){
        GameRegistry.register(item, new ResourceLocation(Constants.MODID, item.getUnlocalizedName().substring(Constants.SUBSTRING)));
    }

    private static void registerItems(Item... itemBases){
        for (Item itemBase : itemBases)
            registerItem(itemBase);
    }

    /**
     * @see #registerRender(Item, int) - Enregistrer avec les metadatas
     * @param item - L'item à enregistrer le rendu
     */
    @SideOnly(Side.CLIENT)
    private static void registerRender(Item item){
        registerRender(item, 0);
    }

    /**
     * @see #registerRender(Item, int) - Enregistrer avec les metadatas
     * @param itemBases - L'item à enregistrer le rendu
     */
    @SideOnly(Side.CLIENT)
    private static void registerRenders(Item... itemBases){
        for (Item itemBase : itemBases)
            registerRender(itemBase, 0);
    }

    /**
     * <p>Enregistrer le rendu de l'item</p>
     * @param item - L'item qui aura le rendu
     * @param metadata - Si l'item a plusieurs metadata
     */
    @SideOnly(Side.CLIENT)
    private static void registerRender(Item item, int metadata){
        String resourceName = Constants.MODID + ":" + item.getUnlocalizedName().substring(Constants.SUBSTRING);
        ModelLoader.setCustomModelResourceLocation(item, metadata, new ModelResourceLocation(resourceName, "inventory"));
    }

    public static void registerItemBlock(Block block) {
        ItemBlock itemBlock = new ItemBlock(block);
        String name = block.getUnlocalizedName().substring(Constants.SUBSTRING);
        itemBlock.setUnlocalizedName(Constants.MODID + ":" + name);
        GameRegistry.register(itemBlock, new ResourceLocation(Constants.MODID, name));
    }
}
