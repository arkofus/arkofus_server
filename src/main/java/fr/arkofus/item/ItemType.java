package fr.arkofus.item;

/**
 * Created by Arkas on 29/09/2017.
 * at 12:27
 */
public enum ItemType {

    UNKNOWN("Not defined"),

    AILE("Ailes"),
    ALLIAGE("Alliage"),
    BOIS("Bois"),
    BOISSON("Boisson"),
    BOITE_FRAGMENTS("Boite de fragments"),
    BOURGEON("Bourgeon"),
    CARAPACE("Carapace"),
    CARTE("Carte"),
    CHAMPIGNON("Champignon"),
    CLEF("Clef"),
    COQUILLE("Coquille"),
    COFFRE("Coffre"),
    CONTENEUR("Conteneur"),
    CUIR("Cuir"),
    CEREALE("Céréale"),
    ESSENCE_DONJON("Essence de Gardien de Donjon"),
    ETOFFE("Étoffe"),
    FARINE("Farine"),
    FLEUR("Fleur"),
    FRAGMENT_CARTE("Fragment de carte"),
    FRUIT("Fruit"),
    GALET("Galet"),
    GELEE("Gelée"),
    GRAINE("Graine"),
    HUILE("Huile"),
    IDOLE("Idole"),
    LAINE("Laine"),
    LEGUME("Légume"),
    MATERIEL("Matériel"),
    METARIA("Metaria"),
    MINERAI("Minerai"),
    NOWEL("Nowel"),
    OEIL("Oeil"),
    OEUF("Oeuf"),
    OREILLE("Oreille"),
    OS("Os"),
    PAIN("Pain"),
    PARCHEMIN_CARACTERISTIQUES("Parchemin de caractéristique"),
    PARCHEMIN_EXPERIENCE("Parchemin d'expérience"),
    PARCHEMIN_SORTILEGE("Parchemin de sortilège"),
    PATTE("Patte"),
    PEAU("Peau"),
    PELUCHE("Peluche"),
    PIERRE_BRUTE("Pierre brute"),
    PIERRE_PRECIEUSE("Pierre précieuse"),
    PIERRE_MAGIQUE("Pierre magique"),
    PLANCHE("Planche"),
    PLANTE("Plante"),
    PLUME("Plume"),
    POIL("Poil"),
    POISSON("Poisson"),
    POISSON_COMESTIBLE("Poisson comestible"),
    POISSON_VIDE("Poisson vidé"),
    POTION("Potion"),
    POTION_TELEPORTATION("Potion de téléportation"),
    POUDRE("Poudre"),
    PREPARATION("Préparation"),
    QUEUE("Queue"),
    RACINE("Racine"),
    RESSOURCE_DIVERSE("Ressource diverse"),
    SOUVENIR("Souvenir"),
    SUBSTRAT("Substrat"),
    SEVE("Sève"),
    TEINTURE("Teinture"),
    VIANDE("Viande"),
    VIANDE_COMESTIBLE("Viande comestible"),
    VETEMENT("Vêtement"),
    ECORCE("Écorce"),

    SWORD("Epée"),
    AXE("Hache"),
    BOW("Arc"),
    PICKAXE("Pioche"),

    HAT("Chapeau"),
    CAPE("Cape"),
    BOOTS("Botte"),
    RING("Anneau"),
    AMULET("Amulette"),
    CEINTURE("Ceinture"),
    DOFUS("Dofus"),
    TROPHY("Trophée"),
    SHIELD("Bouclier"),

    SPELL("Sort");

    private String type;

    ItemType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
