package fr.arkofus.item.tool;

import fr.arkofus.block.jobs.miner.MinerBlockOre;
import fr.arkofus.characteristic.ElementType;
import fr.arkofus.item.base.tool.ItemPickaxe;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 30/10/2018 à 16:13
 * par Jullian Dorian
 */
public class ItemPickaxeMiner extends ItemPickaxe {

    public ItemPickaxeMiner() {
        super("pickaxe_miner", "Outils indispensable à la collecte des minerais.");
    }

    @Override
    public boolean canHarvestBlock(IBlockState state, ItemStack stack) {
        return state.getBlock() instanceof MinerBlockOre;
    }

    @Override
    public int getHarvestLevel(ItemStack stack, String toolClass, @Nullable EntityPlayer player, @Nullable IBlockState blockState) {
        return 2;
    }

    @Override
    public float getStrVsBlock(ItemStack stack, IBlockState state) {
        return 2.5f;
    }

    @Override
    public List<IWeaponDamage> getPropertiesAttack() {
        return Arrays.asList(new IWeaponDamage() {
            @Override
            public ElementType getElement() {
                return ElementType.NEUTRE;
            }

            @Override
            public int getMinAttack() {
                return 2;
            }

            @Override
            public int getMaxAttack() {
                return 4;
            }

            @Override
            public float getCriticalDamage() {
                return 5;
            }
        });
    }

    @Override
    public IWeaponDamage getWeaponDamage() {
        return getPropertiesAttack().get(this.random.nextInt(getPropertiesAttack().size()));
    }

    @Override
    public float getAttack(IWeaponDamage weaponDamage) {
        return this.random.nextInt(weaponDamage.getMinAttack(), weaponDamage.getMaxAttack()+1);
    }

    @Override
    public float getAttackCritical(IWeaponDamage weaponDamage) {
        return weaponDamage.getCriticalDamage();
    }
}
