package fr.arkofus.item;

/**
 * Class créée le 18/08/2018 à 12:22
 * par Jullian Dorian
 */
public enum ArmorType {

    AMULETTE,
    ANNEAU,
    CHAPEAU,
    CAPE,
    CEINTURE,
    BOTTE,
    ARME,
    BOUCLIER,
    DOFUS,
    UNAUTHORIZED

}
