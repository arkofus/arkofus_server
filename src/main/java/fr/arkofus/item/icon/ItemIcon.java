package fr.arkofus.item.icon;

import com.google.common.collect.Lists;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;

import java.util.Collection;

/**
 * Class créée le 30/07/2018 à 16:59
 * par Jullian Dorian
 */
public class ItemIcon extends ItemBase {

    public ItemIcon(String name){
        this(name, "Icon");
    }

    public ItemIcon(String name, String description) {
        super(name, description, 0, ItemType.UNKNOWN, 0);
        this.setMaxStackSize(1);
    }

    /**
     * <p>Si des choses ont besoin d'être ajouté</p>
     *
     * @return content
     */
    @Override
    protected Collection<String> getContent() {
        return Lists.newArrayList();
    }
}
