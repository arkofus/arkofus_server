package fr.arkofus.item.spell;

import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.enutrof.CoinsThrowingSpell;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.entities.spell.EntityCoinsThrowing;
import fr.arkofus.item.base.spell.ItemSpell;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

/**
 * Class créée le 31/07/2018 à 12:58
 * par Jullian Dorian
 */
public class ItemSpellCoinsThrowing extends ItemSpell {

    public ItemSpellCoinsThrowing(String name, String description, int level) {
        super(name, description, level);
    }

    @Override
    public ISpell<?> getSpell() {
        return new CoinsThrowingSpell();
    }

    @Override
    public ActionResult<ItemStack> itemRightClick(World world, EntityPlayer entityPlayer, EnumHand hand, ItemStack itemStack) {

        if(!world.isRemote){
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());
            CoinsThrowingSpell spell = (CoinsThrowingSpell) getSpell();

            if(spell != null && arkofusPlayer.canUseSpell(spell)){

                EntityCoinsThrowing entitySpell = spell.getEntity(world, entityPlayer);
                entitySpell.setHeadingFromThrower(entityPlayer, entityPlayer.rotationPitch, entityPlayer.rotationYaw,0, 1f, 1f);
                entitySpell.setRotationYawHead(entityPlayer.rotationYaw);
                spell.display(world, entitySpell);

            } else {
                entityPlayer.sendMessage(new TextComponentString("Impossible de faire apparaitre le sort."));
            }
        }

        return new ActionResult<>(EnumActionResult.SUCCESS, itemStack);
    }

}
