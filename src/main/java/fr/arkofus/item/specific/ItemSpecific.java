package fr.arkofus.item.specific;

import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;

import java.util.Collection;

/**
 * Class créée le 08/12/2018 à 18:09
 * par Jullian Dorian
 */
public abstract class ItemSpecific extends ItemBase {

    public ItemSpecific(String name) {
        super(name, "", ItemType.UNKNOWN);
    }

    @Override
    protected Collection<String> getContent() {
        return null;
    }
}
