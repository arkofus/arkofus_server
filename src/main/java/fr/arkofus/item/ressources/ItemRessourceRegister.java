package fr.arkofus.item.ressources;

import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 29/10/2018 à 22:45
 * par Jullian Dorian
 */
public class ItemRessourceRegister {

    public static final ItemRessource SILVER = new ItemRessource("silver", ItemType.MINERAI, 120, "L'argent est un métal brillant très apprécié des bijoutiers, contrairement à ce que certaines personnes essaient de vous faire croire, il n'a aucun effet particulier sur les vampires.");
    public static final ItemRessource BAUXITE = new ItemRessource("bauxite", ItemType.MINERAI, 140, "Ce métal est très apprécié pour sa couleur originale et pour son tranchant lorsqu'il est correctement aiguisé.");
    public static final ItemRessource BRONZE = new ItemRessource("bronze", ItemType.MINERAI, 40, "Ce métal entre souvent dans la fabrication d'articles ornementaux, pour embellir vos armes à moindre frais, c'est le métal idéal.");
    public static final ItemRessource COPPER = new ItemRessource("copper", ItemType.MINERAI, 20, "Le Cuivre est un métal très utile pour confectionner les armes bon marché, c'est presque un ersatz de l'or si on fait abstraction de toutes les qualités de l'or.");
    public static final ItemRessource DOLOMITE = new ItemRessource("dolomite", ItemType.MINERAI, 180, "\"Toujours dur, et incassable\" telles étaient les paroles d'Irko le Mineur Sombre à propos du Dolomite.");
    public static final ItemRessource SEPIOLITE = new ItemRessource("sepiolite", ItemType.MINERAI, 200, "Ce tendre minéral blanc a la couleur et la légèreté de l'écume, d'où son nom. Après séchage, il devient dur comme le cœur d'une belle-mère. Sa résistance à la chaleur et sa porosité en font un " +
            "matériau de choix pour la fabrication de pipes.");
    public static final ItemRessource TIN = new ItemRessource("tin", ItemType.MINERAI, 100, "L'Etain est un métal mou et lourd, un peu comme le Wo Wabbit, mais en pire.");
    public static final ItemRessource IRON = new ItemRessource("iron", ItemType.MINERAI, 1, "Le fer est très utile pour les forgerons et pour ceux qui utilisent du métal, certains s'en servent même pour repasser.");
    public static final ItemRessource COBALT = new ItemRessource("cobalt", ItemType.MINERAI, 60, "Le nom « Kobalte » signifie « cuivre du diable » en Sadida ancien, c'est un métal adapté à la forge d'armes légères et élégantes, mais avant tout mortelles.");
    public static final ItemRessource MANGANESE = new ItemRessource("manganese", ItemType.MINERAI, 80, "Contrairement à ce que tout le monde pense, ce métal ne sert pas à faire des gouttières, mais plutôt à faire des armes terrifiantes, comme le redoutable \"Tranchant Manganésien\" qui a estourbi plus d'un Tofu lors des fêtes du Boulgour.");
    public static final ItemRessource OBSIDIAN = new ItemRessource("obsidian", ItemType.MINERAI, 200, "Assez dure pour rayer le verre et assez sombre pour vous saper le moral, cette roche a tout perdu de la chaleur de la lave dont elle est issue. En tout cas, elle sera parfaite pour une pierre tombale, tant que ce n'est pas la vôtre.");
    public static final ItemRessource GOLD = new ItemRessource("gold", ItemType.MINERAI, 160, "L'or est un métal rare et précieux, ses qualités intrinsèques en font un métal de choix pour la conception d'armes ou d'objets particulièrement puissants et d'esthétique raffinée.");
    public static final ItemRessource SILICATE = new ItemRessource("silicate", ItemType.MINERAI, 100, "Ce minerai provient sûrement des profondes mines sombres de l'île de Pandala dans lesquelles Gardan Framine est tombé éperdument amoureux d'un spectre de femelle Pandawa. Comme quoi, l'amour ne connaît aucune limite.");


    public static List<ItemBase> items = Arrays.asList(
            BAUXITE, BRONZE, SILICATE, SILVER, COPPER, DOLOMITE, SEPIOLITE, TIN, IRON, COBALT, MANGANESE, OBSIDIAN, GOLD
    );


}
