package fr.arkofus.item.ressources;

import fr.arkofus.creativetabs.ArkofusTabs;
import fr.arkofus.item.ItemType;
import fr.arkofus.item.base.ItemBase;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class créée le 29/10/2018 à 22:40
 * par Jullian Dorian
 */
public class ItemRessource extends ItemBase {

    public ItemRessource(String name, ItemType itemType, int level, String description) {
        super(name, description, level, itemType);
        this.setCreativeTab(ArkofusTabs.Type.RESSOURCES.getCreativeTab());
    }

    @Override
    protected Collection<String> getContent() {
        return new ArrayList<>();
    }
}
