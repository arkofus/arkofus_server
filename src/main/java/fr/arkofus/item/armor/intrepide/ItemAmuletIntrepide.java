package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemAmulet;

/**
 * Class créée le 15/11/2018 à 13:35
 * par Jullian Dorian
 */
public class ItemAmuletIntrepide extends ItemAmulet {

    public ItemAmuletIntrepide() {
        super("amulet_intrepide", "");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
