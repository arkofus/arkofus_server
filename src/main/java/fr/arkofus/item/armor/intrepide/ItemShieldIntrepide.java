package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemShield;

/**
 * Class créée le 15/11/2018 à 13:36
 * par Jullian Dorian
 */
public class ItemShieldIntrepide extends ItemShield {

    public ItemShieldIntrepide(){
        super("shield_intrepide", "");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
