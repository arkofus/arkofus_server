package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemCape;

/**
 * Class créée le 15/11/2018 à 13:32
 * par Jullian Dorian
 */
public class ItemCapeIntrepide extends ItemCape {

    public ItemCapeIntrepide(){
        super("cape_intrepide", "");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
