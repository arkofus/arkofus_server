package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemRing;

/**
 * Class créée le 15/11/2018 à 13:33
 * par Jullian Dorian
 */
public class ItemRingIntrepide extends ItemRing {

    public ItemRingIntrepide(){
        super("ring_intrepide", "");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
