package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemHat;

/**
 * Class créée le 15/11/2018 à 13:30
 * par Jullian Dorian
 */
public class ItemHatIntrepide extends ItemHat {

    public ItemHatIntrepide() {
        super("hat_intrepide","");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
