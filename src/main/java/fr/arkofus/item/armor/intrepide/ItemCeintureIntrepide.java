package fr.arkofus.item.armor.intrepide;

import fr.arkofus.item.base.armor.ItemCeinture;

/**
 * Class créée le 15/11/2018 à 13:34
 * par Jullian Dorian
 */
public class ItemCeintureIntrepide extends ItemCeinture {

    public ItemCeintureIntrepide(){
        super("ceinture_intrepide", "");
    }

    @Override
    public int getVitalite() {
        return 1;
    }

    @Override
    public int getSagesse() {
        return 1;
    }
}
