package fr.arkofus.server.event;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

/**
 * Class créée le 24/08/2018 à 18:13
 * par Jullian Dorian
 */
public class PlayerListener {

    @SubscribeEvent(receiveCanceled = true)
    public void onPlayerDropItem(PlayerDropsEvent event){

        if(!event.getEntityPlayer().isCreative()){
            event.setCanceled(true);
        }

    }

    @SubscribeEvent(receiveCanceled = true)
    public void onPlayerPickItem(EntityItemPickupEvent event){

        if(event.getEntity() instanceof EntityPlayer){
            if(!((EntityPlayer) event.getEntity()).isCreative()){
                event.setCanceled(true);
            }
        }

    }

    private int tick = 0;

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event){
        if(event.type == TickEvent.Type.PLAYER && event.phase == TickEvent.Phase.START) {
            tick++;
            if (tick == 40) { //Soit 2 secondes

                ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(event.player.getUniqueID());
                if(!arkofusPlayer.isInFight()) {
                    double health = arkofusPlayer.getHealth();
                    if (health < arkofusPlayer.getMaxHealth()) {
                        arkofusPlayer.setHealth(health + 1d, true);
                        ArkofusServer.getMapPlayers().replace(event.player.getUniqueID(), arkofusPlayer);
                    }
                }

                tick = 0;
            }
        }
    }

}
