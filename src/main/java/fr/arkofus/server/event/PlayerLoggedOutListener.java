package fr.arkofus.server.event;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

/**
 * Class créée le 28/07/2018 à 15:22
 * par Jullian Dorian
 */
public class PlayerLoggedOutListener {

    @SubscribeEvent
    public void onPlayerLoggedOut(PlayerEvent.PlayerLoggedOutEvent event){

        EntityPlayer entityPlayer = event.player;

        ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());
        if(arkofusPlayer != null) arkofusPlayer.save();

        ArkofusServer.getMapPlayers().remove(entityPlayer.getUniqueID());

    }

}
