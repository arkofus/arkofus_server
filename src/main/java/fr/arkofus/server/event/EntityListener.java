package fr.arkofus.server.event;

import fr.arkofus.ArkofusServer;
import fr.arkofus.item.base.weapon.ItemWeapon;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Class créée le 24/08/2018 à 18:16
 * par Jullian Dorian
 */
public class EntityListener {

    @SubscribeEvent(receiveCanceled = true)
    public void onPlayerAttackEvent(AttackEntityEvent event){

        Entity target = event.getTarget();
        Entity damager = event.getEntity();

        if(target instanceof EntityPlayer && !(damager instanceof EntityPlayer)){
            event.setCanceled(true);
        }

        if(damager instanceof EntityPlayer){
            if(!((EntityPlayer) damager).isCreative()){
                ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(damager.getUniqueID());
                ItemStack itemStack = arkofusPlayer.getInventory().getCurrentItem();
                event.setCanceled(true);
                if(!itemStack.isEmpty()){
                    Item item = itemStack.getItem();
                    if(item instanceof ItemWeapon && arkofusPlayer.getInventory().currentItem == 0){
                        item.hitEntity(itemStack, (EntityLivingBase) target, (EntityPlayer) damager);
                    }
                }
            }
        }

    }

    @SubscribeEvent(receiveCanceled = true)
    public void onEntityDamageEvent(LivingAttackEvent event){

        Entity receiver = event.getEntity();

        if(receiver instanceof EntityPlayer){

            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(receiver.getUniqueID());
            arkofusPlayer.damage(event.getAmount());

            ArkofusServer.getMapPlayers().replace(receiver.getUniqueID(), arkofusPlayer);
            event.setCanceled(true);
        }

    }
}
