package fr.arkofus.server.event;

import fr.arkofus.ArkofusServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.server.FMLServerHandler;

/**
 * Class créée le 10/12/2018 à 14:26
 * par Jullian Dorian
 */
public class WorldListener {

    private float partialTicks = 0;

    public WorldListener(){}

    @SubscribeEvent
    public void onWorldTick(TickEvent.WorldTickEvent event){

        ArkofusServer.getZoneManager().update(event.world, partialTicks);

        if(event.phase == TickEvent.Phase.START){
            partialTicks++;
        }

        if(event.phase == TickEvent.Phase.END){
            if(partialTicks == 20){
                partialTicks = 0;
            }
        }

    }

}
