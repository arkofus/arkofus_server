package fr.arkofus.server.event;

import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.Classe;
import fr.arkofus.packet.client.CClassePacket;
import fr.arkofus.utils.Reader;
import fr.arkofus.packet.client.CCreateCharacterPacket;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import java.io.*;

/**
 * Class créée le 22/07/2018 à 16:21
 * par Jullian Dorian
 */
public class PlayerLoggedInListener {

    @SubscribeEvent
    public void onPlayerLoggedIn(PlayerEvent.PlayerLoggedInEvent event) throws IOException {

        final EntityPlayer entityPlayer = event.player;

        ArkofusPlayer empty = new ArkofusPlayer(entityPlayer);

        File pFile = empty.getFile();

        //Envoie toutes les données au client
        sendAll(entityPlayer);

        if(pFile.exists()){

            final Reader reader = new Reader(pFile);
            final String content = reader.read();

            final ArkofusPlayer player = ArkofusPlayer.fromJson(content);
            player.setEntityPlayer(entityPlayer);

            ArkofusServer.getMapPlayers().put(entityPlayer.getUniqueID(), player);

            player.synchronise();
            entityPlayer.inventory.setInventorySlotContents(0, player.getInventory().getCurrentItem());
            entityPlayer.inventory.currentItem = player.getInventory().currentItem;

        } else {
            if(pFile.createNewFile()){
                ArkofusServer.getMapPlayers().put(entityPlayer.getUniqueID(), empty);

                CCreateCharacterPacket createCharacterPacket = new CCreateCharacterPacket(true);
                RegistryPacket.getNetwork().sendTo(createCharacterPacket, (EntityPlayerMP) entityPlayer);
            }
        }

    }

    /**
     * <p>Permet d'envoyer toutes les données au client</p>
     * @param entityPlayer - Player
     */
    private synchronized void sendAll(EntityPlayer entityPlayer){

        for(Classe classe : ArkofusServer.registryClasse.getClasses()){
            RegistryPacket.getNetwork().sendTo(new CClassePacket(classe), (EntityPlayerMP) entityPlayer);
        }
    }

}
