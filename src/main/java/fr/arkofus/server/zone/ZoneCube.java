package fr.arkofus.server.zone;

/**
 * Class créée le 09/12/2018 à 22:23
 * par Jullian Dorian
 */
public class ZoneCube extends AbstractZone {

    public ZoneCube(){
        super(2);
    }

    public Position getFirst(){
        return get(0);
    }

    public Position getSecond(){
        return get(1);
    }

}
