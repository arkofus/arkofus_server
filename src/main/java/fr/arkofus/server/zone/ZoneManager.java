package fr.arkofus.server.zone;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.arkofus.ArkofusServer;
import fr.arkofus.json.adapter.ZoneAdapter;
import fr.arkofus.utils.Reader;
import net.minecraft.world.World;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Class créée le 09/12/2018 à 17:44
 * par Jullian Dorian
 */
public class ZoneManager {

    private File root;
    private Gson gsonZone;

    private Set<Zone> zoneSet = new HashSet<>();

    public ZoneManager(){
        this.root = ArkofusServer.modConfiguration.zonesRoot;
        this.gsonZone = new GsonBuilder()
                .registerTypeAdapter(Zone.class, new ZoneAdapter())
                .disableHtmlEscaping()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
    }

    public void serverStarting(){

        if(root.listFiles().length > 0) {
            for (File file : root.listFiles()) {
                final Reader reader = new Reader(file);

                final String content = reader.read();
                final Zone zone = readZone(content);

                this.registerZone(zone);
            }
        }

    }

    /**
     * Enregistre une zone
     * @param zone
     */
    public void registerZone(Zone zone){
        zoneSet.add(zone);
        System.out.println("Une nouvelle zone a été sauvegardée.");
    }

    /**
     * Sauvegarde une zone
     * @param zone
     */
    public void saveZone(Zone zone) throws IOException {
        String content = gsonZone.toJson(zone);

        File file = new File(root, zone.getName() + ".json");

        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write(content);
        bufferedWriter.flush();

        bufferedWriter.close();
        fileWriter.close();

        System.out.println("Une nouvelle zone à été crée | sauvegardé");
    }

    /**
     * Transforme la lecture d'un fichier json en objet
     * @param content
     * @return zone
     */
    private Zone readZone(String content){
        return gsonZone.fromJson(content, Zone.class);
    }

    /**
     * Update all zones
     * @param world
     * @param partialTicks
     */
    public void update(World world, float partialTicks){

        for(Zone zone : zoneSet){
            zone.tick(world, partialTicks);
        }

    }

}
