package fr.arkofus.server.zone;

import javafx.geometry.Pos;

/**
 * Class créée le 10/12/2018 à 00:15
 * par Jullian Dorian
 */
public class ZonePolygone extends AbstractZone {

    public ZonePolygone(){
        super(200);
    }

    public Position getPointAt(int index){

        if(index < 0 || index > 200){
            throw new IndexOutOfBoundsException();
        }

        if(get(index) != null){
            return get(index);
        }
        return null;
    }

    public Position getFirst(){
        return get(0);
    }

    public Position getLast(){
        int s = size();
        return s != 0 ? get(s-1) : null;
    }

    public int size(){
        int size = 0;

        for(int i = 0; i < getPositions().length; i++){
            if(get(i) == null){
                break;
            }
            size++;
        }

        return size;
    }

}
