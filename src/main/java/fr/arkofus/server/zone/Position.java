package fr.arkofus.server.zone;

/**
 * Class créée le 09/12/2018 à 19:20
 * par Jullian Dorian
 */
public class Position {

    private double x;
    private double y;
    private double z;

    public Position(){}

    public Position(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getZ() {
        return z;
    }
}
