package fr.arkofus.server.zone;

import javafx.geometry.Pos;

/**
 * Class créée le 09/12/2018 à 19:18
 * par Jullian Dorian
 */
public abstract class AbstractZone {

    private Position[] positions;

    public AbstractZone(int size){
        this.positions = new Position[size];
    }

    public void addPosition(Position position){
        this.positions[last()] = position;
    }

    private int last(){
        for(int i = 0; i < positions.length; i++){
            if(positions[i] == null)
                return i;
        }
        return -1;
    }

    public Position get(int index){
        return this.positions[index];
    }

    public Position[] getPositions() {
        return positions;
    }
}
