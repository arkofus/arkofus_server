package fr.arkofus.server.zone;

import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.entities.entity.bouftou.EntityBouftonOrageux;
import javafx.geometry.Pos;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 08/12/2018 à 18:16
 * par Jullian Dorian
 */
public class Zone {

    private String name;
    private World world;
    private Type type;
    private AbstractZone zonePosition;

    private List<Entity> entitySpawned = new ArrayList<>();
    private List<Class<? extends Entity>> entities = new ArrayList<>();

    private int maxGroup = 8;
    private int maxEntities = 24;

    private ThreadLocalRandom random;

    public Zone(){
        this("", null, null);
    }

    public Zone(String name, World world, Type type){
        this.name = name;
        this.world = world;
        this.type = type;
        this.random = ThreadLocalRandom.current();
    }

    public Zone(String name){
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public World getWorld() {
        return world;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setArea(AbstractZone zonePosition) {
        this.zonePosition = zonePosition;
    }

    public AbstractZone getArea(){
        return zonePosition;
    }

    public int getMaxEntities() {
        return maxEntities;
    }

    public void setMaxEntities(int maxEntities) {
        this.maxEntities = maxEntities;
    }

    public int getMaxGroup() {
        return maxGroup;
    }

    public void setMaxGroup(int maxGroup) {
        this.maxGroup = maxGroup;
    }

    public void setEntities(List<Class<? extends Entity>> entities) {
        this.entities = entities;
    }

    public List<Class<? extends Entity>> getEntities() {
        return entities;
    }

    public int countSpawned(){
        return getEntitySpawned().size();
    }

    public boolean isFull(){
        return countSpawned() >= maxEntities;
    }

    /**
     * Add a new entity on spawnable entities on zone
     * @param classEntity
     */
    public void addEntity(Class<? extends Entity> classEntity){
        this.entities.add(classEntity);
    }

    public List<Entity> getEntitySpawned() {
        return entitySpawned;
    }

    /**
     * Prend une entité random en fonction de ceux enregistrés
     * Retourne l'entité choisis
     * @param world
     * @param <T>
     * @return entity
     */
    private <T> T createEntity(World world){

        int i = random.nextInt(0, this.entities.size());

        try {
            T entity = (T) getEntities().get(i).getDeclaredConstructors()[0].newInstance(world);
            if(entity != null){
                return entity;
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    private double[] getSpawn(){

        double[] spawn = new double[3];

        if(getType() == Type.CUBE){

            ZoneCube zoneCube = (ZoneCube) getArea();

            Position first = zoneCube.getFirst();
            Position second = zoneCube.getSecond();

            double highX = compareHigh(first.getX(), second.getX());
            double lowX = compareLow(first.getX(), second.getX());

            double highY = compareHigh(first.getY(), second.getY());
            double lowY = compareLow(first.getY(), second.getY());

            double highZ = compareHigh(first.getZ(), second.getZ());
            double lowZ = compareLow(first.getZ(), second.getZ());

            double rX = random.nextDouble(lowX, highX);
            double rY = 0;
            double rZ = random.nextDouble(lowZ, highZ);

            BlockPos blockPos = new BlockPos(rX, lowY, rZ);
            if(world.getBlockState(blockPos).getBlock() == Blocks.AIR)
                rY = lowY+1;

            spawn[0] = rX;
            spawn[1] = rY;
            spawn[2] = rZ;

        } else {

            ZonePolygone zonePolygone = (ZonePolygone) getArea();
            int size = zonePolygone.size();

            int rI = random.nextInt(0, size);

            Position position = zonePolygone.getPointAt(rI);
            spawn[0] = position.getX();
            spawn[1] = position.getY();
            spawn[2] = position.getZ();
        }

        return spawn;
    }

    /**
     * Permet de faire la gestion de la zone, du spawn etc...
     */
    public void tick(World world, float partialTicks){

        if(world == this.world ){

            if(!isFull() && partialTicks == 20) {

                float canSpawn = random.nextFloat();

                if(canSpawn < 0.10) {

                    int nb_entity = random.nextInt(1, 4); //Entre 1 et 4
                    int t = entitySpawned.size() + nb_entity > maxEntities ? (entitySpawned.size() + nb_entity) - maxEntities : nb_entity; //total restant pouvant spawn

                    double[] pos = getSpawn();

                    System.out.println("Qtt spawn : " + t);

                    while (t != 0) {
                        Entity entity = createEntity(world);
                        if (entity != null) {
                            entity.setPosition(pos[0] + random.nextFloat(), pos[1], pos[2] + random.nextFloat());

                            this.entitySpawned.add(entity);
                            world.spawnEntity(entity);
                        }

                        t--;
                    }
                }
                updateEntity();
                System.out.println("Qtt : " + countSpawned());
            } else {
                if(partialTicks == 20)
                    System.out.println("Full");
            }
        }
    }

    /**
     * Update the entities to set Zone
     */
    private void updateEntity(){
        for(Entity entity : getEntitySpawned()){

            if(entity instanceof ArkofusEntity){
                ((ArkofusEntity) entity).setZone(this);
                entity.onUpdate();
            }

        }
    }

    /**
     * Supprime l'éntité tuée et rafraichit la zone sur les autres
     * @param entity
     */
    public void removeAndUpdate(Entity entity){

        for(int i = 0; i < this.entitySpawned.size(); i++){
            Entity entity1 = entitySpawned.get(i);

            if(entity.getUniqueID() == entity1.getUniqueID()){
                entitySpawned.remove(i);
                break;
            }
        }

        updateEntity();
    }

    private double compareHigh(double x, double x2){
        return x >= x2 ? x : x2;
    }

    private double compareLow(double x, double x2){
        return x < x2 ? x : x2;
    }

    public static enum Type{

        CUBE("cube"),
        POLYGON("polygon");

        private String name;

        Type(String name){
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}
