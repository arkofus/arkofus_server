package fr.arkofus.server;

import java.io.File;

/**
 * Class créée le 09/08/2018 à 19:05
 * par Jullian Dorian
 */
public class ModConfiguration {

    public File arkofusRoot;

    public File classesRoot, playersRoot, zonesRoot;

    public ModConfiguration(File configuration){
        this.arkofusRoot = createDir(configuration, "arkofus");
    }

    public void preInitialization() {
        this.classesRoot = createDir(arkofusRoot, "classes");
        this.playersRoot = createDir(arkofusRoot, "players");
        this.zonesRoot = createDir(arkofusRoot, "zones");
    }

    private File createDir(String path, String name){

        File file = new File(path, name);
        file.mkdirs();
        return file;
    }

    private File createDir(File path, String name){
        return createDir(path.getPath(), name);
    }
}
