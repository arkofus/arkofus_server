package fr.arkofus.server.commands;

import fr.arkofus.ArkofusServer;
import fr.arkofus.entities.entity.bouftou.EntityBouftonOrageux;
import fr.arkofus.server.zone.*;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.server.MinecraftServer;

import java.io.IOException;

/**
 * Class créée le 10/12/2018 à 11:14
 * par Jullian Dorian
 */
public class ZoneCommand extends CommandBase {

    public ZoneCommand(){}

    /**
     * Gets the name of the command
     */
    @Override
    public String getName() {
        return "zone";
    }

    /**
     * Gets the usage string for the command.
     *
     * @param sender The ICommandSender who is requesting usage details
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        String name = args[0];

        if(name.equalsIgnoreCase("spawn")){
            ArkofusServer.getZoneManager().update(server.getEntityWorld(), 0f);
            return;
        }

        String type = args[1];


        Zone zone = null;

        switch (type){
            case "cube":
                zone = new Zone(name, sender.getEntityWorld(), Zone.Type.CUBE);
                ZoneCube zoneCube = new ZoneCube();
                zoneCube.addPosition(new Position(sender.getPosition().getX(), sender.getPosition().getY(), sender.getPosition().getZ()));
                zoneCube.addPosition(new Position(sender.getPosition().getX() - 5, sender.getPosition().getY() + 10, sender.getPosition().getZ() + 10));

                zone.setArea(zoneCube);
                break;
            case "polygon":
                zone = new Zone(name, sender.getEntityWorld(), Zone.Type.POLYGON);
                ZonePolygone zonePolygone = new ZonePolygone();
                zonePolygone.addPosition(new Position(sender.getPosition().getX(), sender.getPosition().getY(), sender.getPosition().getZ()));
                zonePolygone.addPosition(new Position(sender.getPosition().getX() - 5, sender.getPosition().getY() + 10, sender.getPosition().getZ() + 10));
                zonePolygone.addPosition(new Position(sender.getPosition().getX() - 4, sender.getPosition().getY() + 2, sender.getPosition().getZ() + 20));
                zonePolygone.addPosition(new Position(sender.getPosition().getX() - 10, sender.getPosition().getY() + 3, sender.getPosition().getZ() + 30));
                zonePolygone.addPosition(new Position(sender.getPosition().getX() - 9, sender.getPosition().getY() + 5, sender.getPosition().getZ() + 40));

                zone.setArea(zonePolygone);
                break;
        }

        if (zone != null) {
            zone.addEntity(EntityBouftonOrageux.class);
            zone.addEntity(EntitySheep.class);
            System.out.println("Ajout des entités");
        }

        final ZoneManager zoneManager = ArkofusServer.getZoneManager();
        zoneManager.registerZone(zone);
        try {
            zoneManager.saveZone(zone);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
