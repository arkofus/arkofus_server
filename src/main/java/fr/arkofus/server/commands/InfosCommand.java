package fr.arkofus.server.commands;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

/**
 * Class créée le 28/07/2018 à 08:44
 * par Jullian Dorian
 */
public class InfosCommand extends CommandBase {
    /**
     * Gets the name of the command
     */
    @Override
    public String getName() {
        return "infos";
    }

    /**
     * Gets the usage string for the command.
     *
     * @param sender The ICommandSender who is requesting usage details
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "infos";
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        World world = server.getEntityWorld();

        if(!world.isRemote){

            EntityPlayer entityPlayer = getCommandSenderAsPlayer(sender);
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());

            TextComponentString textComponents = new TextComponentString("");
            textComponents.appendText(arkofusPlayer.toString());

            sender.sendMessage(textComponents);

        }

    }
}
