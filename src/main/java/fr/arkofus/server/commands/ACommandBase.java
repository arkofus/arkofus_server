package fr.arkofus.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.Nullable;
import java.util.List;

public abstract class ACommandBase extends CommandBase{

    public ACommandBase(){}

    /**
     * Définie le nom de la commande.
     * Ne pas mettre de slash, point ou autre.
     *
     * {@link CommandBase#getName()}
     */
    @Override
    public abstract String getName();

    /**
     * Définie la permission de la commande, cela n'influe pas
     * si le joueur à le droit de l'utiliser ou non.
     *
     * {@link CommandBase#getRequiredPermissionLevel()}
     *
     * @return int Niveau de permission
     */
    @Override
    public abstract int getRequiredPermissionLevel();

    /**
     * Renvoie l'usage de la commande à utiliser en cas d'érreur.
     *
     * {@link CommandBase#getUsage(ICommandSender)}
     *
     * @param sender - Celui qui effectue la commande
     * @return string - L'usage de la commande
     */
    @Override
    public abstract String getUsage(ICommandSender sender);

    /**
     * Les aliases possibles de la commande.
     *
     * Si il n'y aucune aliase à ajouter, retounez une liste vide.
     *
     * {@link CommandBase#getAliases()}
     */
    @Override
    public abstract List<String> getAliases();

    /**
     * Vérifie si le joueur peut effectuer la commande.
     * Usage : sender.canUseCommand(this.getRequiredPermissionLevel(), this.getName());
     *
     * {@link CommandBase#checkPermission(MinecraftServer, ICommandSender)}
     *
     * @return true Le joueur pourras faire la commande sans la permission. false Il ne pourras rien faire malgré
     * le niveau de permission.
     */
    @Override
    public abstract boolean checkPermission(MinecraftServer server, ICommandSender sender);

    /**
     * Get a list of options for when the user presses the TAB key
     *
     * @param server    The server instance
     * @param sender    The ICommandSender to get tab completions for
     * @param args      Any arguments that were present when TAB was pressed
     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
     *
     * @return Retourner une liste vide si il n'y a pas besoin d'utiliser l'auto-completation
     */
    @Override
    public abstract List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos);

    /**
     * Return whether the specified command parameter index is a username parameter.
     *
     * Retourne si la commande utilise un joueur dans un argument.
     *
     * @param args  The arguments of the command invocation
     * @param index The index
     */
    @Override
    public abstract boolean isUsernameIndex(String[] args, int index);

    /**
     * {@link CommandBase#compareTo(Object)}
     */
    @Override
    public abstract int compareTo(ICommand p_compareTo_1_);

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public abstract void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException;

    public void sendMessage(ICommandSender sender, String text){
        sender.sendMessage(new TextComponentString(text));
    }
}
