package fr.arkofus.server.commands;

import fr.arkofus.entities.entity.EntityPNJ;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 02/12/2018 à 12:32
 * par Jullian Dorian
 */
public class PnjCommand extends ACommandBase {

    public PnjCommand(){}

    /**
     * Définie le nom de la commande.
     * Ne pas mettre de slash, point ou autre.
     * <p>
     * {@link CommandBase#getName()}
     */
    @Override
    public String getName() {
        return "pnj";
    }

    /**
     * Définie la permission de la commande, cela n'influe pas
     * si le joueur à le droit de l'utiliser ou non.
     * <p>
     * {@link CommandBase#getRequiredPermissionLevel()}
     *
     * @return int Niveau de permission
     */
    @Override
    public int getRequiredPermissionLevel() {
        return 4;
    }

    /**
     * Renvoie l'usage de la commande à utiliser en cas d'érreur.
     * <p>
     * {@link CommandBase#getUsage(ICommandSender)}
     *
     * @param sender - Celui qui effectue la commande
     * @return string - L'usage de la commande
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "/pnj <name>";
    }

    /**
     * Les aliases possibles de la commande.
     * <p>
     * Si il n'y aucune aliase à ajouter, retounez une liste vide.
     * <p>
     * {@link CommandBase#getAliases()}
     */
    @Override
    public List<String> getAliases() {
        return Arrays.asList();
    }

    /**
     * Vérifie si le joueur peut effectuer la commande.
     * Usage : sender.canUseCommand(this.getRequiredPermissionLevel(), this.getName());
     * <p>
     * {@link CommandBase#checkPermission(MinecraftServer, ICommandSender)}
     *
     * @param server
     * @param sender
     * @return true Le joueur pourras faire la commande sans la permission. false Il ne pourras rien faire malgré
     * le niveau de permission.
     */
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    /**
     * Get a list of options for when the user presses the TAB key
     *
     * @param server    The server instance
     * @param sender    The ICommandSender to get tab completions for
     * @param args      Any arguments that were present when TAB was pressed
     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
     * @return Retourner une liste vide si il n'y a pas besoin d'utiliser l'auto-completation
     */
    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return Arrays.asList();
    }

    /**
     * Return whether the specified command parameter index is a username parameter.
     * <p>
     * Retourne si la commande utilise un joueur dans un argument.
     *
     * @param args  The arguments of the command invocation
     * @param index The index
     */
    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    /**
     * {@link CommandBase#compareTo(Object)}
     *
     * @param p_compareTo_1_
     */
    @Override
    public int compareTo(ICommand p_compareTo_1_) {
        return 0;
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        if(args.length > 0 && sender instanceof EntityPlayer){
            String name = parseName(args);

            EntityPNJ pnj = new EntityPNJ(sender.getEntityWorld(), name, null);
            pnj.setLocationAndAngles(sender.getPosition().getX(), sender.getPosition().getY(),sender.getPosition().getZ(),
                    ((EntityPlayer) sender).cameraYaw, ((EntityPlayer) sender).cameraPitch);

            System.out.println("pnk name: " + name);
            server.getEntityWorld().spawnEntity(pnj);
        }

    }

    private String parseName(String[] args){
        StringBuilder builder = new StringBuilder();
        for(String s : args){
            builder.append(s);
            builder.append(" ");
        }
        builder.delete(builder.length()-1, builder.length());
        return builder.toString();
    }
}
