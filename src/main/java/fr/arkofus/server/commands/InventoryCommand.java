package fr.arkofus.server.commands;

import fr.arkofus.ArkofusServer;
import fr.arkofus.packet.client.CPacketAddItemInventory;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class créée le 19/08/2018 à 10:57
 * par Jullian Dorian
 */
public class InventoryCommand extends ACommandBase {
    /**
     * Définie le nom de la commande.
     * Ne pas mettre de slash, point ou autre.
     * <p>
     * {@link CommandBase#getName()}
     */
    @Override
    public String getName() {
        return "inventory";
    }

    /**
     * Définie la permission de la commande, cela n'influe pas
     * si le joueur à le droit de l'utiliser ou non.
     * <p>
     * {@link CommandBase#getRequiredPermissionLevel()}
     *
     * @return int Niveau de permission
     */
    @Override
    public int getRequiredPermissionLevel() {
        return 4;
    }

    /**
     * Renvoie l'usage de la commande à utiliser en cas d'érreur.
     * <p>
     * {@link CommandBase#getUsage(ICommandSender)}
     *
     * @param sender - Celui qui effectue la commande
     * @return string - L'usage de la commande
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "inventory [add|remove|see] <player> <item> <qtt>";
    }

    /**
     * Les aliases possibles de la commande.
     * <p>
     * Si il n'y aucune aliase à ajouter, retounez une liste vide.
     * <p>
     * {@link CommandBase#getAliases()}
     */
    @Override
    public List<String> getAliases() {
        return Arrays.asList("inv");
    }

    /**
     * Vérifie si le joueur peut effectuer la commande.
     * Usage : sender.canUseCommand(this.getRequiredPermissionLevel(), this.getName());
     * <p>
     * {@link CommandBase#checkPermission(MinecraftServer, ICommandSender)}
     *
     * @param server
     * @param sender
     * @return true Le joueur pourras faire la commande sans la permission. false Il ne pourras rien faire malgré
     * le niveau de permission.
     */
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    /**
     * Get a list of options for when the user presses the TAB key
     *
     * @param server    The server instance
     * @param sender    The ICommandSender to get tab completions for
     * @param args      Any arguments that were present when TAB was pressed
     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
     * @return Retourner une liste vide si il n'y a pas besoin d'utiliser l'auto-completation
     */
    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return args.length == 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : (args.length == 3 ? getListOfStringsMatchingLastWord(args, Item.REGISTRY.getKeys()) : Collections.<String>emptyList());
    }

    /**
     * Return whether the specified command parameter index is a username parameter.
     * <p>
     * Retourne si la commande utilise un joueur dans un argument.
     *
     * @param args  The arguments of the command invocation
     * @param index The index
     */
    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return true;
    }

    /**
     * {@link CommandBase#compareTo(Object)}
     *
     * @param p_compareTo_1_
     */
    @Override
    public int compareTo(ICommand p_compareTo_1_) {
        return 0;
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        if(args.length <= 1){
            sender.sendMessage(new TextComponentString("Il manque des arguments."));
            return;
        }

        String namePlayer = args[1];
        EntityPlayer player = getCommandSenderAsPlayer(sender);

        EntityPlayer target = getPlayer(server, sender, namePlayer);
        ArkofusPlayer arkofusTarget = ArkofusServer.getMapPlayers().get(target.getUniqueID());

        if(args.length == 2 && args[0].equalsIgnoreCase("see")){
            //See player
        } else {

            if(args.length < 3){
                player.sendMessage(new TextComponentString("Il manque des arguments."));
                return;
            }

            Item item = getItemByText(sender, args[2]);
            int q = args.length > 3 ? parseInt(args[3], 1, item.getItemStackLimit()) : 1;
            ItemStack itemStack = new ItemStack(item, q);

            if(args[0].equalsIgnoreCase("add")){

                //Si on peux ajouter l'item à l'inventaire
                boolean flag = arkofusTarget.getInventory().addItemStackToInventory(itemStack);

                //Si vrai
                if(flag || true){
                    //On rafraichit les NBT
                    arkofusTarget.getInventory().refreshNBT();

                    arkofusTarget.save();
                    target.sendMessage(new TextComponentString("Item gived : " + item.getUnlocalizedName()));

                    //Envoie du packet au client pour un ajout d'item
                    RegistryPacket.getNetwork().sendTo(new CPacketAddItemInventory(args[2], q), (EntityPlayerMP) target);
                } else {
                    target.world.playSound((EntityPlayer)target, target.posX, target.posY, target.posZ, SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.PLAYERS, 0.2F, ((target.getRNG().nextFloat() - target.getRNG().nextFloat()) * 0.7F + 1.0F) * 2.0F);
                    EntityItem entityitem = target.dropItem(itemStack, false);

                    if (entityitem != null)
                    {
                        entityitem.setNoPickupDelay();
                        entityitem.setOwner(target.getName());
                    }
                }

            } else if(args[0].equalsIgnoreCase("remove")){

            } else {
                player.sendMessage(new TextComponentString("Impossible de faire cette action."));
            }

        }

    }
}
