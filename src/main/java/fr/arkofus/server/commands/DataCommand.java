package fr.arkofus.server.commands;

import fr.arkofus.ArkofusServer;
import fr.arkofus.characteristic.ElementType;
import fr.arkofus.entities.entity.ArkofusEntity;
import fr.arkofus.player.ArkofusPlayer;
import net.minecraft.command.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class DataCommand extends ACommandBase{

    /**
     * Définie le nom de la commande.
     * Ne pas mettre de slash, point ou autre.
     * <p>
     * {@link CommandBase#getName()}
     */
    @Override
    public String getName() {
        return "data";
    }

    /**
     * Définie la permission de la commande, cela n'influe pas
     * si le joueur à le droit de l'utiliser ou non.
     * <p>
     * {@link CommandBase#getRequiredPermissionLevel()}
     *
     * @return int Niveau de permission
     */
    @Override
    public int getRequiredPermissionLevel() {
        return 4;
    }

    /**
     * Renvoie l'usage de la commande à utiliser en cas d'érreur.
     * <p>
     * {@link CommandBase#getUsage(ICommandSender)}
     *
     * @param sender - Celui qui effectue la commande
     * @return string - L'usage de la commande
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "/test [args...]";
    }

    /**
     * Les aliases possibles de la commande.
     * <p>
     * Si il n'y aucune aliase à ajouter, retounez une liste vide.
     * <p>
     * {@link CommandBase#getAliases()}
     */
    @Override
    public List<String> getAliases() {
        return new ArrayList<>();
    }

    /**
     * Vérifie si le joueur peut effectuer la commande.
     * Usage : sender.canUseCommand(this.getRequiredPermissionLevel(), this.getName());
     * <p>
     * {@link CommandBase#checkPermission(MinecraftServer, ICommandSender)}
     *
     * @param server
     * @param sender
     * @return true Le joueur pourras faire la commande sans la permission. false Il ne pourras rien faire malgré
     * le niveau de permission.
     */
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return sender.canUseCommand(this.getRequiredPermissionLevel(), this.getName());
    }

    /**
     * Get a list of options for when the user presses the TAB key
     *
     * @param server    The server instance
     * @param sender    The ICommandSender to get tab completions for
     * @param args      Any arguments that were present when TAB was pressed
     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
     * @return Retourner une liste vide si il n'y a pas besoin d'utiliser l'auto-completation
     */
    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        ArrayList<String> entities = new ArrayList<>();
        ArkofusServer.arkofusEntities.iterator().forEachRemaining(entity -> entities.add(entity.getName()));
        return args.length == 2 ? getListOfStringsMatchingLastWord(args, entities) : new ArrayList<>();
    }

    /**
     * Return whether the specified command parameter index is a username parameter.
     * <p>
     * Retourne si la commande utilise un joueur dans un argument.
     *
     * @param args  The arguments of the command invocation
     * @param index The index
     */
    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    /**
     * {@link CommandBase#compareTo(Object)}
     *
     * @param p_compareTo_1_
     */
    @Override
    public int compareTo(ICommand p_compareTo_1_) {
        return 0;
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        World world = sender.getEntityWorld();

        if(args.length <= 0){
            throw new WrongUsageException(this.getUsage(sender), new Object[0]);
        } else {
            switch (args[0]){
                case "side":
                    if(world.isRemote){
                        sender.sendMessage(new TextComponentString("Side côté client"));
                    } else {
                        sender.sendMessage(new TextComponentString("Side côté serveur"));
                    }
                    break;
                case "entity":
                    Entity entity = null;
                    if(args.length == 2){
                        if(args[1].equalsIgnoreCase("me")){
                            EntityPlayer entityPlayer = getCommandSenderAsPlayer(sender);
                            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());

                            TextComponentString textComponents = new TextComponentString(TextFormatting.RED + "Informations sur le joueur: " +
                                    entity.getName());
                            textComponents.appendText("\n");
                            textComponents.appendText(TextFormatting.YELLOW + "Niveau : " + arkofusPlayer.getLevel()).appendText("\n");
                            textComponents.appendText(TextFormatting.AQUA + "Experience : " + arkofusPlayer.getExperience()).appendText("\n");
                            textComponents.appendText(TextFormatting.AQUA + "Experience pour Up : " + arkofusPlayer.getNeedExperience())
                                    .appendText("\n");
                            textComponents.appendText(TextFormatting.DARK_AQUA + "Total Experience : " + arkofusPlayer.getTotalExperience())
                                    .appendText("\n");

                            sender.sendMessage(textComponents);


                        } else {
                            entity = getEntity(server, sender, args[1]);
                            if (entity instanceof ArkofusEntity) {
                                ArkofusEntity entityArkofus = (ArkofusEntity) entity;

                                TextComponentString textComponents = new TextComponentString(TextFormatting.RED + "Informations sur l'entité : " +
                                        entityArkofus.getName());
                                textComponents.appendText("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Niveau : " + entityArkofus.getLevel()).appendText("\n");
                                textComponents.appendText("\n");
                                textComponents.appendText("Vitalité : " + ((ArkofusEntity) entity).getVitalite()).appendText("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Res Neutre : " + entityArkofus.getFinalResistance(ElementType.NEUTRE)).appendText
                                        ("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Res Terre : " + entityArkofus.getFinalResistance(ElementType.TERRE)).appendText
                                        ("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Res Feu : " + entityArkofus.getFinalResistance(ElementType.FEU)).appendText
                                        ("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Res Eau : " + entityArkofus.getFinalResistance(ElementType.EAU)).appendText
                                        ("\n");
                                textComponents.appendText(TextFormatting.YELLOW + "Res Air : " + entityArkofus.getFinalResistance(ElementType.AIR)).appendText
                                        ("\n");

                                sender.sendMessage(textComponents);
                            }
                        }
                    }
                    break;
                case "entities":
                    ArkofusServer.arkofusEntities.clear();

                    TextComponentString entitiesFounds = new TextComponentString("Entities founds : ");

                    int count = args.length >= 2 ? Integer.valueOf(args[1]) : 100;

                    for(Entity e : getEntityList(server, sender, "@e[r=10,c=" + count + "]")){
                        TextComponentString entityFound;
                        if(e instanceof ArkofusEntity){
                            ArkofusServer.arkofusEntities.add((ArkofusEntity) e);
                            entityFound = new TextComponentString(e.getCustomNameTag() + TextFormatting.RESET.toString());
                            entityFound.getStyle().setClickEvent(
                                    new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/data entity " + e.getCachedUniqueIdString())
                            );
                            entityFound.getStyle().setHoverEvent(
                                    new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new TextComponentString("DisplayName : " + e.getDisplayName().getUnformattedText())
                                                    .appendText("\n")
                                                    .appendText("Name : " + e.getName())
                                                    .appendText("\n")
                                                    .appendText("UUID : " + e.getCachedUniqueIdString())
                                                    .appendText("\n")
                                                    .appendText("EntityID : " + e.getEntityId())
                                                    .appendText("\n")
                                                    .appendText("hasCustomName : " + e.hasCustomName())
                                                    .appendText("\n")
                                                    .appendText("Level : " + ((ArkofusEntity) e).getLevel())
                                                    .appendText("\n")
                                                    .appendText("Clique pour avoir plus d'informations.")
                                    )
                            );
                        } else {
                            entityFound = new TextComponentString(e.getName() + TextFormatting.RESET.toString());
                            entityFound.getStyle().setClickEvent(
                                    new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/data entity " + e.getCachedUniqueIdString())
                            );
                            entityFound.getStyle().setHoverEvent(
                                    new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new TextComponentString("DisplayName : " + e.getDisplayName().getFormattedText())
                                                    .appendText("\n")
                                                    .appendText("UUID : " + e.getCachedUniqueIdString())
                                                    .appendText("\n")
                                                    .appendText("EntityID : " + e.getEntityId())
                                                    .appendText("\n")
                                                    .appendText("hasCustomName : " + e.hasCustomName())
                                                    .appendText("\n")
                                                    .appendText("CustomName : " + e.getCustomNameTag())
                                                    .appendText("\n")
                                                    .appendText("Clique pour avoir plus d'informations.")
                                    )
                            );
                        }


                        entitiesFounds.appendSibling(entityFound);
                        entitiesFounds.appendText(", ");
                    }
                    sender.sendMessage(entitiesFounds);
                    break;
                default:
                    throw new WrongUsageException("Args: [side, entity]", new Object[0]);
            }
        }
    }
}
