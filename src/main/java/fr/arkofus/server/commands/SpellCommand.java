package fr.arkofus.server.commands;

import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.IClasse;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

/**
 * Class créée le 22/07/2018 à 21:22
 * par Jullian Dorian
 */
public class SpellCommand extends CommandBase {
    /**
     * Gets the name of the command
     */
    @Override
    public String getName() {
        return "spell";
    }

    /**
     * Gets the usage string for the command.
     *
     * @param sender The ICommandSender who is requesting usage details
     */
    @Override
    public String getUsage(ICommandSender sender) {
        return "spell <name>";
    }

    /**
     * Callback for when the command is executed
     *
     * @param server The server instance
     * @param sender The sender who executed the command
     * @param args   The arguments that were passed
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        World world = server.getEntityWorld();

        EntityPlayer player = getCommandSenderAsPlayer(sender);

        IClasse classe = ArkofusServer.getMapPlayers().get(player.getUniqueID()).getClasse();

    }
}
