package fr.arkofus.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

/**
 * Class créée le 10/12/2018 à 11:25
 * par Jullian Dorian
 */
public class CommandRegister {

    private FMLServerStartingEvent event;

    public CommandRegister(FMLServerStartingEvent event){
        this.event = event;
    }

    public void registerCommands(){
        register(new SpellCommand());
        register(new InfosCommand());
        register(new DataCommand());
        register(new InventoryCommand());
        register(new PnjCommand());
        register(new ZoneCommand());
    }

    private void register(CommandBase commandBase){
        this.event.registerServerCommand(commandBase);
    }

}
