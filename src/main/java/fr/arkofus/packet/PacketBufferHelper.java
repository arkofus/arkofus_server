package fr.arkofus.packet;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

/**
 * Class créée le 08/11/2018 à 10:05
 * par Jullian Dorian
 */
public class PacketBufferHelper {

    /**
     * Write an ItemStack on the ByteBuffer
     * @param byteBuf - the byte buffer
     * @param itemStack - the itemstack
     * @return bytebuf
     */
    public static ByteBuf writeItemStack(ByteBuf byteBuf, ItemStack itemStack){
        if (itemStack.isEmpty()) {
            byteBuf.writeShort(-1);
        } else {
            byteBuf.writeShort(Item.getIdFromItem(itemStack.getItem()));
            byteBuf.writeShort(itemStack.getCount());
            byteBuf.writeShort(itemStack.getMetadata());

        }

        return byteBuf;
    }

    /**
     * Read an itemstack from the ByteBuffer
     * @param byteBuf - the bytebuffer
     * @return itemstack if the count is greater than 0 else is Empty
     */
    public static ItemStack readItemStack(ByteBuf byteBuf){
        int i = byteBuf.readShort();

        if (i < 0) {
            return ItemStack.EMPTY;
        } else {
            int j = byteBuf.readShort();
            int k = byteBuf.readShort();
            ItemStack itemstack = new ItemStack(Item.getItemById(i), j, k);
            return itemstack;
        }
    }

    /**
     * Read a enum value from the byte buffer from a enum class
     * @param byteBuf
     * @param enumClass
     * @param <T>
     * @return enum
     */
    public static <T extends Enum<T>> T readEnumValue(ByteBuf byteBuf, Class<T> enumClass) {
        return (T)((Enum[])enumClass.getEnumConstants())[byteBuf.readInt()];
    }

    /**
     * Write a enum in the buffer
     * @param byteBuf
     * @param value
     * @return bytebuf
     */
    public static ByteBuf writeEnumValue(ByteBuf byteBuf, Enum<?> value) {
        return byteBuf.writeInt(value.ordinal());
    }

}
