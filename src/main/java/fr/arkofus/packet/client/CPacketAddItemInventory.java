package fr.arkofus.packet.client;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 05/11/2018 à 11:31
 * par Jullian Dorian
 */
public class CPacketAddItemInventory implements IMessage {

    public String itemName;
    public int quantity;

    public CPacketAddItemInventory(){}

    /**
     * Send a packet to add an item on the side client
     * @param itemName string
     * @param qtt int
     */
    public CPacketAddItemInventory(String itemName, int qtt){
        this.itemName = itemName;
        this.quantity = qtt;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.itemName = ByteBufUtils.readUTF8String(buf);
        this.quantity = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.itemName);
        buf.writeInt(this.quantity);
    }

    public static class Handler implements IMessageHandler<CPacketAddItemInventory, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketAddItemInventory message, MessageContext ctx) {
            return null;
        }
    }
}
