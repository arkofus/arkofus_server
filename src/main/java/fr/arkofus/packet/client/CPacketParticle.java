package fr.arkofus.packet.client;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.EnumParticleTypes;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 24/07/2018 à 14:18
 * par Jullian Dorian
 */
public class CPacketParticle implements IMessage {

    private EnumParticleTypes enumParticleType;
    private double posX, posY, posZ;
    private double xSpeed, ySpeed, zSpeed;
    private int[] params;

    public CPacketParticle(){

    }

    public CPacketParticle(EnumParticleTypes enumParticleType, double posX, double posY, double posZ, double p0, double p1, double p2, int... params){
        this.enumParticleType = enumParticleType;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.xSpeed = p0;
        this.ySpeed = p1;
        this.zSpeed = p2;
        this.params = params;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        enumParticleType = EnumParticleTypes.getParticleFromId(buf.readInt());
        posX = buf.readDouble();
        posY = buf.readDouble();
        posZ = buf.readDouble();

        xSpeed = buf.readDouble();
        ySpeed = buf.readDouble();
        zSpeed = buf.readDouble();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(enumParticleType.getParticleID());
        buf.writeDouble(posX);
        buf.writeDouble(posY);
        buf.writeDouble(posZ);
        buf.writeDouble(xSpeed);
        buf.writeDouble(ySpeed);
        buf.writeDouble(zSpeed);
    }

    public static class Handler implements IMessageHandler<CPacketParticle, IMessage> {

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketParticle message, MessageContext ctx) {
            return null;
        }
    }
}
