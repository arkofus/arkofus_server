package fr.arkofus.packet.client;

import fr.arkofus.packet.PacketBufferHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Class créée le 08/11/2018 à 10:16
 * par Jullian Dorian
 */
public class CPacketSetSlot implements IMessage {

    public int windowId;
    public int slotId;
    public ItemStack itemStack;

    public CPacketSetSlot(){}

    public CPacketSetSlot(int windowId, int slotId, ItemStack itemIn){
        this.windowId = windowId;
        this.slotId = slotId;
        this.itemStack = itemIn.isEmpty() ? ItemStack.EMPTY : itemIn.copy();
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.windowId = buf.readUnsignedByte();
        this.slotId = buf.readShort();
        this.itemStack = PacketBufferHelper.readItemStack(buf);
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(this.windowId);
        buf.writeShort(this.slotId);
        PacketBufferHelper.writeItemStack(buf, this.itemStack);
    }

    public static class Handler implements IMessageHandler<CPacketSetSlot, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketSetSlot message, MessageContext ctx) {

            if(ctx.side == Side.CLIENT){

                Minecraft minecraft = Minecraft.getMinecraft();

                minecraft.addScheduledTask(() -> {
                    //ArkofusClient.arkofusPlayer.getInventory().addItemSlotContents(message.slotId, message.itemStack);
                });

            }

            return null;
        }
    }
}
