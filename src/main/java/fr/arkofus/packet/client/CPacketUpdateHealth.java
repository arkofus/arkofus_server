package fr.arkofus.packet.client;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 28/10/2018 à 10:22
 * par Jullian Dorian
 */
public class CPacketUpdateHealth implements IMessage {

    public double health;

    public CPacketUpdateHealth(){}

    public CPacketUpdateHealth(double health){
        this.health = health;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.health = buf.readDouble();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeDouble(this.health);
    }

    public static class Handler implements IMessageHandler<CPacketUpdateHealth, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketUpdateHealth message, MessageContext ctx) {
            return null;
        }
    }
}
