package fr.arkofus.packet.client;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 28/07/2018 à 10:04
 * par Jullian Dorian
 */
public class CCreateCharacterPacket implements IMessage {

    public boolean canCreate;

    public CCreateCharacterPacket(){}

    public CCreateCharacterPacket(boolean canCreate){
        this.canCreate = canCreate;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(canCreate);
    }

    public static class Handler implements IMessageHandler<CCreateCharacterPacket, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CCreateCharacterPacket message, MessageContext ctx) {
            return null;
        }
    }
}
