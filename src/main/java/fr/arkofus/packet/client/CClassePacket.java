package fr.arkofus.packet.client;

import fr.arkofus.classe.Classe;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 13/08/2018 à 14:11
 * par Jullian Dorian
 */
public class CClassePacket implements IMessage {

    private Classe classe;
    private String encoded_classe;

    public CClassePacket(){}

    public CClassePacket(Classe classe){
        this.classe = classe;
        this.encoded_classe = classe.toJson();
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        encoded_classe = ByteBufUtils.readUTF8String(buf);
        classe = Classe.fromJson(encoded_classe);
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, encoded_classe);
    }

    public static class Handler implements IMessageHandler<CClassePacket, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CClassePacket message, MessageContext ctx) {
            //Sended on client
            return null;
        }
    }
}
