package fr.arkofus.packet.client;

import fr.arkofus.packet.PacketBufferHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 08/11/2018 à 09:58
 * par Jullian Dorian
 */
public class CPacketWindowItems implements IMessage {

    public int windowId;
    public NonNullList<ItemStack> itemStacks;

    public CPacketWindowItems(){}

    public CPacketWindowItems(int id, NonNullList<ItemStack> nonNullList){
        this.windowId = id;
        this.itemStacks = nonNullList;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.windowId = buf.readUnsignedByte();
        int size = buf.readShort();
        this.itemStacks = NonNullList.withSize(size, ItemStack.EMPTY);

        for(int i = 0; i < size; i++){
            this.itemStacks.set(i, PacketBufferHelper.readItemStack(buf));
        }
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(this.windowId);
        buf.writeShort(this.itemStacks.size());
        for(ItemStack itemStack : this.itemStacks){
            PacketBufferHelper.writeItemStack(buf, itemStack);
        }
    }

    public static class Handler implements IMessageHandler<CPacketWindowItems, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketWindowItems message, MessageContext ctx) {
            return null;
        }
    }
}
