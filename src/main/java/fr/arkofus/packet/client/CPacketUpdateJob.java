package fr.arkofus.packet.client;

import fr.arkofus.player.jobs.IJob;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 30/10/2018 à 12:23
 * par Jullian Dorian
 */
public class CPacketUpdateJob implements IMessage {

    protected String name;
    protected int level;
    protected long experience;
    protected long totalExperience;

    public CPacketUpdateJob(){}

    public CPacketUpdateJob(IJob job){
        this.name = job.getName();
        this.level = job.getLevel();
        this.experience = job.getExperience();
        this.totalExperience = job.getTotalExperience();

    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.name = ByteBufUtils.readUTF8String(buf);
        this.level = buf.readInt();
        this.experience = buf.readLong();
        this.totalExperience = buf.readLong();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.name);
        buf.writeInt(this.level);
        buf.writeLong(this.experience);
        buf.writeLong(this.totalExperience);
    }

    public static class Handler implements IMessageHandler<CPacketUpdateJob, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketUpdateJob message, MessageContext ctx) {
            return null;
        }
    }
}
