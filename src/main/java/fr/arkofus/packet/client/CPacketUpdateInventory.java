package fr.arkofus.packet.client;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 30/10/2018 à 16:02
 * par Jullian Dorian
 */
public class CPacketUpdateInventory implements IMessage {

    private String compound;

    public CPacketUpdateInventory(){}

    public CPacketUpdateInventory(String compound){
       this.compound = compound;
    }

    public CPacketUpdateInventory(NBTTagCompound compound){
        this(compound.toString());
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.compound = ByteBufUtils.readUTF8String(buf);
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.compound);
    }

    public static class Handler implements IMessageHandler<CPacketUpdateInventory, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketUpdateInventory message, MessageContext ctx) {
            return null;
        }
    }
}
