package fr.arkofus.packet.client;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Map;

/**
 * Class créée le 27/07/2018 à 13:30
 * par Jullian Dorian
 */
public class CPacketSynchronise implements IMessage{

    /**
     * <p>Le player convertit en JSON</p>
     */
    public String json;

    /**
     * <p>Si le packet doit en renvoyer un</p>
     */
    public boolean returnValue;

    public CPacketSynchronise(){}

    public CPacketSynchronise(String json){
        this(json, false);
    }

    public CPacketSynchronise(String json, boolean returnValue){
        this.json = json;
        this.returnValue = returnValue;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.json = ByteBufUtils.readUTF8String(buf);
        this.returnValue = buf.readBoolean();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, json);
        buf.writeBoolean(returnValue);
    }

    public static class Handler implements IMessageHandler<CPacketSynchronise, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(CPacketSynchronise message, MessageContext ctx) {

            if(ctx.side == Side.SERVER){

                System.out.println("Json received from client : " + message.json);

                EntityPlayer entityPlayer = ctx.getServerHandler().player;

                String json = message.json;

                ArkofusPlayer arkofusPlayer = ArkofusPlayer.fromJson(json);
                arkofusPlayer.setEntityPlayer(entityPlayer);

                //On définit la nouvelle instance du joueur
                Map<?,?> map = ArkofusServer.getMapPlayers();

                if(map.containsKey(entityPlayer.getUniqueID())){
                    ArkofusServer.getMapPlayers().replace(entityPlayer.getUniqueID(), arkofusPlayer);
                } else {
                    ArkofusServer.getMapPlayers().put(entityPlayer.getUniqueID(), arkofusPlayer);
                }

                arkofusPlayer.save();

                if(message.returnValue){
                    return new CPacketSynchronise(arkofusPlayer.toJson());
                }

            }

            return null;
        }
    }
}
