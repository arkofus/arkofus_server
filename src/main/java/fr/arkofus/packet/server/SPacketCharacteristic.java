package fr.arkofus.packet.server;

import fr.arkofus.ArkofusServer;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.packet.client.CPacketCharacteristic;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 08/10/2018 à 13:20
 * par Jullian Dorian
 */
public class SPacketCharacteristic implements IMessage {

    public SPacketCharacteristic(){}

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {

    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {

    }

    public static class Handler implements IMessageHandler<SPacketCharacteristic, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(SPacketCharacteristic message, MessageContext ctx) {

            EntityPlayerMP entityPlayer = ctx.getServerHandler().player;
            final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());

            if(arkofusPlayer == null)
                return null;

            //Renvoyer un packet
            entityPlayer.getServerWorld().addScheduledTask(() -> {
                //Packet pour synchroniser le joueur client
                arkofusPlayer.synchronise();
                //Envoie du packet pour ouvrir le gui
                RegistryPacket.getNetwork().sendTo(new CPacketCharacteristic(), entityPlayer);
            });

            return null;
        }
    }
}
