package fr.arkofus.packet.server;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 10/11/2018 à 17:19
 * par Jullian Dorian
 */
public class SPacketConfirmTransaction implements IMessage {

    protected int windowId;
    protected short transactionId;
    protected boolean wasAccepted;

    public SPacketConfirmTransaction(){}

    public SPacketConfirmTransaction(int windowId, short transactionId, boolean wasAccepted) {
        this.wasAccepted = wasAccepted;
        this.windowId = windowId;
        this.transactionId = transactionId;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.windowId = buf.readUnsignedByte();
        this.transactionId = buf.readShort();
        this.wasAccepted = buf.readBoolean();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(this.windowId);
        buf.writeShort(this.transactionId);
        buf.writeBoolean(this.wasAccepted);
    }

    public int getWindowId() {
        return windowId;
    }

    public short getTransactionId() {
        return transactionId;
    }

    public boolean wasAccepted() {
        return wasAccepted;
    }

    public static class Handler implements IMessageHandler<SPacketConfirmTransaction, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(SPacketConfirmTransaction message, MessageContext ctx) {

            /** ne sert à rien mais on laisse au cas ou
            final EntityPlayer player = ctx.getServerHandler().player;
            final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(player.getUniqueID());

            Short oshort = (Short) arkofusPlayer.getPendingTransactions().lookup(player.openContainer.windowId);

            if (oshort != null && packetIn.getUid() == oshort.shortValue() && player.openContainer.windowId == packetIn.getWindowId() && !this.player.openContainer.getCanCraft(this.player) && !this.player.isSpectator())
            {
                player.openContainer.setCanCraft(this.player, true);
            }
            */

            return null;
        }
    }
}
