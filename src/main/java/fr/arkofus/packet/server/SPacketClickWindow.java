package fr.arkofus.packet.server;

import fr.arkofus.ArkofusServer;
import fr.arkofus.packet.client.CPacketConfirmTransaction;
import fr.arkofus.packet.PacketBufferHelper;
import fr.arkofus.packet.RegistryPacket;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 10/11/2018 à 15:20
 * par Jullian Dorian
 */
public class SPacketClickWindow implements IMessage {

    protected int windowId, slotId, mouseButton;
    protected ClickType clickType;
    protected ItemStack clickedItem;
    protected short transactionId;

    public SPacketClickWindow(){}

    public SPacketClickWindow(int windowId, int slotId, int mouseButton, ClickType type, ItemStack clickedItem, short transactionId) {
        this.windowId = windowId;
        this.slotId = slotId;
        this.mouseButton = mouseButton;
        this.clickType = type;
        this.clickedItem = clickedItem.isEmpty() ? ItemStack.EMPTY : clickedItem.copy();
        this.transactionId = transactionId;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.windowId = buf.readByte();
        this.slotId = buf.readShort();
        this.mouseButton = buf.readByte();
        this.transactionId = buf.readShort();
        this.clickType = (ClickType) PacketBufferHelper.readEnumValue(buf, ClickType.class);
        this.clickedItem = PacketBufferHelper.readItemStack(buf);
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(this.windowId);
        buf.writeShort(this.slotId);
        buf.writeByte(this.mouseButton);
        buf.writeShort(this.transactionId);
        PacketBufferHelper.writeEnumValue(buf, this.clickType);
        PacketBufferHelper.writeItemStack(buf, this.clickedItem);
    }

    public ClickType getClickType() {
        return clickType;
    }

    public short getTransactionId() {
        return transactionId;
    }

    public int getWindowId() {
        return windowId;
    }

    public ItemStack getClickedItem() {
        return clickedItem;
    }

    public int getMouseButton() {
        return mouseButton;
    }

    public int getSlotId() {
        return slotId;
    }

    public static class Handler implements IMessageHandler<SPacketClickWindow, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(SPacketClickWindow message, MessageContext ctx) {

            EntityPlayerMP player = ctx.getServerHandler().player;
            
            player.getServerWorld().addScheduledTask(() -> {

                player.markPlayerActive();
                final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(player.getUniqueID());

                if (player.openContainer.windowId == message.getWindowId() && player.openContainer.getCanCraft(player)) {
                    if (player.isSpectator()) {
                        NonNullList<ItemStack> nonnulllist = NonNullList.<ItemStack>create();

                        for (int i = 0; i < player.openContainer.inventorySlots.size(); ++i) {
                            nonnulllist.add(((Slot)player.openContainer.inventorySlots.get(i)).getStack());
                        }

                        player.sendAllContents(player.openContainer, nonnulllist);
                    } else {
                        ItemStack itemstack2 = player.openContainer.slotClick(message.getSlotId(), message.getMouseButton(), message.getClickType(), player);

                        if (ItemStack.areItemStacksEqual(message.getClickedItem(), itemstack2)) {
                            //player.connection.sendPacket(new SPacketConfirmTransaction(message.getWindowId(), message.getTransactionId(), true));
                            RegistryPacket.getNetwork().sendTo(new CPacketConfirmTransaction(message.getWindowId(), message.getTransactionId(), true), player);
                            player.isChangingQuantityOnly = true;
                            player.openContainer.detectAndSendChanges();
                            player.updateHeldItem();
                            player.isChangingQuantityOnly = false;
                        } else {
                            //pendingTransactions.addKey(player.openContainer.windowId, Short.valueOf(message.getTransactionId()));
                            //player.connection.sendPacket(new SPacketConfirmTransaction(message.getWindowId(), message.getTransactionId(), false));
                            //arkofusPlayer.addKey(player.openContainer.windowId, Short.valueOf(message.getTransactionId()));
                            RegistryPacket.getNetwork().sendTo(new CPacketConfirmTransaction(message.getWindowId(), message.getTransactionId(), false), player);
                            player.openContainer.setCanCraft(player, false);
                            NonNullList<ItemStack> nonnulllist1 = NonNullList.<ItemStack>create();

                            for (int j = 0; j < player.openContainer.inventorySlots.size(); ++j) {
                                ItemStack itemstack = ((Slot)player.openContainer.inventorySlots.get(j)).getStack();
                                ItemStack itemstack1 = itemstack.isEmpty() ? ItemStack.EMPTY : itemstack;
                                nonnulllist1.add(itemstack1);
                            }

                            //player.sendAllContents(player.openContainer, nonnulllist1);
                            arkofusPlayer.sendAllContents(player.openContainer, nonnulllist1);
                        }
                    }
                }
                
            });

            return null;
        }
    }
}
