package fr.arkofus.packet.server;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 08/10/2018 à 13:20
 * par Jullian Dorian
 */
public class SPacketCharacteristicPoint implements IMessage {

    public String element;
    public int value;

    public SPacketCharacteristicPoint(){}

    public SPacketCharacteristicPoint(String element, int value){
        this.element = element;
        this.value = value;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.element = ByteBufUtils.readUTF8String(buf);
        this.value = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.element);
        buf.writeInt(this.value);
    }

    public static class Handler implements IMessageHandler<SPacketCharacteristicPoint, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(SPacketCharacteristicPoint message, MessageContext ctx) {
            System.out.println("Packet recu : " + message.element);

            EntityPlayerMP entityPlayerMP = ctx.getServerHandler().player;
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayerMP.getUniqueID());

            if(arkofusPlayer == null)
                return null;

            entityPlayerMP.getServerWorld().addScheduledTask(()->{

                if(arkofusPlayer.getNaturalPoints() -1 < 0)
                    return;

                switch (message.element){
                    case "vitalite":
                        arkofusPlayer.getVitalite().addNatural(5);
                        arkofusPlayer.removeNaturalPoints(1);
                        break;
                    case "sagesse":
                        arkofusPlayer.getSagesse().addNatural(1);
                        arkofusPlayer.removeNaturalPoints(3);
                        break;
                    case "force":
                        arkofusPlayer.getForce().addNatural(1);
                        arkofusPlayer.removeNaturalPoints(1);
                        break;
                    case "intelligence":
                        arkofusPlayer.getIntelligence().addNatural(1);
                        arkofusPlayer.removeNaturalPoints(1);
                        break;
                    case "chance":
                        arkofusPlayer.getChance().addNatural(1);
                        arkofusPlayer.removeNaturalPoints(1);
                        break;
                    case "agilite":
                        arkofusPlayer.getAgilite().addNatural(1);
                        arkofusPlayer.removeNaturalPoints(1);
                        break;
                }

                ArkofusServer.getMapPlayers().replace(entityPlayerMP.getUniqueID(), arkofusPlayer);
            });

            return null;
        }
    }
}
