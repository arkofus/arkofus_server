package fr.arkofus.packet.server;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 20/10/2018 à 12:19
 * par Jullian Dorian
 */
public class SPacketIdClasse implements IMessage {

    public int idClasse;

    public SPacketIdClasse(){}

    public SPacketIdClasse(int idClasse){
        this.idClasse = idClasse;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.idClasse = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.idClasse);
    }

    public static class Handler implements IMessageHandler<SPacketIdClasse, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(SPacketIdClasse message, MessageContext ctx) {

            final EntityPlayerMP entityPlayer = ctx.getServerHandler().player;
            final ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());

            if(arkofusPlayer == null)
                return null;

            entityPlayer.getServerWorld().addScheduledTask(() -> {
                arkofusPlayer.setClasse(ArkofusServer.registryClasse.getClasses().get(message.idClasse));
                arkofusPlayer.save();

                ArkofusServer.getMapPlayers().replace(entityPlayer.getUniqueID(), arkofusPlayer);
            });

            return null;
        }
    }
}
