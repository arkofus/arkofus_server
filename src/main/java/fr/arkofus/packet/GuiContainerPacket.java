package fr.arkofus.packet;

import fr.arkofus.ArkofusServer;
import fr.arkofus.packet.client.CPacketUpdateInventory;
import fr.arkofus.utils.ContainerHelper;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.lang.reflect.InvocationTargetException;

/**
 * Class créée le 17/08/2018 à 14:25
 * par Jullian Dorian
 */
public class GuiContainerPacket implements IMessage {

    public int idGui;

    public GuiContainerPacket(){}

    public GuiContainerPacket(int idGui){
        this.idGui = idGui;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.idGui = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(idGui);
    }

    public static class Handler implements IMessageHandler<GuiContainerPacket, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(GuiContainerPacket message, MessageContext ctx) {

            EntityPlayerMP entityPlayer = ctx.getServerHandler().player;

            entityPlayer.getServerWorld().addScheduledTask(() -> {
                if(message.idGui != 0){
                    RegistryPacket.getNetwork().sendTo(new CPacketUpdateInventory(ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID()).getInventory().savedCompound), entityPlayer);
                    //entityPlayer.openGui(ArkofusServer.INSTANCE, message.idGui, entityPlayer.world, (int) entityPlayer.posX, (int)  entityPlayer.posY, (int) entityPlayer.posZ);
                    try {
                        ContainerHelper.openGui(entityPlayer, ArkofusServer.INSTANCE, message.idGui);
                    } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }
    }
}
