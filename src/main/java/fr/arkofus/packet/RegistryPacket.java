package fr.arkofus.packet;

import fr.arkofus.packet.client.*;
import fr.arkofus.packet.server.*;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Class créée le 25/07/2018 à 14:01
 * par Jullian Dorian
 */
public class RegistryPacket {

    private static SimpleNetworkWrapper networkWrapper;

    private static int id;

    public RegistryPacket(SimpleNetworkWrapper wrapper) {
        networkWrapper = wrapper;
        this.id = 0;
    }

    /**
     * <p>A mettre dans le FMLInitialization ; Enregistre les packets</p>
     */
    public void initialization(){
        registerMessage(CPacketParticle.Handler.class, CPacketParticle.class, Side.CLIENT);

        registerMessage(CPacketSynchronise.Handler.class, CPacketSynchronise.class, Side.CLIENT);

        registerMessage(CCreateCharacterPacket.Handler.class, CCreateCharacterPacket.class, Side.CLIENT);
        registerMessage(CClassePacket.Handler.class, CClassePacket.class, Side.CLIENT);
        registerMessage(SPacketIdClasse.Handler.class, SPacketIdClasse.class, Side.SERVER);

        registerMessage(GuiContainerPacket.Handler.class, GuiContainerPacket.class, Side.SERVER);

        registerMessage(InventoryPacket.Handler.class, InventoryPacket.class, Side.SERVER);

        registerMessage(SPacketCharacteristic.Handler.class, SPacketCharacteristic.class, Side.SERVER);
        registerMessage(CPacketCharacteristic.Handler.class,  CPacketCharacteristic.class, Side.CLIENT);
        registerMessage(SPacketCharacteristicPoint.Handler.class, SPacketCharacteristicPoint.class, Side.SERVER);

        registerMessage(CPacketUpdateHealth.Handler.class, CPacketUpdateHealth.class, Side.CLIENT);
        registerMessage(CPacketUpdateJob.Handler.class, CPacketUpdateJob.class, Side.CLIENT);
        registerMessage(CPacketUpdateInventory.Handler.class, CPacketUpdateInventory.class, Side.CLIENT);
        registerMessage(CPacketAddItemInventory.Handler.class, CPacketAddItemInventory.class, Side.CLIENT);

        registerMessage(CPacketSetSlot.Handler.class, CPacketSetSlot.class, Side.CLIENT);
        registerMessage(CPacketWindowItems.Handler.class, CPacketWindowItems.class, Side.CLIENT);

        registerMessage(SPacketClickWindow.Handler.class, SPacketClickWindow.class, Side.SERVER);
        registerMessage(CPacketConfirmTransaction.Handler.class, CPacketConfirmTransaction.class,  Side.CLIENT);
        registerMessage(SPacketConfirmTransaction.Handler.class, SPacketConfirmTransaction.class, Side.SERVER);
    }

    /**
     * <p>Retourne le network des packets</p>
     * @return networkWrapper
     */
    public static SimpleNetworkWrapper getNetwork(){
        return networkWrapper;
    }

    private  <REQ extends IMessage, REPLY extends IMessage> void registerMessage(Class<? extends IMessageHandler<REQ, REPLY>> messageHandler, Class<REQ> requestMessageType, Side side)
    {
        networkWrapper.registerMessage(messageHandler, requestMessageType, id, side);
        id++;
    }

}
