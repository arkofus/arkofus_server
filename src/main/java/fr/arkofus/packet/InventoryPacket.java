package fr.arkofus.packet;

import fr.arkofus.ArkofusServer;
import fr.arkofus.player.ArkofusPlayer;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Class créée le 30/09/2018 à 12:25
 * par Jullian Dorian
 */
public class InventoryPacket implements IMessage {

    public int currentItem;

    public InventoryPacket(){}

    public InventoryPacket(int currentItem){
        this.currentItem = currentItem;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        this.currentItem = buf.readInt();
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.currentItem);
    }

    public static class Handler implements IMessageHandler<InventoryPacket, IMessage>{

        /**
         * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
         * is needed.
         *
         * @param message The message
         * @param ctx
         * @return an optional return message
         */
        @Override
        public IMessage onMessage(InventoryPacket message, MessageContext ctx) {

            EntityPlayer entityPlayer = ctx.getServerHandler().player;
            ArkofusPlayer arkofusPlayer = ArkofusServer.getMapPlayers().get(entityPlayer.getUniqueID());

            if(arkofusPlayer != null){
                arkofusPlayer.getInventory().setCurrentItem(message.currentItem);
            }

            return null;
        }
    }
}
