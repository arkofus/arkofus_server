package fr.arkofus.json;


/**
 * Class créée le 25/07/2018 à 13:13
 * par Jullian Dorian
 */
public interface IJson<T> {

    String toJson(T clazz);

    T fromJson(String json);

}
