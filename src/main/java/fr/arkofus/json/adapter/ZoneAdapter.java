package fr.arkofus.json.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import fr.arkofus.entities.entity.bouftou.EntityBouftonOrageux;
import fr.arkofus.server.zone.*;
import fr.arkofus.utils.WorldHelper;
import net.minecraft.entity.Entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 09/12/2018 à 18:29
 * par Jullian Dorian
 */
public class ZoneAdapter extends TypeAdapter<Zone> {

    public ZoneAdapter(){ }

    /**
     * Writes one JSON value (an array, object, string, number, boolean or null)
     * for {@code value}.
     *
     * @param writer
     * @param value the Java object to write. May be null.
     */
    @Override
    public void write(JsonWriter writer, Zone value) throws IOException {

        writer.beginObject();
        writer.name("name").value(value.getName());
        writer.name("world").value(value.getWorld().getWorldInfo().getWorldName());
        writer.name("type").value(value.getType().toString());
        writer.name("maxGroup").value(value.getMaxGroup());
        writer.name("maxEntities").value(value.getMaxEntities());
        writer.name("entities").beginArray();
        for(Class<? extends Entity> entity : value.getEntities()){
            writer.value(entity.getName()); //Name of class
        }
        writer.endArray();
        if(value.getType().toString().equalsIgnoreCase("cube")){
            writer.name("positions").beginObject();
            writer.name("x").value(((ZoneCube) value.getArea()).getFirst().getX());
            writer.name("y").value(((ZoneCube) value.getArea()).getFirst().getY());
            writer.name("z").value(((ZoneCube) value.getArea()).getFirst().getZ());
            writer.name("x2").value(((ZoneCube) value.getArea()).getSecond().getX());
            writer.name("y2").value(((ZoneCube) value.getArea()).getSecond().getY());
            writer.name("z2").value(((ZoneCube) value.getArea()).getSecond().getZ());
            writer.endObject();
        } else {
            writer.name("positions").beginArray();
            for(int i =0; i < value.getArea().getPositions().length; i++){
                writer.beginObject();
                Position position = ((ZonePolygone) value.getArea()).getPointAt(i);
                if(position != null) {
                    writer.name("x").value(position.getX());
                    writer.name("y").value(position.getY());
                    writer.name("z").value(position.getZ());
                }
                writer.endObject();
            }
            writer.endArray();
        }

        writer.endObject();

    }

    /**
     * Reads one JSON value (an array, object, string, number, boolean or null)
     * and converts it to a Java object. Returns the converted object.
     *
     * @param in
     * @return the converted Java object. May be null.
     */
    @Override
    public Zone read(JsonReader in) throws IOException {
        Zone zone = new Zone();

        in.beginObject();
        while (in.hasNext()){
            switch (in.nextName()){
                case "name":
                    zone.setName(in.nextString());
                    break;
                case "world":
                    System.out.println("set world in adapter");
                    zone.setWorld(WorldHelper.findWorldByName(in.nextString()));
                    break;
                case "type":
                    switch (in.nextString()){
                        case "cube":
                            zone.setType(Zone.Type.CUBE);
                            break;
                        case "polygon":
                            zone.setType(Zone.Type.POLYGON);
                            break;
                    }
                    break;
                case "maxGroup":
                    zone.setMaxGroup(in.nextInt());
                    break;
                case "maxEntities":
                    zone.setMaxEntities(in.nextInt());
                    break;
                case "entities":
                    in.beginArray();

                    List<Class<? extends Entity>> entities = new ArrayList<>();

                    while(in.hasNext()){
                        try {
                            Class<?> clazz = Class.forName(in.nextString());
                            entities.add((Class<? extends Entity>) clazz);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    zone.setEntities(entities);

                    in.endArray();
                    break;
                case "positions":

                    AbstractZone zonePosition;

                    if(zone.getType() == Zone.Type.CUBE){
                        zonePosition = new ZoneCube();
                        final Position first = new Position();
                        final Position second = new Position();

                        in.beginObject();
                        while (in.hasNext()){
                            switch (in.nextName()){
                                case "x":
                                    first.setX(in.nextDouble());
                                    break;
                                case "y":
                                    first.setY(in.nextDouble());
                                    break;
                                case "z":
                                    first.setZ(in.nextDouble());
                                    break;
                                case "x2":
                                    second.setX(in.nextDouble());
                                    break;
                                case "y2":
                                    second.setY(in.nextDouble());
                                    break;
                                case "z2":
                                    second.setZ(in.nextDouble());
                                    break;
                            }
                        }
                        in.endObject();
                        zonePosition.addPosition(first);
                        zonePosition.addPosition(second);
                    } else {
                        zonePosition = new ZonePolygone();
                        in.beginArray();

                        while (in.hasNext()){
                            in.beginObject();

                            Position position = new Position();

                            while (in.hasNext()){
                                switch (in.nextName()){
                                    case "x":
                                        position.setX(in.nextDouble());
                                        break;
                                    case "y":
                                        position.setY(in.nextDouble());
                                        break;
                                    case "z":
                                        position.setZ(in.nextDouble());
                                        break;
                                }
                            }
                            in.endObject();
                            zonePosition.addPosition(position);
                        }

                        in.endArray();
                    }
                    zone.setArea(zonePosition);
                    break;
            }
        }
        in.endObject();

        System.out.println("zone name:" + zone.getName());

        return zone;
    }
}
