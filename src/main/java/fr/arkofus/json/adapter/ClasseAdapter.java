package fr.arkofus.json.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.Classe;
import fr.arkofus.classe.spell.ISpell;
import fr.arkofus.classe.spell.RegistrySpell;
import fr.arkofus.entities.spell.EntitySpell;
import fr.arkofus.utils.SpellList;

import java.io.IOException;

/**
 * Class créée le 09/08/2018 à 19:31
 * par Jullian Dorian
 */
public class ClasseAdapter extends TypeAdapter<Classe> {

    /**
     * Writes one JSON value (an array, object, string, number, boolean or null)
     * for {@code value}.
     *
     * @param writer
     * @param classe the Java object to write. May be null.
     */
    @Override
    public void write(JsonWriter writer, Classe classe) throws IOException {

        writer.beginObject();

        if(classe.getId() != -1)
            writer.name("id").value(classe.getId());

        writer.name("name").value(classe.getName());
        writer.name("description").value(classe.getDescription());
        writer.name("icon").value(classe.getIconName());

        writer.name("spells").beginArray();
        for (ISpell<? extends EntitySpell> spell : classe.getSpells().getList()) {
            if(spell != null) {
                if (!spell.getName().equalsIgnoreCase("")) {
                    writer.value(spell.getName());
                }
            }
        }
        writer.endArray();

        writer.endObject();

    }

    /**
     * Reads one JSON value (an array, object, string, number, boolean or null)
     * and converts it to a Java object. Returns the converted object.
     *
     * @param reader
     * @return the converted Java object. May be null.
     */
    @Override
    public Classe read(JsonReader reader) throws IOException {

        int id = -1;
        String name = "";
        String desc = "";
        String icon = "";
        SpellList list = new SpellList();

        ISpell spell;
        RegistrySpell registrySpell = ArkofusServer.registrySpell;

        reader.beginObject();

        while(reader.hasNext()){

            String next = reader.nextName();

            if(next.equalsIgnoreCase("id")){
                id = reader.nextInt();
            }

            switch (next){

                case "name":
                    name = reader.nextString();
                    break;
                case "description":
                    desc = reader.nextString();
                    break;
                case "icon":
                    icon = reader.nextString();
                    break;
                case "spells":

                    reader.beginArray();

                    while(reader.hasNext()){

                        String spell_name = reader.nextString();

                        if(registrySpell.getRegistries().contains(spell_name)){
                            if(registrySpell.getRegistries().getLastFind() != null){
                                spell = registrySpell.getRegistries().getLastFind();
                            } else {
                                spell = registrySpell.getRegistries().get(spell_name);
                            }

                            if(spell != null){
                                list.add(spell);
                            }
                        }

                    }

                    reader.endArray();
                    break;
            }

        }

        reader.endObject();

        Classe classe = new Classe(name, desc, icon, list);
        if(id != -1) classe.setId(id);

        return classe;
    }
}
