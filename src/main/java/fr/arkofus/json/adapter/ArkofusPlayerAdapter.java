package fr.arkofus.json.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import fr.arkofus.ArkofusServer;
import fr.arkofus.classe.Classe;
import fr.arkofus.classe.RegistryClasse;
import fr.arkofus.player.ArkofusPlayer;
import fr.arkofus.player.inventory.ArkofusInventory;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;

import java.io.IOException;

/**
 * Class créée le 25/07/2018 à 13:36
 * par Jullian Dorian
 */
public class ArkofusPlayerAdapter extends TypeAdapter<ArkofusPlayer> {

    public ArkofusPlayerAdapter(){}

    /**
     * Writes one JSON value (an array, object, string, number, boolean or null)
     * for {@code value}.
     *
     * @param out
     * @param arkofusPlayer the Java object to write. May be null.
     */
    @Override
    public void write(JsonWriter out, ArkofusPlayer arkofusPlayer) throws IOException {

        out.beginObject();

        out.name("id_classe").value(arkofusPlayer.getClasse().getId());
        out.name("inventory").beginObject();
            out.name("content").value(arkofusPlayer.getInventory().savedCompound.toString());
            out.name("index").value(arkofusPlayer.getInventory().getSlotIndex());
        out.endObject();
        out.name("kamas").value(arkofusPlayer.getKamas());
        out.name("level").value(arkofusPlayer.getLevel());
        out.name("experience").value(arkofusPlayer.getExperience());
        out.name("experienceTotal").value(arkofusPlayer.getTotalExperience());
        out.name("health").value(arkofusPlayer.getHealth());

        out.name("characs_points").beginObject();
            out.name("natural").value(arkofusPlayer.getNaturalPoints());
            out.name("additional").value(arkofusPlayer.getAdditionalPoints());
        out.endObject();

        out.name("vitalite").beginObject();
            out.name("natural").value(arkofusPlayer.getVitalite().getNatural());
            out.name("additional").value(arkofusPlayer.getVitalite().getAdditional());
            out.name("bonus").value(arkofusPlayer.getVitalite().getBonus() - 65);
        out.endObject();

        out.name("sagesse").beginObject();
            out.name("natural").value(arkofusPlayer.getSagesse().getNatural());
            out.name("additional").value(arkofusPlayer.getSagesse().getAdditional());
            out.name("bonus").value(arkofusPlayer.getSagesse().getBonus());
        out.endObject();

        out.name("force").beginObject();
            out.name("natural").value(arkofusPlayer.getForce().getNatural());
            out.name("additional").value(arkofusPlayer.getForce().getAdditional());
            out.name("bonus").value(arkofusPlayer.getForce().getBonus());
        out.endObject();

        out.name("intelligence").beginObject();
            out.name("natural").value(arkofusPlayer.getIntelligence().getNatural());
            out.name("additional").value(arkofusPlayer.getIntelligence().getAdditional());
            out.name("bonus").value(arkofusPlayer.getIntelligence().getBonus());
        out.endObject();

        out.name("chance").beginObject();
            out.name("natural").value(arkofusPlayer.getChance().getNatural());
            out.name("additional").value(arkofusPlayer.getChance().getAdditional());
            out.name("bonus").value(arkofusPlayer.getChance().getBonus());
        out.endObject();

        out.name("agilite").beginObject();
            out.name("natural").value(arkofusPlayer.getAgilite().getNatural());
            out.name("additional").value(arkofusPlayer.getAgilite().getAdditional());
            out.name("bonus").value(arkofusPlayer.getAgilite().getBonus());
        out.endObject();

        out.name("miner").beginObject();
            out.name("level").value(arkofusPlayer.getJob("miner").getLevel());
            out.name("experience").value(arkofusPlayer.getJob("miner").getExperience());
            out.name("totalExperience").value(arkofusPlayer.getJob("miner").getTotalExperience());
        out.endObject();

        out.endObject();

    }

    /**
     * Reads one JSON value (an array, object, string, number, boolean or null)
     * and converts it to a Java object. Returns the converted object.
     *
     * @param in
     * @return the converted Java object. May be null.
     */
    @Override
    public ArkofusPlayer read(JsonReader in) throws IOException {

        final ArkofusPlayer arkofusPlayer = new ArkofusPlayer();
        ArkofusInventory arkofusInventory = new ArkofusInventory(null);

        in.beginObject();

        while(in.hasNext()){

            switch (in.nextName()){
                case "id_classe":
                    arkofusPlayer.setClasse(ArkofusServer.registryClasse.getClasses().get(in.nextInt()));
                    break;
                case "inventory":
                    in.beginObject();

                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "index":
                                arkofusInventory.setCurrentItem(in.nextInt());
                                break;
                            case "content":
                                final String compoundInventory = in.nextString();

                                if(compoundInventory.contains("Inventory:")) {
                                    NBTTagCompound nbtTagCompound = null;
                                    try {
                                        nbtTagCompound = JsonToNBT.getTagFromJson(compoundInventory);
                                    } catch (NBTException e) {
                                        e.printStackTrace();
                                    }

                                    if (nbtTagCompound != null && nbtTagCompound.hasKey("Inventory")) {
                                        NBTTagList nbtTagList = nbtTagCompound.getTagList("Inventory", Constants.NBT.TAG_COMPOUND);

                                        arkofusInventory = new ArkofusInventory(null);
                                        arkofusInventory.readNBT(nbtTagList);
                                        break;
                                    }
                                }
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "kamas":
                    arkofusPlayer.setKamas(in.nextInt());
                    break;
                case "level":
                    arkofusPlayer.setLevel(in.nextInt());
                    break;
                case "experience":
                    arkofusPlayer.setExperience(in.nextLong());
                    break;
                case "experienceTotal":
                    arkofusPlayer.setTotalExperience(in.nextLong());
                    break;
                case "health":
                    arkofusPlayer.setHealth(in.nextDouble(), false);
                    break;
                case "characs_points":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.setNaturalPoints(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.setAdditionalPoints(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "vitalite":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getVitalite().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getVitalite().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getVitalite().setBonus(65+ in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "sagesse":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getSagesse().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getSagesse().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getSagesse().setBonus(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "force":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getForce().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getForce().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getForce().setBonus(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "intelligence":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getIntelligence().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getIntelligence().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getIntelligence().setBonus(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "chance":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getChance().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getChance().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getChance().setBonus(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "agilite":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "natural":
                                arkofusPlayer.getAgilite().setNatural(in.nextInt());
                                break;
                            case "additional":
                                arkofusPlayer.getAgilite().setAdditional(in.nextInt());
                                break;
                            case "bonus":
                                arkofusPlayer.getAgilite().setBonus(in.nextInt());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "miner":
                    in.beginObject();
                    while(in.hasNext()){
                        switch (in.nextName()){
                            case "level":
                                arkofusPlayer.getJob("miner").setLevel(in.nextInt());
                                break;
                            case "experience":
                                arkofusPlayer.getJob("miner").setExperience(in.nextLong());
                                break;
                            case "totalExperience":
                                arkofusPlayer.getJob("miner").setTotalExperience(in.nextLong());
                                break;
                        }
                    }
                    in.endObject();
                    break;
            }
        }

        in.endObject();

        arkofusPlayer.setInventory(arkofusInventory);

        return arkofusPlayer;
    }
}
